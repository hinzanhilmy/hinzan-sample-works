$(document).ready(function (data){
		$('.alert').hide();
		$('form[name=APPOINTMENT]').validate({
			submitHandler: function(form) {
				$('.submit').text('Please Wait...');
				$.ajax({
					type : 'post',
					url	   : '/ajax/app.php',
					data   : $(form).serialize(),
					dataType : 'json',
					success : function(data){
						if(data.status){
							$('.alert').fadeIn(300).addClass('alert-success').html('Your message has been submited.');
							$('.submit').fadeOut(300);
						} else {
							$('.alert').slidedown(300).addClass('alert-warning').html('Your message not submited.');
							$('.submit').text('Resubmit');
						}
					},
					error : function(data) { alert("Not sent.. Please try again."); }
				});
				
				return false;
			 }
		});
		
		$('.con_alert').hide();
		$('form[name=contact]').validate({
			submitHandler: function(form) {
				$('.submit').text('Please Wait...');
				$.ajax({
					type : 'post',
					url	   : '/ajax/contact.php',
					data   : $(form).serialize(),
					dataType : 'json',
					success : function(data){
						if(data.status){
							$('.con_alert').fadeIn(300).addClass('alert-success').html('Email Sent Successfully.');
							$('.con_submit').fadeOut(300);
						} else {
							$('.con_alert').slidedown(300).addClass('alert-warning').html('Your message not submited.');
							$('.con_submit').text('Resubmit');
						}
					},
					error : function(data) { alert("Not sent.. Please try again."); }
				});
				
				return false;
			 }
		});
	});