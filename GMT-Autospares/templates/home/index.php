<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<meta http-equiv="Cache-control" content="private">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="google-site-verification" content="VDcCnuYYcgb8za9Lj4hiyLLrbygNoAwjH50QXgazj2E" />
		<!--
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo TEMPLATE_REC_PATH;?>android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo TEMPLATE_REC_PATH;?>favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo TEMPLATE_REC_PATH;?>favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo TEMPLATE_REC_PATH;?>favicon-16x16.png">
		<link rel="manifest" href="<?php echo TEMPLATE_REC_PATH;?>manifest.json">
		-->
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo TEMPLATE_REC_PATH;?>img/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo TEMPLATE_REC_PATH;?>img/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo TEMPLATE_REC_PATH;?>img/favicon-16x16.png">
		<link rel="manifest" href="<?php echo TEMPLATE_REC_PATH;?>img/manifest.json">
		<link rel="mask-icon" href="<?php echo TEMPLATE_REC_PATH;?>img/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<meta name="apple-mobile-web-app-title" content="GMT">
		<meta name="application-name" content="GMT">
		<meta name="theme-color" content="#ffffff">
		
		<!-- Custom fonts for this template -->
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		
		
		
		
		<!-- Custom styles for this template -->
		<link href="<?php echo TEMPLATE_REC_PATH;?>css/parent_gmt.css" rel="stylesheet">
		<title>GMT Auto Spares</title>
	</head>
    
	<body>
		<div class="preloader-wrapper" id="plw">
		<div class="preloader" id="pl">
			<!--img src="assets/images/preloader.gif" alt="NILA"-->
		</div>
		</div>
	
	
		<div class="container centered">
			
			<div class="row">
				<div class="col-md-12">
					<img src="<?php echo TEMPLATE_REC_PATH;?>img/gmt_main_logo.png" class="img-responsive center-block parent-home-logo" />
				</div>
			</div>
			<div class="col-md-12 text-center banner-text">
				<h2> 
					Quality Tools and Parts For Trucks
				</h2>
			</div>
			
			<div class="row text-center">
				<div class="col-md-12">
					<a href="/tools/" class="home">Tools</a>
					&nbsp;&nbsp;&nbsp;
					<a href="/parts/" class="home">Parts</a>  
				</div>
			</div>
		</div>
		
		<!--footer>
			  <div class="container">
				<p>&copy; GMT Auto Spares 2018. All Rights Reserved.</p>
				<ul class="list-inline">
				  <li class="list-inline-item">
					<a href="#">Privacy</a>
				  </li>
				  <li class="list-inline-item">
					<a href="#">Terms</a>
				  </li>
				  <li class="list-inline-item">
					<a href="#">FAQ</a>
				  </li>
				</ul>
			  </div>
		</footer-->

		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>	
	
		<script>
			
		$(document).ready(function($) {
			var Body = $('body');
			Body.addClass('preloader-site');
		});
		$( window ).on('load',function() {
		
				//alert("fff");
				//$('.preloader-wrapper').fadeOut();
				$('#plw').removeClass('preloader-wrapper');
				$('body').removeClass('preloader-site');
				$('#pl').hide();
		});
		




		
		
		</script>
		
		
	</body>
</html>
