<?php include ('../../../system/main.php');?>
<style>
	.error { font-size:11px; color:red; font-weight:normal; }
	.select.error , input.error , textarea.error { border-left: 1px solid red; } 
</style>

<?php echo $fw->show_session_message();?>

<form role="form" name="editwork" class="form-group-sm" style="width:450px;" method="post">
   <div class="form-group">
   	<input type="hidden" name="id" value="<?php echo $paint['id'];?>" />
  </div>
  
  <div class="form-group">
  		<label>Are you a member of GMT auto spares?</label>
  	  	<select name="member"  class="form-control" class="required" required="required">			
			
			<option>Yes</option>
			<option>No</option>
		</select>
  </div>

  <div class="form-group">
  		<label>Suggestion Type</label>
  	  	<select name="suggestion_type"  class="form-control" class="required" required="required">			
			<option>Genaral</option>
			<option>Improvment</option>
			<option>New Idea</option>
		</select>
  </div>
  
  <div class="form-group">
  	<input name="full_name" type="text" class="form-control required" placeholder="Full Name" value="" />	
  </div>
  <div class="form-group">
  	<input name="email" type="email" class="form-control email required" placeholder="eMail" value="" />	
  </div>
  <div class="form-group">
  	<input name="phone" type="text" class="form-control required" placeholder="Phone" value="" />	
  </div>
  <div class="form-group">
  	<textarea name="description" class="form-control required" rows="8" placeholder="Description"></textarea>	
  </div>
  	  		  
  
  <div class="form-group">
  <?php echo $ui->input_button_primary(array('name'=>'AddNews','type'=>'Submit','value'=>'Submit'));?>
  </div>
</form>
<?php echo $ajax->submitForm(array('form'=>'editwork','get'=>'#sys_message', 'do'=>'do_feedback.php'));?>