<?php $p = $fw->products()->get_edit($id);?>
<!-- Inner page Banner
 ================================================== -->
 	<?php $link = array(0=> 'category', 1=> 'sub-category');?>
    <div class="container-fluid banner box">
      <div class="container">
      	<?php $cat = $fw->meta()->getCatNameById($p['catagories_id']);?>
            <h1><?php echo $p['title'];?></h1>    
      </div>
    </div>
    
    <!-- /.inner-page banner -->
    <div class="container-fluid breadcrumb">
        <div class="container">
        	<a href="/">Home</a> 
        		<?php $x =0;?>
            	<?php foreach($fw->meta()->crumb($p['catagories_id']) as $crumb){?>
            			<span class="glyphicon glyphicon-menu-right"></span> <a href="/<?php echo $link[$x++];?>/<?php echo $crumb['id'];?>/<?php echo strtolower(str_replace(" ", "-", $crumb['label']));?>.html"><?php echo $crumb['label'];?></a>
            	<?php }?>
            	<span class="glyphicon glyphicon-menu-right"></span><?php echo $p['title'];?>
        </div>
    </div>

    <div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->
        <!-- inner-content block ================================================== -->
        <div class="inner-content">
          <div class="container-fluid">
            <div class="container">	

			
			   <?php if($fw->users()->isLogin()==FALSE){?>
			        <!-- inner-content block
			        ================================================== -->
			        <div class="row">
						<div class="col-lg-12">
							<p class="product-detail-login">
								Login to view price or give us a call <a href="tel:1300 048 130" class="phone-num-reg">1300 048 130</a>. If you are a new member? <a href="/register.html" class="gmt-button signup"> Signup </a>
							</p>
						</div>
			        </div>
					<div class="row">
						<div class="col-lg-12">
						   <form method="post" action="" name="login">
								<div class="form-group">
									<?php echo $ui->input_text(array('name'=>'username','hint'=>'User Name','class'=>'required form-control' , 'req'=> 'required'));?>
								</div>
								<div class="form-group">
									<?php echo $ui->input_password(array('name'=>'password','hint'=>'Password','class'=>'required form-control' , 'req'=> 'required'));?>
								</div>
								<div class="form-group">
									<?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Login','type'=>'submit','class'=>'gmt-btton'));?>
									<?php echo $ui->input_button_info(array('name'=>'forget','value'=>'Forgot Password','type'=>'button','class'=>'gmt-btton'));?>
								</div>
							</form>
						</div>
					
					</div>	
					<hr class="gmt-line" />	
			<?php } ?>
              <div class="row m-details box">

                <div class="col-lg-7">
                  <div class="owl-carousel owl-theme">
					<?php foreach($fw->products()->getProductImagesByProductId($id) as $image){?>
                    <div class="item">
                      <span class="n-r-img"><img src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE . $image['file'];?>" class="image-fixed-size"></span>
                    </div>
                    <?php }?>
                  </div>
                </div>
		                <div class="col-lg-5 product-info">
					                  <h4>Part Number: <?php echo $p['title'];?></h2>
					                  <?php if(!$fw->users()->isLogin()==FALSE){?>
								<?php $fw->users()->users_log_products($_REQUEST);?>	
					                  <p class="product-detail-p"><i class="fa fa-tag" aria-hidden="true"></i> Price AUD <span class="price"><?php echo $p['price'];?> <span style="font-size:18px;">+gst</span></span></p>
					                  <?php }?>	
					                  <i class="fa fa-sticky-note" aria-hidden="true"></i> Product Note <h4 class="product-detail-padding-left"> <?php echo nl2br($p['highlight']);?> </h4>
					                  <p class="product-detail-p">
					                    <?php echo $p['desc'];?>
					                  
					                  <!--p class="product-detail-p">
									
									  <i class="fa fa-plus-square" aria-hidden="true"></i> 
									   More Detail
									   </h4>
										<span class="product-detail-padding-left">
										<?php 
										
											//echo $p['performances'];
										 
										 ?>
										</span>
									  </p-->
									  
									  <p class="product-detail-p">
										<?php if(!$fw->users()->isLogin()==FALSE){?>
					                  	<a href="/tools/vieworder.html?action=add&id=<?php echo $p['id'];?>&global=cart_action" class="add-to-cart"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Add To Order</a>
										<?php }?>
									  </p>
		                </div>
                    </div>
                </div>
        
        
              </div>
			  
        </div><!-- /.subsidiaries -->

    </div><!-- /.fadingo -->
   
    <script>

      $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
          lazyLoad: false,
          autoplay:true,
          autoplayTimeout:4000,
          autoplayHoverPause:false,
          navSpeed: 10000,
          loop: true,
          margin: 10,
          responsiveClass: true,
          dots: true,
          nav: false,
          autoHeight:true,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 1
            },
            1000: {
              items: 1,
              margin: 20
            }
          }
        });
        $('.owl-2').owlCarousel({
          autoplay:true,
          autoplayTimeout:4000,
          autoplayHoverPause:false,
          loop: true,
          margin: 10,
          responsiveClass: true,
          dots: true,
          nav: false,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: 3
              },
              1000: {
                items: 4,
                margin: 20
              }
            }
        })
      })
    </script>
<style>
  .owl-2 .owl-item.cloned {
    float: left;
  }
</style>