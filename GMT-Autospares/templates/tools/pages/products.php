<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!--div class="container-fluid">



		<!--section class="search-products">
			<div class="row">
				<form role="search" method="get" class="search-form" action="/tools/search.html">
					<div class="col-md-9 col-xs-12">
					
						<input type="search" id="search" style="width:100%;" class="search-field gmt-text" placeholder="Search …" value="" name="s" data-swplive="true" data-swpengine="default" data-swpconfig="default" autocomplete="off">
						
					</div>
					
					<div class="col-md-3 col-xs-12 mobile-only-search">
						<input type="submit" class="search-submit gmt-button" value="Search">
						<button type="button" class="serach-by-cat gmt-button">By Category</button>
				</div>
				</form>
			</div>
		</section-->


<!--

<div class="row product-listing">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 trigger-toggle-category">
				<div class="panel panel-default">
					<div class="panel-body nested-accordion">
					
					<?php foreach($fw->meta()->getMainCats() as $m){?>
					
					
						<div class="row mt-10">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="panel-group" id="level1-accordion">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">								
												<a data-toggle="collapse" data-parent="#level1-accordion" href="#plast-pro-tree<?php echo $m['id'];?>" class="collapsed">
													<span class="accordian-arrow">
														<i class="fa fa-minus-square-o" aria-hidden="true"></i>
														<i class="fa fa-plus-square-o" aria-hidden="true"></i>
													</span> 
													<?php echo $m['name']?>
												</a>
											</h4>
										</div>
										<div id="plast-pro-tree<?php echo $m['id'];?>" class="panel-collapse collapse">
											<div class="panel-body">												
												<ul class="list-unstyled">
													<?php foreach($fw->meta()->getCatsByParentId($m['id']) as $s){?>
													<li>
														<li>
															<a href="/tools/sub-category/<?php echo $s['id'];?>/<?php echo strtolower(str_replace(" ", "_", $s['name']));?>.html"><?php echo $s['name'];?></a>	
														<?php foreach($fw->meta()->getCatsByParentId($s['id']) as $super){?>
														<ul style="padding-left:10px;">
															<li style="list-style-type:none;">
																<a href="/tools/sub-category/<?php echo $super['id'];?>/<?php echo strtolower(str_replace(" ", "_", $super['name']));?>.html"><?php echo $super['name'];?>
																</a>
															</li>
														</ul>
														<?php }?>		
														</li>
													</li>
													<?php }?>	
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
							
					<?php }?>			

					
					</div>
					
					
				</div>
			</div>
		</div>

</div-->



<div class="container-fluid">
	<div class="row affix-row">
    <?php //include_once('templates/home/pages/login.php');?>
		<div class="col-sm-3 col-md-3 affix-sidebar">
			<section class="search-products" style="margin-bottom:15%;margin-top:4%;margin-left: -5%">			
				<form role="search" class="form-inline" method="get" class="search-form" action="/tools/search.html">
					<div class="col-md-9 col-xs-9">					
						<input type="search"  style="display:inline-block;width:100%" id="search" class="search-field gmt-text" placeholder="Search …" value="" name="s" data-swplive="true" data-swpengine="default" data-swpconfig="default" autocomplete="off">
					</div>
					<div class="col-md-3 col-xs-3">
					<input type="submit" style="display:inline-block" class="search-submit gmt-button" value="Search">	
					</div>
				</form>
			</section>
		
		
			<div class="sidebar-nav">
				<div class="navbar navbar-default" role="navigation">
					<ul class="nav navbar-nav" id="sidenav01">
						<li class="active">
							<a href="#!" data-toggle="collapse"  data-target="#toggleDemo2" data-parent="#sidenav02" style="background-color:#efecec;border-bottom:3px solid #ffffff" >
								<h5>
									<i class="fa fa-sitemap"></i> Browse by Make
								</h5>
							</a>							
							
							<div class="nav-collapse collapse in" id="toggleDemo2" style="height:auto;">
								<ul class="nav nav-list sidebar-class">
									<?php foreach($product_makes as $make){ ?>
										<li><a href="/tools/search.html?u=false&s=<?php echo urlencode($make);?>" id="#toggleCategory"><i class="fa fa-angle-right"></i></i> <?php echo $make;?></a></li>
									<?php } ?>

									
								</ul>
							</div>
							
							
							<a href="#!" data-toggle="collapse"  data-target="#toggleDemo0" data-parent="#sidenav01" style="background-color:#efecec;border-bottom:3px solid #ffffff" >
								<h5>
									<i class="fa fa-sitemap"></i> Browse by Category
								</h5>
							</a>
							<div class="nav-collapse collapse in" id="toggleDemo0" style="height:auto;">
								<ul class="nav nav-list sidebar-class">
									<?php foreach($fw->meta()->getMainCats() as $m){?>
										<li><a href="/tools/sub-category/<?php echo $m['id'];?>/<?php echo strtolower(str_replace(" ", "_", $m['name']));?>.html"><i class="fa fa-angle-right"></i></i> <?php echo $m['name'];?></a>
									<?php } ?>
								</ul>
							</div>
				
						</li>
					</ul>
				</div>
			</div>
		</div> <!-- Column -->


			
				
				<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="row products">
						<!-- Products/ajax/pagination -->
							<div class="pagecontent"></div>
						<div style="clear:both;"></div>	
							<div class="pagination" style="float:right"></div>              		
						<div style="clear:both;"></div>	
					</div>
				</div>
			  
			  
			</div> <!-- row -->  
            </div><!-- /.container -->	
			<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'product_list.php','total'=> $fw->products()->total(array('status'=>1))));?>
            
	
<script>
	//reference
    //https://bootsnipp.com/snippets/featured/bootstrap-30-treeview 
	$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});



//Initialization of treeviews
<?php foreach($fw->meta()->getMainCats() as $m){?>
	$('#tree<?php echo $m['id'];?>').treed({openedClass:'glyphicon-chevron-right', closedClass:'glyphicon-chevron-down'});
<?php }?>


$( ".serach-by-cat" ).click(function() {
  $(".trigger-toggle-category").toggleClass("show-catgory");
});



$(document).ready(function () {

//alert("ff");

	$('[data-toggle="offcanvas"]').click(function () {
	$('.row-offcanvas').toggleClass('active')
});




  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww <= 768) {
      $('#toggleDemo0').removeClass('in');
	  $('#toggleDemo2').removeClass('in');
    } 
	/*else if (ww >= 401) {
      $('.test').addClass('blue');
    };*/
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();



});

	
$( "#toggleCategory" ).on( "click", function(e) {

	 e.preventDefault();
        e.stopPropagation();
});



</script>
