<!-- Inner page Banner
 ================================================== -->
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>Tearms and Conditions</h1>
    </div>
</div>
<!-- /.inner-page banner -->


<div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->

    <!-- inner-content block
    ================================================== -->
    <div class="inner-content">
        <div class="container-fluid">
            <div class="container">

                <div class="row">
					<div class="col-md-12">

						
		
<ol>
	<li>All goods supplied by GMT Autospares to the customer / client on credit (To be organised with GMT Autospares on its terms before credit facility can be used) will be paid for in full by the customer on or before the agreed day. Failure to do so will result in termination of credit facility without any notification to you (the customer / client). </li>
	
	<br />
	
	<li>GMT Autospares may decline any order for goods and services  (wholly or partially) at any time prior to the delivery of goods and services, in which case GMT Autospares will not be under any obligation to fulfil order request places by you (the customer / client). </li>
	<br />
	
	
	<li>If the Customer fails to pay GMT Autospares for goods and services within the credit period GMT Autospares may: 
	<br /><br />
		<ul> 
			<li>Suspend credit extended to you (The customer / client) and the balance of the account shall immediately become due and payable without further demand.</li>
			<br />
			<li> Withhold the delivery of goods and services already ordered</li>
			<br />
			<li> Charge interest on the overdue amount at the overdraft rate payable by GMT Autospares plus 7% calculated on daily balances  on the amount overdue until payment in full.</li>
			<br />
			<li>Charge costs (Bourne by the customer/client) of any attempt made by or on behalf of GMT Autospares to recover monies for goods and services from the customer/client or to secure and indebtedness or liability to the customer. </li>
			
		
		</ul>
	
	</li>
	
	
	
	
	<li>4. GMT Autospares reserves the right at all times to suspend   supplies of further goods and services to the customer/client without a requirement to provide reasons for its action to the customer/client or subsidiaries of the customer/client.</li>
	<br />
	
	<li>GMT Autospares will not take any liability (fully or partially) for any loss or damage caused to the products during delivery, handling or installation. </li>
	<br />
	
	
	<li>All goods shall be at the risk of the customer/client from the time of despatch of the goods by GMT Autospares for delivery to the customer/client and responsibility for insurance during transit falls with the customer/client solely.  The supplier will arrange insurance on behalf of the customer only being expressly requested to do so by the customer in writing.  Costs for such insurance shall be debited to the customer/client. Customer/client is to enquire Insurance information from GMT Autospares prior to place order or purchase of products.
</li>
	<br />
	
	
	<li>GMT Autospares does not take any responsibility (Fully or partially) in misuse or injuries or death caused while using the product</li>
	<br />
	
	
	<li>Any non-agreements or disputes in relation to invoices or any other matters must be bought to GMT Autospares as soon as practicable, in addition to this, a written notice also, should be forwarded to GMT Autospares within ten (10) days (inclusive of weekends) of purchase made.

</li>
	<br />
	
		<li>Pending payment for faulty products, the purchaser must store the product/s in good condition and in a way, which clearly indicated by GMT Autospares.

</li>
	<br />
	
	
</ol>

<h3>
RETURN OF GOODS

</h3>

<ol>
<li>
Electrical goods are non-returnable. E.g. Globes etc. 
</li>
<br />

<li>
 Goods returned for credit must be received within 10 days of sale or as agreed by GMT Autospares with prior arrangements.
</li>
<br />

<li>
Faulty product/s must be returned in original condition and original packaging including boxes. Product/s must not have been tested or used in any way or of form. 
</li>
<br />

<li>
Any use or misuse of product/s (to be returned as faulty) nullifies the agreement and warranty agreement/s with GMT Autospares and becomes customer/client’s sole responsible for payment in full within 7 days (inclusive weekends). 
</li>
<br />

<li>
 GMT Autospares reserves the right to impose extra charges on any returned product/s such as handling, insurances charges etc. 
</li>
<br />	
</ol>


<h3>
WARRANTIES

</h3>



<ol>
<li>
Our goods come with guarantees that cannot be excluded under the Australian Consumer Law. You are entitled to a replacement or refund for a major failure. You are also entitled to have the goods repaired or replaced if the goods fail to be of acceptable quality and the failure does not amount to a major failure.
</li>
<br />


<li>
Products supplied by GMT Autospares comply with manufacturer’s warranty only.
</li>
<br />


<li>
 Manufacturer’s warranty may not cover damage caused by improper use of product/s or attempted to be used by unqualified personnel. Our products are to be handled and used by only approved and or qualified personnel.
</li>
<br />


<li>
 Wear and tear of product/s is not covered by any warrantee agreements by the manufacturer or GMT Autospares.
</li>
<br />


<li>
Faulty product/s must be returned to our address provided, via freight paid for inspection first and then for processing if eligible under warrantee agreement. 
</li>
<br />




		







 
					

					</div>
					
				

                </div>

            </div><!-- /.container -->
        </div>
    </div><!-- /.subsidiaries -->

</div>