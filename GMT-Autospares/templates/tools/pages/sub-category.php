

<!-- Inner page Banner
 ================================================== -->
    <div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
      <div class="container">
      	<?php $cat = $fw->meta()->getCatNameById($id);?>
        <h1><?php echo $cat['name'];?></h1>
      </div>
    </div>
    <!-- /.inner-page banner -->
    <div class="container-fluid breadcrumb">
        <div class="container">
        	<a href="/">Home</a> 
            	<?php foreach($fw->meta()->crumb($id) as $crumb){?>
            		<?php if($id != $crumb['id']){?>
            			<span class="glyphicon glyphicon-menu-right"></span> <a href="/category/<?php echo $crumb['id'];?>/<?php echo strtolower(str_replace(" ", "-", $crumb['label']));?>.html"><?php echo $crumb['label'];?></a>
            		<?php } else {?>
            			<span class="glyphicon glyphicon-menu-right"></span><?php echo $crumb['label'];?>
            		<?php }?>
            	<?php }?>
        </div>
    </div>
	
	
	<div class="container-fluid">
	<div class="row affix-row">
    <?php //include_once('templates/home/pages/login.php');?>
		<div class="col-sm-3 col-md-3 affix-sidebar">
			<section class="search-products" style="margin-bottom:15%;margin-top:4%;margin-left: -5%">			
				<form role="search" class="form-inline" method="get" class="search-form" action="/search.html">
					<div class="col-md-9 col-xs-9">					
						<input type="search"  style="display:inline-block;width:100%" id="search" class="search-field gmt-text" placeholder="Search …" value="" name="s" data-swplive="true" data-swpengine="default" data-swpconfig="default" autocomplete="off">
					</div>
					<div class="col-md-3 col-xs-3">
					<input type="submit" style="display:inline-block" class="search-submit gmt-button" value="Search">	
					</div>
				</form>
			</section>
		
		
			<div class="sidebar-nav">
				<div class="navbar navbar-default" role="navigation">
					<ul class="nav navbar-nav" id="sidenav01">
						<li class="active">
							<a href="#!" data-toggle="collapse"  data-target="#toggleDemo2" data-parent="#sidenav02" style="background-color:#efecec;border-bottom:3px solid #ffffff" >
								<h5>
									<i class="fa fa-sitemap"></i> Browse by Make
								</h5>
							</a>							
							
							<div class="nav-collapse collapse in" id="toggleDemo2" style="height:auto;">
								<ul class="nav nav-list sidebar-class">
									<?php foreach($product_makes as $make){ ?>
										<li><a href="/search.html?s=<?php echo urlencode($make);?>" id="#toggleCategory"><i class="fa fa-angle-right"></i></i> <?php echo $make;?></a></li>
									<?php } ?>

									
								</ul>
							</div>
							
							
							<a href="#!" data-toggle="collapse"  data-target="#toggleDemo0" data-parent="#sidenav01" style="background-color:#efecec;border-bottom:3px solid #ffffff" >
								<h5>
									<i class="fa fa-sitemap"></i> Browse by Category
								</h5>
							</a>
							<div class="nav-collapse collapse in" id="toggleDemo0" style="height:auto;">
								<ul class="nav nav-list sidebar-class">
									<?php foreach($fw->meta()->getMainCats() as $m){?>
										<li><a href="/sub-category/<?php echo $m['id'];?>/<?php echo strtolower(str_replace(" ", "_", $m['name']));?>.html"><i class="fa fa-angle-right"></i></i> <?php echo $m['name'];?></a>
									<?php } ?>
								</ul>
							</div>
				
						</li>
					</ul>
				</div>
			</div>
		</div> <!-- Column -->
	
	

			<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="row products">
						<!-- Products/ajax/pagination -->
							<div class="pagecontent">
								
								             	<?php if(sizeof($fw->products()->get_productsby_cat($id)) > 0) {?>
					<?php foreach($fw->products()->get_productsby_cat($id) as $p){?>
	                <div class="item-list col-lg-4 col-sm-4 col-xs-12 box" style="padding:10px;">
	                  <a href="/machinery-detail/<?php echo $p['id'];?>/<?php echo str_replace(" ","-",$p['title']);?>.html" class="item">
	                    <span class="item-w">
	                      <span class="item-img"><img style="width:150px; height:auto" src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE . $p['file'];?>"> </span>
	                      <span class="item-name"><?php echo $p['title'];?></span>
	                    </span>
	                  </a>
	                </div>
					<?php }?>
				<?php } else {?>
					<div class="col-lg-12 col-sm-12 col-xs-12" style="width:100%">
						<div class="alert alert-danger">No Records</div>
					</div>
				<?php }?>	
								
							
							</div>
							
							
						<div style="clear:both;"></div>	
							<div class="pagination"></div>              		
						<div style="clear:both;"></div>	
					</div>
			</div>
				
		</div>
		</div>
		
	


