<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Cache-control" content="private">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="google-site-verification" content="VDcCnuYYcgb8za9Lj4hiyLLrbygNoAwjH50QXgazj2E" />
	<!--
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo TEMPLATE_REC_PATH;?>apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo TEMPLATE_REC_PATH;?>android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo TEMPLATE_REC_PATH;?>favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo TEMPLATE_REC_PATH;?>favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo TEMPLATE_REC_PATH;?>favicon-16x16.png">
    <link rel="manifest" href="<?php echo TEMPLATE_REC_PATH;?>manifest.json">
	-->
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo TEMPLATE_REC_PATH;?>apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo TEMPLATE_REC_PATH;?>favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo TEMPLATE_REC_PATH;?>favicon-16x16.png">
	<link rel="manifest" href="<?php echo TEMPLATE_REC_PATH;?>manifest.json">
	<link rel="mask-icon" href="<?php echo TEMPLATE_REC_PATH;?>safari-pinned-tab.svg" color="#5bbad5">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<meta name="apple-mobile-web-app-title" content="GMT">
	<meta name="application-name" content="GMT">
	<meta name="theme-color" content="#ffffff">
	
    <title>GMT Auto Spares</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo TEMPLATE_REC_PATH;?>css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo TEMPLATE_REC_PATH;?>css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo TEMPLATE_REC_PATH;?>js/ie-emulation-modes-warning.js"></script>

      <!-- Fonts for this template -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

      <!-- Owl Stylesheets -->
      <link rel="stylesheet" href="<?php echo TEMPLATE_REC_PATH;?>css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo TEMPLATE_REC_PATH;?>css/owl.theme.default.min.css">

      <!-- icon moon styles -->
      <link rel="stylesheet" href="<?php echo TEMPLATE_REC_PATH;?>icomoon/style.css">
      
      <!-- Custom styles for this template -->
      <link href="<?php echo TEMPLATE_REC_PATH;?>css/gmt.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo TEMPLATE_REC_PATH;?>js/vendor/jquery.min.js"><\/script>')</script>
    
	<script src="<?php echo TEMPLATE_REC_PATH;?>js/owl.carousel.js"></script>
	<script src="<?php echo TEMPLATE_REC_PATH;?>js/bootstrap.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	
	
	<!--
	<script type="text/javascript" src="<?php echo JS_PATH;?>jquery-1.11.0.js"></script>
	-->
	
    <script src="<?php echo PATH_3DPARTY;?>validation/validation_jquery_1.11.js"></script>


    <script src="<?php echo TEMPLATE_REC_PATH;?>js/bootstrap.min.js"></script>    
    <script src="<?php echo TEMPLATE_REC_PATH;?>js/jquery.twbsPagination.js"></script>
    
    <!-- Auto -->
    <script src="<?php echo PATH_3DPARTY;?>auto/jquery.easy-autocomplete.min.js"></script> 

    <!--  AutoComplete -->
    <script type="text/javascript" src="<?php echo PATH_3DPARTY;?>autocomplete/jquery.auto-complete.min.js"></script>
    <link href="<?php echo PATH_3DPARTY;?>autocomplete/jquery.auto-complete.css?r=1518081859" rel="stylesheet" type="text/css"/>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo PATH_3DPARTY;?>fancyapps/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo PATH_3DPARTY;?>fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" /> 
	 
	 
	<script src="<?php echo TEMPLATE_REC_PATH;?>js/vendor/typewitter/jquery.typewriter.js"></script>    
	
	
   
   
  

  </head>  
  
<?php if($page=='logout'){ $fw->users()->logOut(); }?>
<?php if($_REQUEST['btnlogin']){?>
	 <?php if($fw->users()->isLogin()==FALSE){ $fw->users()->login($_REQUEST); } else {}?>
<?php }?>
