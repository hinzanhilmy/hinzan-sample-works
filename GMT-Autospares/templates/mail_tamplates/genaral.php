<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!--<![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <style>

    body {
      Margin: 0;
      padding: 0;
      min-width: 100%;
      background-color: #ffffff;
      font-family: Arial, sans-serif;
    }
    table {
      border-spacing: 0;
      font-family: sans-serif;
      color: #333333;
    }
    td {
      padding: 0;
    }
    img {
      border: 0;
    }
    .wrapper {
      width: 100%;
      table-layout: fixed;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }
    .webkit {
      max-width: 590px;
      padding-bottom: 50px;
      border: 5px solid #13B495;
      padding-top: 20px;
    }

    .outer {
      Margin: 0 auto;
      width: 100%;
      max-width: 600px;
    }

    .full-width-image{
      text-align: center;
      background: url('http://dev.webuytogether.com.au/cdn/themes/ice/img/header-line.gif');
      background-repeat: repeat-x;
      background-position: 0 -35px;
      padding: 0 15px;
      padding-bottom: 20px;
    }

    .full-width-image img {
      width: 100%;
      max-width: 300px;
      height: auto;
    }


    .inner {
      padding: 10px;
    }
    p {
      Margin: 0;
    }
    a {
      color: #ee6a56;
      text-decoration: underline;
    }
    .h1 {
      font-size: 21px;
      background: #F5F5F5;
      Margin-bottom: 0;
      padding: 12px 10px;
      color: #545656;
      border-radius: 5px 5px 0 0;
      border-bottom: 1px solid #DEDEDE;
    }


    .one-column .contents {
      text-align: left;
    }
    p {
      font-size: 15px;
      line-height: 18px;
      Margin-bottom: 10px;
    }



    .social-m{
      text-align: center;
    }

    .social-m table{
      width: 70%;
      margin: 0 auto;
    }

    .social-m .text{
      text-align: center;
    }
    .s-item
    {
      font-size: 14px;
      text-align: center;
      margin: 0 auto;
    }
    .s-item a{
      color: #808184;
      text-decoration: none;
      text-align: center;
    }
    .foot {
      color: #808184;
      text-align: center;
      border-top: 1px solid #E0E0E0;
      padding-top: 30px;
      padding-bottom: 20px;
    }
    .footer-links{
      Margin: 40px auto 15px auto;
    }
    .footer-links a{
      display: inline-block;
      padding: 0 10px;
      font-size: 13px;
      color: #808184;
      text-decoration: none;
    }

    .foot-logo{
      Margin: 0 auto
    }
    .foot-logo td{
      text-align: center;
      color: #6D6E70;
      font-size: 14px;
      line-height: 21px;
    }
    .foot-logo img{
      width: 160px;
      margin-bottom: 20px;
    }

    @media screen and (max-width: 400px) {
      .two-column .column,
      .three-column .column {
        max-width: 100% !important;
      }
      .two-column img {
        max-width: 100% !important;
      }
      .three-column img {
        max-width: 50% !important;
      }

      .full-width-image{
        background-position: 0 -27px;
        background-size: 3px;
      }

      .social-m table{
        width: 100%;
      }

      .footer-links a{
        display: block;
        text-align: center;
      }
    }

    @media screen and (min-width: 401px) and (max-width: 620px) {
      .three-column .column {
        max-width: 33% !important;
      }
      .two-column .column {
        max-width: 50% !important;
      }
    }
  </style>

  <!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
    table {border-collapse: collapse !important;}
  </style>
  <![endif]-->
</head>
<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#ffffff;font-family:Arial, sans-serif;" >
<center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;" >
  <div class="webkit" style="max-width:590px;padding-bottom:50px;border-width:1px;border-style:solid;border-color:#a4a5a5;padding-top:20px;" >
    <!--[if (gte mso 9)|(IE)]>
    <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
      <tr>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
    <![endif]-->
    <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;font-family:sans-serif;color:#333333;Margin:0 auto;width:100%;max-width:600px;" >
      <tr>
        <td style="text-align:center;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#E0E0E0;padding-top:1px;padding-bottom:10px;">
          <img src="http://gmt.yaalu.com.au/templates/home/img/gmt_main_logo.jpg" width="600" alt="" style="border-width:0;width:20%;max-width:200px;height:auto;" />
        </td>
      </tr>

       <tr>
        <td class="one-column" style="padding-top:50px;padding-bottom:100px;padding-right:0;padding-left:0;" >
          <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
            <tr>
              <td class="inner contents" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;text-align:left;" >
                  <p style="Margin:0;font-size:15px;line-height:18px;Margin-bottom:10px;" >Dear {name},</p>
                  <p style="Margin:0;font-size:15px;line-height:18px;Margin-bottom:10px;" >{message}</p>				  
			  </td>
			</tr>
			<tr>
				<td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
					{html}
				</td>
			</tr>			
          </table>
        </td>
      </tr>





      <tr>
        <td class="foot" style="padding-right:0;padding-left:0;color:#808184;text-align:center;border-top-width:1px;border-top-style:solid;border-top-color:#E0E0E0;padding-top:30px;padding-bottom:1px;" >
          <p style="Margin:0;font-size:15px;line-height:18px;Margin-bottom:10px;" >For more information Call us on: 03 9015 4684</p>
        </td>
      </tr>

      <tr>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
          <table class="footer-links" style="border-spacing:0;font-family:sans-serif;color:#333333;Margin:2px auto 15px auto;" >
            <tr>
              <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                <a href="http://gmtautospares.com.au/about.html" style="display:inline-block;padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;font-size:13px;color:#808184;text-decoration:none;" >About Us</a>
                <a href="http://gmtautospares.com.au/contact.html" style="display:inline-block;padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;font-size:13px;color:#808184;text-decoration:none;" >Contact Us</a>
                <a href="http://gmtautospares.com.au/terms_condition.php" style="display:inline-block;padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;font-size:13px;color:#808184;text-decoration:none;" >Terms and Conditions</a>
                <a href="http://gmtautospares.com.au/privacy_policy.php" style="display:inline-block;padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;font-size:13px;color:#808184;text-decoration:none;" >Privacy Policy</a>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
          <table class="foot-logo" style="border-spacing:0;font-family:sans-serif;color:#333333;Margin:0 auto;" >
         
            <tr>
              <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;color:#6D6E70;font-size:14px;line-height:21px;" >
                 &copy; GMT Auto spares 2018, No 18A Second Av, Boxhills, VIC 3152
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!--[if (gte mso 9)|(IE)]>
    </td>
    </tr>
    </table>
    <![endif]-->
  </div>
</center>
</body>
</html>