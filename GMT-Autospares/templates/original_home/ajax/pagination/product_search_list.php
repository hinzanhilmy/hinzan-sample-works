<?php include ('../../../../system/main.php'); ?>
				<?php if(sizeof($fw->products()->search($next)) > 0) {?>
					<?php foreach($fw->products()->search($next) as $p){?>
	                <div class="col-lg-3 col-sm-4 col-xs-12 box item-list" style="padding:10px;">
	                  <a href="/machinery-detail/<?php echo $p['id'];?>/<?php echo str_replace(" ","-",$p['title']);?>.html" class="item">
	                    <span class="item-w">
	                      <span class="item-img"><img style="width:150px; height:auto" src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE . $p['file'];?>"> </span>
	                      <span class="item-name"><?php echo $p['title'];?></span>
	                    </span>
	                  </a>
	                </div>
					<?php }?>
				<?php } else {?>
					<div class="alert alert-danger">No Records</div>
				<?php }?>