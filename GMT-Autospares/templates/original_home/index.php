<?php include_once('content/header.php')?>
  <body class="<?php echo ($page=='home')? 'home front' : 'not-front inner-page';?>">

  
  <?php
  
  echo($page);
  ?>

	<?php
		/* pages/vieworder.php */
		if($fw->xss()->safe($_REQUEST['global']) == "cart_action"){
			$fw->cart()->add($_REQUEST);			
		}
	?>


  	<?php include_once ('content/nav.php');?>
    
    <?php
	    switch($page){ 
	    	case 'logout': include_once ('content/banner.php');	break;
	    	case 'home' :	include_once ('content/banner.php');	break;
			//case 'home' :	include_once ('content/banner.php');	break;
	    }
    ?>  

    <?php 
    	switch($page){
			default: 			include_once('pages/products.php');							break; //home.php
			case 'login':			include_once('pages/login.php');						break;
			case 'register':			include_once('pages/register.php');					break;
			case 'logout':			include_once('pages/home.php');							break;
			case 'category': 		include_once('pages/category.php');						break;
			case 'sub-category': 		include_once ('pages/sub-category.php'); 					break;
			case 'machinery-detail': 	include_once ('pages/machinery_detail.php'); 					break;
			case 'spare_parts':		include_once ('pages/traders.php'); 						break;
			case 'spare_parts_details': 	include_once ('pages/traders_details.php'); 					break;
			case 'contact': 		include_once ('pages/contact.php'); 						break;
            case 'about': 			include_once ('pages/about.php'); 						break;
            case 'terms_condition': 	include_once ('pages/terms_condition.php'); 					break;
            case 'privacy_policy': 		include_once ('pages/privacy_policy.php'); 					break;
			case 'search': 			include_once ('pages/search.php'); 						break;
      		case 'search_spare_parts':	include_once ('pages/search_spare_parts.php'); 					break;
			case 'products':	include_once ('pages/products.php'); 							break;
			case 'vieworder':	include_once ('pages/vieworder.php'); 							break;
			
	
			
			
      		
		}
    ?>
	<div id="main">
	</div>
    <!-- FOOTER -->
    <footer>
    <nav><div class="clearfix container">
        <div class="col-lg-6 col-md-6 foot-menu">
          <a href="https://gmtautospares.com.au/products.php">Products</a>   |   <a href="https://gmtautospares.com.au/about.php">About</a>   |   
	  <a href="https://gmtautospares.com.au/contact.php">Contact</a> | 
	  <a href="https://gmtautospares.com.au/terms_condition.php">Terms and Conditions</a> | 
	  <a href="https://gmtautospares.com.au/privacy_policy.php">Privacy Policy</a>  


        </div>
        <div class="col-lg-6 col-md-6 social-links">
          
		  <span> </span>
		  <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="9.7" height="18.76" viewBox="0 0 9.7 18.76"><path fill-rule="evenodd" clip-rule="evenodd" d="M2.15 10.37v8.4h4.1V10.4H9l.68-3.47h-3.4V4.37c0-.35.2-.82.75-.82H9V0H5.3C4.03 0 2.15 1.57 2.15 3.24v3.58H0v3.53l2.15.02z"/></svg></a>
		  
		  <!--
          <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="18.92" height="18.91" viewBox="0 0 18.92 18.91"><path d="M9.96 15.2c0-.17-.02-.32-.06-.47l-.1-.4c-.04-.13-.1-.26-.2-.4l-.24-.34c-.07-.1-.17-.3-.3-.4-.14-.2-.24-.2-.3-.3l-.38-.3-.35-.3-.38-.3-.35-.28h-.56c-.4 0-.82.03-1.2.1-.4.03-.82.12-1.23.26-.4.13-.7.3-1.1.52-.3.2-.5.5-.7.83-.2.4-.3.8-.3 1.25 0 .5.2.98.4 1.38.3.4.6.73 1.1.96.5.2.9.4 1.4.5.5.1 1 .2 1.48.2.45 0 .9-.1 1.28-.2.4-.1.8-.24 1.16-.44.38-.2.65-.5.87-.87.2-.33.3-.75.3-1.23M8.6 5.4c0-.46-.06-.94-.2-1.45-.12-.52-.3-1-.53-1.48-.23-.48-.55-.88-.96-1.2C6.5.97 6.1.8 5.6.8c-.7 0-1.25.3-1.64.8-.4.53-.58 1.16-.58 1.9 0 .34.05.7.13 1.1.1.4.2.8.4 1.2.2.4.4.75.7 1.05.3.32.6.57.9.78.4.2.7.3 1.1.3.8 0 1.3-.24 1.6-.7.4-.46.5-1.06.5-1.82M7.1 0h4.97l-1.53.9H9c.54.34.96.8 1.25 1.42.3.62.44 1.26.44 1.93 0 .56-.1 1.06-.3 1.5-.2.43-.4.78-.7 1.04s-.5.5-.8.7c-.3.2-.5.4-.68.7-.2.2-.27.5-.27.7 0 .2.1.4.2.5.14.2.3.34.5.52l.7.53.7.62c.24.2.5.46.66.73.2.3.37.6.5 1 .1.37.2.8.2 1.2 0 1.2-.54 2.3-1.6 3.2-1.15 1-2.75 1.5-4.8 1.5-.43 0-.9-.03-1.35-.1-.46-.1-.9-.2-1.4-.4-.44-.17-.86-.4-1.22-.65-.34-.23-.63-.6-.85-1-.23-.4-.34-.9-.34-1.4 0-.42.12-.94.4-1.5.24-.5.6-.9 1.1-1.25.5-.35 1.02-.6 1.64-.8.6-.2 1.2-.33 1.8-.4.58-.1 1.14-.14 1.7-.15-.47-.66-.72-1.2-.72-1.7 0-.1 0-.2.03-.3l.07-.2.1-.2.06-.23c-.3.03-.57.04-.8.04-1.1 0-2.1-.3-2.9-1.1-.8-.7-1.2-1.6-1.2-2.8 0-1 .35-2 1.07-2.8C3.03 1 3.93.5 4.97.3 5.67.03 6.4 0 7.1 0m11.82 2.9v1.46H16v2.9h-1.46v-2.9h-2.9V2.9h2.9V0H16v2.9h2.92z"/></svg></a>
          <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="17.99" height="12.56" viewBox="0 0 17.99 12.56"><path fill-rule="evenodd" clip-rule="evenodd" d="M17.23 1.2C16.73.34 16.2.18 15.13.1 14.05.03 11.33 0 9 0 6.65 0 3.94.03 2.86.1S1.26.33.76 1.2C.26 2.1 0 3.6 0 6.3c0 2.66.26 4.2.76 5.06.5.88 1.02 1.04 2.1 1.12 1.08.06 3.8.1 6.14.1 2.35 0 5.06-.04 6.13-.1 1.08-.08 1.6-.24 2.1-1.12.5-.87.76-2.4.76-5.05s-.3-4.2-.8-5.04M7.2 9.6V3l5.1 3.3-5.1 3.3z"/></svg></a>
          <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="18.87" height="15.33" viewBox="0 0 18.87 15.33"><path d="M18.87 1.8c-.7.32-1.45.53-2.23.63.8-.48 1.4-1.24 1.7-2.14-.75.4-1.57.7-2.45.9C15.1.43 14.1 0 13 0c-2.08 0-3.8 1.74-3.8 3.87 0 .3.02.6.1.88-3.24-.15-6.1-1.7-8-4.05C1 1.3.8 1.96.8 2.67.8 4 1.5 5.2 2.52 5.9 1.9 5.86 1.3 5.7.78 5.4v.05c0 1.88 1.34 3.44 3.1 3.8-.32.1-.67.13-1.02.13-.25 0-.5 0-.73-.07.5 1.6 1.93 2.7 3.62 2.7-1.3 1.1-2.98 1.7-4.8 1.7-.3 0-.62 0-.92-.03 1.7 1.1 3.74 1.74 5.93 1.74 7.12 0 11-5.9 11-11v-.5c.77-.55 1.4-1.23 1.94-2.02"/></svg></a>
		  
		  -->
        </div>
      </div></nav>
      <!--p class="pull-right"><a href="#">Back to top</a></p-->
      <p>&copy;  GMT Autospares ABN - 66861828625 <!--&middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a--></p>
	  
	  	
		
		
		<div id="" class="">
			<p id="">
					
					<a href="<?php echo AJAX_PATH;?>feedback.php" class="fancybox.ajax popup feedback">FEEDBACK</a>		
				</p>
		</div>

    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--Owl-carousel js
    =======================-->
    <script src="<?php echo TEMPLATE_REC_PATH;?>js/owl.carousel.js"></script>
    <?php if($page != "machinery-detail"){?>
    <script>
        $(document).ready(function() {
            $('.owl-carousel').owlCarousel({
                lazyLoad: true,
                autoplay:true,
                autoplayTimeout:5000,
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: true
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: true,
                        margin: 20
                    }
                }
            })
        })
    </script>
    <?php }?>
    <!--Owl-carousel js END =======================-->


	<link rel="stylesheet" href="<?php echo PATH_3DPARTY;?>auto/easy-autocomplete.min.css">
    	<link rel="stylesheet" href="<?php echo PATH_3DPARTY;?>auto/easy-autocomplete.themes.min.css">
    	<style type="text/css">
		.eac-item a { color:white !important; font-size:14px !important;}
	</style>
	<script>
			var options = {

			  url: function(phrase) {
			    return "api/search_product.php";
			  },


			  	getValue: "title",
			    template: {
			    	type: "links",
			        fields: {
			            link: "link"
			        }
			    },

			    list: {
			        match: {
			            enabled: true
			        }
			    },

			    theme: "plate-dark",
				adjustWidth: false,

				    
			  /*getValue: function(element) {
			    return element.title;
			  },*/

			  ajaxSettings: {
			    dataType: "json",
			    method: "POST",
			    data: {
			      dataType: "json"
			    }
			  },

			  preparePostData: function(data) {
			    data.phrase = $("#search").val();
			    return data;
			  },

			  requestDelay: 400
			};

			$("#search").easyAutocomplete(options);	
	</script>



	
	
	<script>
		
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo TEMPLATE_REC_PATH;?>js/ie10-viewport-bug-workaround.js"></script>
    <?php echo $ajax->fancyBoxByClass('popup');?>
  </body>
</html>