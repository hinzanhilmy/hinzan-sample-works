<?php if($fw->users()->isLogin()==FALSE){?>
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>Login</h1>
    </div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 signup-adj">
		
			New to GMT? <a href="/register.html"> register here </a>
	
		</div>
	</div>        
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		   <form method="post" action="" name="login">
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'username','hint'=>'User Name','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>
				
				<div class="form-group">
					<?php echo $ui->input_password(array('name'=>'password','hint'=>'Password','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>
				
				
				<div class="form-group">
					<?php echo $ui->input_button_info(array('name'=>'forget','value'=>'Forgot Password','type'=>'button','class'=>'gmt-btton'));?>
					<?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Login','class'=>'gmt-btton','type'=>'submit'));?>
				</div>
				
			</form>
		 </div>    
	</div>
</div>
<?php } else {?>
	<?php include_once ('templates/home/content/banner.php'); ?>
	<?php include_once('home.php');?>
<?} ?>