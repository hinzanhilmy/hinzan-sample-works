
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>Register</h1>
    </div>
</div>
<div class="container">
        
	<div class="row">
		<div class="col-md-8 col-md-offset-2" style="padding-top:20px;">
		   <?php if(!($id==1 || $id==2)){?>
		   <form method="post" action="" name="register" id="register">
		   		<input type="hidden" name="accountrype" value="NOLOG" />
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'fullname','hint'=>'Full Name','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>				
				<div class="form-group">
					<input type="text" name="email" class="form-control required" placeholder="Email" id="email" />
				</div>				
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'company','hint'=>'Company Name','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>	
				
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'streetno','hint'=>'Street No','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>
				
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'streetname','hint'=>'Street Name','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>
				
				<div class="form-group">
					<input type="text" name="postal_code" class="form-control" placeholder="Postal Code" value="" required />
				</div>
				
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'suburb','hint'=>'Suburb','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>
				
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'state','hint'=>'State','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>				
					
				<div class="form-group">
					<?php echo $ui->input_text(array('name'=>'abn','hint'=>'ABN','class'=>'required form-control' , 'req'=> 'required'));?>
				</div>
				<div class="form-group">
					<?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Register','type'=>'submit','class'=>'gmt-btton'));?>
				</div>
			</form>
			<?php } else if ($id == 1){?>
				<div class="alert alert-success" role="alert">
					We Will Verify Your Detail and Confirm via an Email
				</div>
			<?php } else if($id== 2){?>
				<div class="aalert alert-danger" role="alert">
					Oops! something went wrong, Please fill all the fields correclty.
				</div>
			<?php }?>
		 </div>    
	</div>
</div>

<style>
.autocomplete-suggestions { font-size:11px;}
</style>

<script>
$(function(){
	$('input[name="postal_code"]').autoComplete({
		minChars:1,
		cache: true,
		menuClass: '',
		source: function(term, response){
			$.getJSON('<?php echo AJAX_PATH?>json_postal_code.php', { search: term, postal:true}, function(data){ response(data); });
		},
		onSelect: function(e, term, item){
			var re = term.split("-");
			$('input[name="postal_code"]').val($.trim(re[1]));
			return true;	                        	
		}
	});

	$('input[name="state"]').autoComplete({
		minChars:1,
		cache: true,
		menuClass: '',
		source: function(term, response){
			$.getJSON('<?php echo AJAX_PATH?>json_postal_code.php', { search: term, state:true , postal:false}, function(data){ response(data); });
		},
		onSelect: function(e, term, item){
			var re = term.split("-");
			$('input[name="state"]').val($.trim(term));
			return true;	                        	
		}
	});

	$('input[name="suburb"]').autoComplete({
		minChars:1,
		cache: true,
		menuClass: '',
		source: function(term, response){
			$.getJSON('<?php echo AJAX_PATH?>json_postal_code.php', { search: term, suburb:true, state:false, postal:false}, function(data){ response(data); });
		},
		onSelect: function(e, term, item){
			var re = term.split("-");
			$('input[name="suburb"]').val($.trim(term));
			return true;	                        	
		}
	});
});
</script>
<?php echo $ajax->submitForm(array('form'=>'register','do'=>'add_customer.php', 'get'=>'#sys_message'));?>