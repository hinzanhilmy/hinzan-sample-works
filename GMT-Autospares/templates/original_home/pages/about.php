<!-- Inner page Banner
 ================================================== -->
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>About Us</h1>
    </div>
</div>
<!-- /.inner-page banner -->


<div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->

    <!-- inner-content block
    ================================================== -->
    <div class="inner-content">
        <div class="container-fluid">
            <div class="container">

                <div class="row">
					<div class="col-md-12">
					

<h3>GMT Autospares</h3>
<P>
GMT Autospares is a company based in Melbourne that services the whole of
Australia with the view to expand our services to providing specialised Truck
and heavy industry tools and spare parts to the automotive and transport
industry while providing an outstanding customer service and value for
money to our valued customers.
Our staffs are committed and trained to provide the best service in the
industry to our esteemed customers who rely on our services. Our services are
backed with the guarantee second to none in the industry.
</P>
<h3>Our Aim</h3>
<p>
Our aim is to secure and source the best quality tools for our clients at all
times at the best price.
</p>
<h3>Best price</h3>
<p>
Best price is achieved by negotiating suitable Terms and Conditions that suit
our supplier/s and GMT Autospares at the time of negotiation. Also, our
intention is to buy in bulk – this reduces cost for freight, transport,
administration and time. Hence, we are able to provide the best price for our
customers.
<h5>Note:</h5> if stock is minimal or not available, delivery of the product(s) could take
up to approximately 21 business days.
</p>
<h3>Our Commitment</h3>
<p>
GMT Autospares thrives to provide the best service in the industry by
sourcing quality products on time and with our 12 month guarantee on all
our products for peace of mind to our customers.
Our staff members are professionally trained, friendly and polite and backed
by a team of dedicated managers and administrators including IT services. A
team of IT professionals ensure our system is secure, robust and available at
all times.
</p>
<h3>Disclaimer</h3>
<p>
1. We have made every effort to maintain the accuracy of the
information on this site.
</p>
<p>
2. We do not guarantee of the accuracy of information contained on its
website. While we will take all measures to secure our website and
information provided by you (customers). Any outside hacker activities
are out of our control.
</p>

<p>
3. Details on our website Is only a guideline and general information only
and might not be up to date.
</p>
<p>
4. GMT Autospares does not take any responsibility or injuries resulting
from improper use of tools by untrained or under trained staff
(customers). Sole responsibility lies with the customer for professional
training of their staff for proper use of tools by following available
instructions at all times.
</p>
<p>
5. All prices displayed and specified on this site are in Australian dollars.
</p>					

					</div>
					
				

                </div>

            </div><!-- /.container -->
        </div>
    </div><!-- /.subsidiaries -->

</div>