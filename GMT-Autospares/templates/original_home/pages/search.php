<?php 
	if($_REQUEST['s'] != ""){
		$_SESSION['search-text'] = $fw->xss()->safe($_REQUEST['s']);
	}

	$_SESSION['universal'] = false;

	if($_REQUEST['u'] == "false"){
		$_SESSION['universal'] = false;
	}
	if($_REQUEST['u'] == "true"){
		$_SESSION['universal'] = true;
	}
?>
<!-- Inner page Banner
 ================================================== -->
    <div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
      <div class="container">
      	<h1>Search</h1>
      </div>
    </div>
    <!-- /.inner-page banner -->
    <div class="container-fluid breadcrumb">
        <div class="container">
        	<a href="/">Home</a> 
            	<span class="glyphicon glyphicon-menu-right"></span>Search
            	<span class="glyphicon glyphicon-menu-right"></span><?php  echo $fw->xss()->safe($_REQUEST['s']);?>            	
        </div>
    </div>

    <div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->

        <!-- inner-content block
        ================================================== -->
        <div class="inner-content">

   <div class="container-fluid">
	<div class="row affix-row">
    <?php //include_once('templates/home/pages/login.php');?>
		<div class="col-sm-3 col-md-3 affix-sidebar">
			<section class="search-products" style="margin-bottom:15%;margin-top:4%;margin-left: -5%">			
				<form role="search" class="form-inline" method="get" class="search-form" action="/search.html">
					<div class="col-md-9 col-xs-9">					
						<input type="search"  style="display:inline-block;width:100%" id="search" class="search-field gmt-text" placeholder="Search …" value="" name="s" data-swplive="true" data-swpengine="default" data-swpconfig="default" autocomplete="off">
					</div>
					<div class="col-md-3 col-xs-3">
					<input type="submit" style="display:inline-block" class="search-submit gmt-button" value="Search">	
					</div>
				</form>
			</section>
		
		
			<div class="sidebar-nav">
				<div class="navbar navbar-default" role="navigation">
					<ul class="nav navbar-nav" id="sidenav01">
						<li class="active">
							<a href="#!" data-toggle="collapse"  data-target="#toggleDemo2" data-parent="#sidenav02" style="background-color:#efecec;border-bottom:3px solid #ffffff" >
								<h5>
									<i class="fa fa-sitemap"></i> Browse by Make
								</h5>
							</a>							
							
							<div class="nav-collapse collapse in" id="toggleDemo2" style="height:auto;">
								<ul class="nav nav-list sidebar-class">
									<?php foreach($product_makes as $make){ ?>
										<li><a href="/search.html?u=false&s=<?php echo urlencode($make);?>" id="#toggleCategory"><i class="fa fa-angle-right"></i></i> <?php echo $make;?></a></li>
									<?php } ?>

									
								</ul>
							</div>
							
							
							<a href="#!" data-toggle="collapse"  data-target="#toggleDemo0" data-parent="#sidenav01" style="background-color:#efecec;border-bottom:3px solid #ffffff" >
								<h5>
									<i class="fa fa-sitemap"></i> Browse by Category
								</h5>
							</a>
							<div class="nav-collapse collapse in" id="toggleDemo0" style="height:auto;">
								<ul class="nav nav-list sidebar-class">
									<?php foreach($fw->meta()->getMainCats() as $m){?>
										<li><a href="/sub-category/<?php echo $m['id'];?>/<?php echo strtolower(str_replace(" ", "_", $m['name']));?>.html"><i class="fa fa-angle-right"></i></i> <?php echo $m['name'];?></a>
									<?php } ?>
								</ul>
							</div>
				
						</li>
					</ul>
				</div>
			</div>
		</div> <!-- Column -->


			
				
				<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="row products">
						<!-- Products/ajax/pagination -->
							<div class="pagecontent"></div>
						<div style="clear:both;"></div>	
							<div class="pagination" style="float:right"></div>              		
						<div style="clear:both;"></div>	
					</div>
				</div>
				<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'product_search_list.php','total'=> $fw->products()->search_total()));?>
			  
			  
			</div> <!-- row -->  
            </div><!-- /.container -->	
        </div><!-- /.subsidiaries -->

    </div><!-- /.fadingo -->