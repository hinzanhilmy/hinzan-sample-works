<!-- Inner page Banner
 ================================================== -->
    <div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
      <div class="container">
      	<h1>Search</h1>
      </div>
    </div>
    <!-- /.inner-page banner -->
    <div class="container-fluid breadcrumb"  data-spy="affix" data-offset-top="100">
        <div class="container">
        	<a href="/">Home</a> 
            	<span class="glyphicon glyphicon-menu-right"></span>Search
            	<span class="glyphicon glyphicon-menu-right"></span><?php  echo $fw->xss()->safe($_REQUEST['s']);?>            	
        </div>
    </div>

    <div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->

        <!-- inner-content block
        ================================================== -->
        <div class="inner-content">

          <div class="container-fluid">
            <div class="container">
              <div style="border-bottom: 1px solid;margin-bottom: 20px;">
                  <p>Search results for <b style="font-style: italic;"><?php  echo $fw->xss()->safe($_REQUEST['s']);?> </b></p>
              </div>
              <div class="row item-list products">
              	<?php if(sizeof($fw->products()->search($_REQUEST)) > 0) {?>
					<?php foreach($fw->products()->search($_REQUEST) as $p){?>
	                <div class="col-lg-3 col-sm-4 col-xs-12 box">
	                  <a href="/machinery-detail/<?php echo $p['id'];?>/<?php echo str_replace(" ","-",$p['title']);?>.html" class="item">
	                    <span class="item-w">
	                      <span class="item-img"><img src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE . $p['file'];?>"> </span>
	                      <span class="item-name"><?php echo $p['title'];?></span>
	                    </span>
	                  </a>
	                </div>
					<?php }?>
				<?php } else {?>
					<div class="alert alert-danger">No Records</div>
				<?php }?>	
              </div>
            </div><!-- /.container -->
          </div>
        </div><!-- /.subsidiaries -->

    </div><!-- /.fadingo -->