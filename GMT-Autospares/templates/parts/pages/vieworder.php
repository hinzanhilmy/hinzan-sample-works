<?php
	//this function move to /content/nav.php for cart count issu
	$fw->cart()->is_qoh_exceed($_REQUEST);
?>
<!-- Inner page Banner
 ================================================== -->
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>My Order</h1>
    </div>
</div>
<!-- /.inner-page banner -->


<div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->
    <!-- inner-content block
    ================================================== -->
    <div class="inner-content">
        <div class="container-fluid">
       <div class="container">
       <?php echo $fw->show_session_message();?>
    <div class="row order-detail">
		<div class="well">
        <div class="list-group">
        <?php if(!$_REQUEST['submit_order']){ ?>
        	<?php if(sizeof($fw->cart()->list_cart()) > 0){?>
			        <?php foreach($fw->cart()->list_cart() as $c){?>
			          <?php $total += $c['price'] * $c['qun'];?>
					  <div class="content-order-item">
			          <form method="post" action="/parts/vieworder.html">
			          	  <input type="hidden" name="id" value="<?php echo $c['id'];?>" />
				          <a href="#" class="list-group-item">
				                <div class="media col-md-3">
				                    <figure class="pull-left">
				                        <img class="media-object img-rounded img-responsive" style="height:200px;"  src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE . $c['image'];?>" alt="<?php echo $c['title'];?>" >
				                    </figure>
				                </div>
				                <div class="col-md-3">
				                  <h4 class="list-group-item-heading"> <?php echo $c['title'];?> </h4>
				                    <p class="list-group-item-text"><?php echo $c['desc'];?>                     
				                    </p>
				                </div>
				                <div class="col-md-6">
								
							
									<div class="col-md-3">
										AUD<?php echo $c['price'] * $c['qun'];?>
				                    </div>		                
															
									<div class="col-md-3">
				                    
										<input type="text" name="qun" value="<?php echo $c['qun'];?>" style="width:123px; height:36px; <?php echo ($_SESSION['CART_REMOVE_ITEMS'][$c['id']]!="")? 'border:1px solid red': '';?>" /> 
										<!--  select name="qun">
										<option> Select Quantity </option>
										<?php
		
		//								for ($i = 1; $i <= $c['qoh']; $i++){ ?>
											<option <?php //echo ($c['qun']== $i)? 'selected="selected"' : '';?>><?php echo $i ?></option>
										<?php //} ?>
										</select -->
										
				                    
				                    
									</div>	
									
									
									
									<div class="col-md-6" style="padding-right:15px;">
										<button type="submit" name="action" class="btn btn-default" value="add"> Update </button>
										<button type="submit" name="action" class="btn btn-default" value="remove"> Remove </button>
				                    </div>	
									
									
									
				                </div>    
							
				          </a>
			          </form>
					  </div> <!-- content order item-->
					<?php }?>
			
			
			
			<section style="padding:20px 0px;">
				<?php
					$pureTotal = $total;
					$total = number_format(round($total, 1) , 2);
					$gsp   = number_format( round( ($pureTotal / 100) * GSP , 1) , 2);
					$grandtotal = number_format( round( ($pureTotal + $gsp) , 1) , 2);
				?>
				<div class="row" style="padding:5px 0px;">
					<div class="col-md-12 col-xs-12" style="text-align:right;">
						Subtotal (AUD) <?php echo $total;?>
					</div>
				</div>
				
				<div class="row" style="padding:5px 0px;">
					<div class="col-md-12 col-xs-12" style="text-align:right;">
						GST(AUD) <?php echo $gsp;?>
					</div>
				</div>
				
				<div class="row" style="padding:5px 0px;">
				
					<div class="col-md-12 col-xs-12" style="text-align:right;font-weight:bold">
						<span style="border-bottom: 3px double;border-top: 3px double;"> Total (AUD) <?php echo $grandtotal;?> </span>
					</div>
				</div>
			</section>
			<?php } else {?>
					<div class="alert alert-warning">Empty Cart!. Please choose a <a href='/parts/products.html'>item</a>.</div>
			<?php }?>			
		<?php }?>
	</div>
	<?php if(sizeof($fw->cart()->list_cart()) > 0){?>
				<form method="POST" action="/parts/vieworder.html" id="payment-form" data-eway-encrypt-key="rjCf5XWIgqxLhyuyMno5NH3tv6H6TGNGh/TN53oT7F6XVVWtOlfIzOoS6/CgNH1mjrVs+P60m6wVb+tWRVD1ixVrNlxpe+h8FJZmuF25lpRn7ECAS5PWWayAHaz8whilhIA/IiywspxlzwjJ1tCGlPLkSEbon5Phx+UBdBVYc627311DhIwGWXwL1AOnjPtNTL193JxzJN8/eWfz1iM0mxhzbvJmS0NMITLrdwoAsTEj8o524MoEFlYpU4RruKgFuifMRFTv+frNxkVFxJDh14eoUOE+LmsXpmmuKvi1JOw1p+6QuRAl7t6l+GJHAqq3rbvkoj2PN9ljW6MlPLvXTQ==">
						<div class="row">
							<?php if(!$_REQUEST['ccn']){ ?>
								<div class="row">
									<div class="col-md-12">
										<?php if($_SESSION['CART_EXCEED']){ ?>						
											<label class="alert alert-warning" style="width:100%">
												Highlighted product(s) is currently out of stock. 
												Item on pre order, delivery within apprx. 28 days.
												
												<br />
												<br />							
												<input type="checkbox" name="cart_agree" value="1" /> - I Proceed
											</label>
										<?php }	?>
									</div>
								</div>
								
								
								<div class="row">
									<div class="col-md-12">
										<p style="padding:5px 0px 5px 5px;border-left:5px solid #c40000;">]
										
											Note: We Accept <img src="https://gmtautospares.com.au/templates/parts/img/visa-master.png" style="width:100px; height:auto;display:inline-block;" class="img-responsive">
										</p>
										<br /><br />
									</div>
								</div>
								
								
								
								
								<form action="/pay_en.php" method="POST" data-eway-encrypt-key="rjCf5XWIgqxLhyuyMno5NH3tv6H6TGNGh/TN53oT7F6XVVWtOlfIzOoS6/CgNH1mjrVs+P60m6wVb+tWRVD1ixVrNlxpe+h8FJZmuF25lpRn7ECAS5PWWayAHaz8whilhIA/IiywspxlzwjJ1tCGlPLkSEbon5Phx+UBdBVYc627311DhIwGWXwL1AOnjPtNTL193JxzJN8/eWfz1iM0mxhzbvJmS0NMITLrdwoAsTEj8o524MoEFlYpU4RruKgFuifMRFTv+frNxkVFxJDh14eoUOE+LmsXpmmuKvi1JOw1p+6QuRAl7t6l+GJHAqq3rbvkoj2PN9ljW6MlPLvXTQ==">								
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="exampleInputEmail1">Card Name</label>
												<input type="text" name="name" value="Test" class="form-control"   placeholder="Enter Name">
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label>Card Number</label>
												<input type="text" name="ccn" class="form-control"  data-eway-encrypt-name="ccn" value="4444333322221111" />	
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label>CVV</label>
												<input type="text" class="form-control" name="cvn" data-eway-encrypt-name="cvn" value="123" />	
											</div>
										</div>
									</div>
								
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label>Month</label>
												<input type="text" class="form-control" name="exmonth" value="12" />
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label>Year</label>
												<input type="text" class="form-control" name="exyear" value="26" />	
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="button submit" style="padding-top:27px;">
												<button type="submit" class="btn btn-primary gmt-btton" id="submit">Proceed to Payment</button>
											</div>
										</div>
									</div>
								</form>
								<script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js"></script>
								<!-- ccd form -->									
								</div>
								
							<?php } else {?>		
								<?php if($fw->cart()->send($_REQUEST)){?>
									<div class="col-md-12">
										<h2>Your Order has been submited.</h2>
									</div>
								<?php } else {?>
									<div class="col-md-12 alert warning">
										<h2>Payment Not Recived.</h2>
										<a href="/vieworder.html">View Order</a>
									</div>
									
									
								</div>
								<?php }?>	
							<?php }?>
						</div>
						<script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js"></script>
					</form>
					
	<?php }?>
</div><!-- /.container -->
        </div>
    </div><!-- /.subsidiaries -->

</div>