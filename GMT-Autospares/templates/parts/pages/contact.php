<!-- Inner page Banner
 ================================================== -->
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
  <div class="container">
    <h1>Contact Us</h1>
  </div>
</div>

<div class="container">

	<div class="row">
		<div class="col-lg-6">
			<h3>Our Address</h3>
				<p>
				18A Second Av,
				</br>
				Box Hill North</br>
				  VIC 3152 <br /> Australia
				</p>
		

						<p><span style="
				font-weight: 700;
				color: #404040;
				padding-right: 10px;
			">Tel:</span>1300 048 130
			</p>
			
					<p>
				
				<span style="
				font-weight: 700;
				color: #404040;
				padding-right: 5px;
			">Mobile:</span> 044 7043 832 </p>
	
	
						<p><span style="
				font-weight: 700;
				color: #404040;
				padding-right: 10px;
			">Email:</span>info@gmtautospares.com.au
			</p>
		</div>
		  <div class="col-lg-6" style="padding-top:5px;">
			<!--iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100881.40793921876!2d145.0692139837462!3d-37.80072506759244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad638a0ebdd541d%3A0x28db1abaa6cc9f0!2s18A+Second+Ave%2C+Box+Hill+North+VIC+3129!5e0!3m2!1sen!2sau!4v1518694591796" 
			width="400" height="300" 
			frameborder="0" style="border:0" allowfullscreen></iframe-->
		  </div>
	</div>


		
		
<div class="clearfix" style="padding: 10px 0; margin: 30px 0; border-top: 1px solid;">

</div>


  <?php include_once (SYSTEM_PATH . '3rdparty/recapcha/recaptchalib.php');?>
  
  <div class="row">
  <div class="col-lg-12">
  
    <h3>Make an Inquiry</h3>

	
      <?php if(!$_REQUEST['send_mail']){
		  ?>
        
		<form method="post" action="/contact.html" class="form-class-element" >
		
		

		<div class="form-group">	
          <label>Name</label>
          <input type="text" class="form-control required="required" name="name">
		</div>
		<div class="form-group">	
          <label>Email</label>
          <input type="email" class="form-control required="required" name="email">
          <br />
		</div>
		<div class="form-group">
          <label>Contact No</label>
          <input type="text" class="form-control"required="required" name="phone">
          <br />
		</div>
		<div class="form-group">	
          <label>Message</label>
          <textarea name="message" class="form-control" rows="5"></textarea>
          <br />
		</div>

	  <label>&nbsp</label>
	  <div class="g-recaptcha" data-sitekey="6LdXc0YUAAAAAKyvsvwNNed-9EfVSuVMBZ-x6urg"></div>
	  </br>	

          <input type="submit" name="send_mail" value="Submit">
      </form>
      <?php } else {?>
      		<?php
      			$reCaptcha = new ReCaptcha('6LdXc0YUAAAAAHQqYJi-6X4bc_QVYQRQnTmGJ9K9');
      			$response = $reCaptcha->verifyResponse(
				        $_SERVER["REMOTE_ADDR"],
				        $_POST["g-recaptcha-response"]
				    );
		?>
		
		<?php if ($response != null && $response->success) { ?>
			<?php $fw->mail()->sendInquery($_REQUEST);?>       	
        		<h2 style="text-align: center; font-size: 2rem; background: #d2e9fd; padding: 20px; color: #1074c1;">Thank you for your message.<br>We will get back to you soon.</h2>
		<?php } else {?>
			<h2 style="text-align: center; font-size: 2rem; background: #d2e9fd; padding: 20px; color: red;">Your request not sent!. Please check the fields have been correctly filled in.</h2>
			<h4> <a href="/contact.html">Go Back</a></h4>
		<?php }?>
      <?php }?>

  </div>

</div> <!-- Container -->
</div>

	<script>
		window.onload = function() {
		    var $recaptcha = document.querySelector('#g-recaptcha-response');
		
		    if($recaptcha) {
		        $recaptcha.setAttribute("required", "required");
		    }
		};
	</script>
	<style>
		#g-recaptcha-response {
		    display: block !important;
		    position: absolute;
		    margin: -78px 0 0 0 !important;
		    width: 302px !important;
		    height: 76px !important;
		    z-index: -999999;
		    opacity: 0;
		}
	</style>
  
</div>