<!-- Inner page Banner
 ================================================== -->
    <div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
      <div class="container">
      	<?php $cat = $fw->meta()->getCatNameById($id);?>
        <h1><?php echo $cat['name'];?></h1>
      </div>
    </div>
    <!-- /.inner-page banner -->


    <div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->

    	
        <!-- inner-content block
        ================================================== -->
        <div class="inner-content">
          <div class="container-fluid">
            <div class="container">
			<?php include_once('templates/home/pages/login.php');?>

              <div class="row item-list cat-sub">
              	<?php if(sizeof($fw->meta()->getCatsByParentId($id)) > 0) {?>
					<?php foreach($fw->meta()->getCatsByParentId($id) as $c){?>
	                <div class="col-lg-4 col-sm-4 col-xs-12 box">
	                  <a href="/sub-category/<?php echo $c['id'];?>/<?php echo strtolower(str_replace(" ", "-", $c['name']));?>.html" class="item">
	                    <span class="item-w">
	                      <?php $file = $fw->catagories()->getProductImagesByProductId($c['id']);
	                      	    $file = $file[0]['file'];
	                      ?>
	                      	<?php if($file == ""){?>
	                      		<!-- <img src="<?php echo TEMPLATE_REC_PATH;?>products/sub-cat-img/single-needle-computerized.jpg"> -->
	                      	<?php } else {?>
	                      		<img src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE. $file;?>">
	                      	<?php }?> 
	                      <span class="item-name"><?php echo $c['name'];?></span>
	                    </span>
	                  </a>
	                </div>
					<?php }?>
				<?php } else {?>
					<div class="alert alert-danger">No Records</div>
				<?php }?>
              </div>
            </div><!-- /.container -->
          </div>
        </div><!-- /.subsidiaries -->

    </div><!-- /.fadingo -->