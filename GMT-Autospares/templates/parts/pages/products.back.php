<!-- Inner page Banner
 ================================================== -->
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>Our Products</h1>
    </div>
</div>
<!-- /.inner-page banner -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="container">



		<section class="search-products">
			<div class="row">
				<form role="search" method="get" class="search-form" action="/search.html">
					<div class="col-md-9 col-xs-12">
					
						<input type="search" id="search" style="width:100%;" class="search-field gmt-text" placeholder="Search …" value="" name="s" data-swplive="true" data-swpengine="default" data-swpconfig="default" autocomplete="off">
						
					</div>
					
					<div class="col-md-3 col-xs-12 mobile-only-search">
						<input type="submit" class="search-submit gmt-button" value="Search">
						<button type="button" class="serach-by-cat gmt-button">By Category</button>
				</div>
				</form>
			</div>
		</section>




<div class="row product-listing">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 trigger-toggle-category">
				<div class="panel panel-default">
					<div class="panel-body nested-accordion">
					
					<?php foreach($fw->meta()->getMainCats() as $m){?>
					
					
						<div class="row mt-10">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="panel-group" id="level1-accordion">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">								
												<a data-toggle="collapse" data-parent="#level1-accordion" href="#plast-pro-tree<?php echo $m['id'];?>" class="collapsed">
													<span class="accordian-arrow">
														<i class="fa fa-minus-square-o" aria-hidden="true"></i>
														<i class="fa fa-plus-square-o" aria-hidden="true"></i>
													</span> 
													<?php echo $m['name']?>
												</a>
											</h4>
										</div>
										<div id="plast-pro-tree<?php echo $m['id'];?>" class="panel-collapse collapse">
											<div class="panel-body">												
												<ul class="list-unstyled">
													<?php foreach($fw->meta()->getCatsByParentId($m['id']) as $s){?>
													<li>
														<li>
															<a href="/sub-category/<?php echo $s['id'];?>/<?php echo strtolower(str_replace(" ", "_", $s['name']));?>.html"><?php echo $s['name'];?></a>	
														<?php foreach($fw->meta()->getCatsByParentId($s['id']) as $super){?>
														<ul style="padding-left:10px;">
															<li style="list-style-type:none;">
																<a href="/sub-category/<?php echo $super['id'];?>/<?php echo strtolower(str_replace(" ", "_", $super['name']));?>.html"><?php echo $super['name'];?>
																</a>
															</li>
														</ul>
														<?php }?>		
														</li>
													</li>
													<?php }?>	
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
							
					<?php }?>			

					
					</div>
					
					
				</div>
			</div>
		</div>

</div>




            <div class="container">
			
              <?php //include_once('templates/home/pages/login.php');?>
			  
              <div class="row item-list products">
              	<?php if(sizeof($fw->products()->get_productsby_cat($id)) > 0) {?>
					<?php foreach($fw->products()->get_productsby_cat($id) as $p){?>
	                <div class="col-lg-4 col-sm-4 col-xs-12 box">
	                  <a href="/machinery-detail/<?php echo $p['id'];?>/<?php echo str_replace(" ","-",$p['title']);?>.html" class="item">
	                    <span class="item-w">
	                      <span class="item-img"><img src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE . $p['file'];?>"> </span>
	                      <span class="item-name"><?php echo $p['title'];?></span>
	                    </span>
	                  </a>
	                </div>
					<?php }?>
				<?php } else {?>
					<div class="alert alert-danger">No Records</div>
				<?php }?>	
              </div>
            </div><!-- /.container -->


	
<script>
	//reference
    //https://bootsnipp.com/snippets/featured/bootstrap-30-treeview 
	$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});



//Initialization of treeviews
<?php foreach($fw->meta()->getMainCats() as $m){?>
	$('#tree<?php echo $m['id'];?>').treed({openedClass:'glyphicon-chevron-right', closedClass:'glyphicon-chevron-down'});
<?php }?>


$( ".serach-by-cat" ).click(function() {
  $(".trigger-toggle-category").toggleClass("show-catgory");
});


</script>
