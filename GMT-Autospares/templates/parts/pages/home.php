<div id="fadingo-remove"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->
    <!-- search and where blocks
    ================================================== -->
    <div class="container-fluid full-strech">

            <div class="col-lg-12 col-md-12 h-b color-block bg-orange search-area">
                <div class="col-lg-12 col-md-12">
                    <h2 class="title">Search All Products</h2>
                    <form role="search" method="get" class="search-form" action="/search.html">
                        <label style="width:50%">
                            <span class="screen-reader-text">Search for:</span>
                            <input type="search" id="search" style="width:100%;" class="search-field" placeholder="Search …" value="" name="s" data-swplive="true" data-swpengine="default" data-swpconfig="default" autocomplete="off">
							<span class="icon">
							  <svg xmlns="http://www.w3.org/2000/svg" width="192.52" height="192.52" viewBox="3.74 3.74 192.52 192.52"><path d="M194.1 183.66l-54.8-54.78c11.3-13.68 17.48-30.86 17.48-48.62 0-42.2-34.32-76.52-76.52-76.52-42.2 0-76.52 34.33-76.52 76.52 0 42.2 34.33 76.52 76.52 76.52 17.7 0 34.84-6.2 48.54-17.47l54.86 54.8c1.4 1.4 3.25 2.17 5.2 2.17 1.98 0 3.83-.77 5.23-2.17 1.4-1.4 2.15-3.26 2.15-5.23 0-1.96-.77-3.82-2.17-5.22zM80.25 18.56c34.03 0 61.7 27.68 61.7 61.7s-27.67 61.7-61.7 61.7-61.7-27.67-61.7-61.7 27.68-61.7 61.7-61.7z"/></svg>
							</span>
                            <input type="submit" class="search-submit" value="Search">
                        </label>

                    </form>
                </div>
            </div>
    </div>
	
	<!-- New arrivals block
    ================================================== -->
    <div class="container-fluid h-b new-arrivals">
        <div class="container">
            <h1 class="title">New arrivals</h1>
            <div class="owl-carousel owl-theme">
                <?php foreach($fw->products()->new_arivals() as $n){?>
                    <div class="item">
                        <a href="/machinery-detail/<?php echo $n['id'];?>/<?php echo str_replace(" ","-",$n['title']);?>.html"><span class="n-r-img"><img src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_THUMB . $n['file'];?>"></span></a>
                        <h4>
                            <a href="#">
                                <?php $cat = $fw->meta()->getCatNameById($n['catagories_id']);?>
                                <span><?php echo $cat['name'];?></span>
                                <?php echo $n['title'];?>
                            </a>
                        </h4>
                    </div>
                <?php }?>
            </div>
        </div>
    </div><!-- /.new-arrivals -->
	
	
       


      <!-- The Company block
      ================================================== -->
      <div class="container-fluid h-b color-block company bg-blue">
        <div class="container">
          <h2 class="title">The Company</h2>
          <p class="col-lg-11" style="float: none; margin: 0 auto;">
            GMT is a company based in Melbourne that services Nationwide with the view to expand our services in providing tools and spare parts to the automotive and 
					transport industry in Australia providing an outstanding service to our valued customers.
					</p>
				
					<br />
					<p>
					Our staffs are committed and trained to provide the best service in the industry to our customers who rely on our services. 

Our services are backed with the guarantee second to none in the industry
            
          </p>
          <a href="/about.html" class="btn">More details <span class="link-icon"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="-5.29 -3.34 18 18"><path d="M4.8 5.67l-3 2.98c-.3.3-.3.78 0 1.08.3.3.8.3 1.08 0L6.4 6.2c.3-.28.3-.76 0-1.06l-.04-.04-3.5-3.5c-.16-.16-.35-.24-.55-.24-.16 0-.35.08-.5.23-.3.26-.3.74 0 1.04l3 3z"/><path d="M3.7 14.66c4.97 0 9-4.04 9-9 0-4.97-4.03-9-9-9-4.95 0-9 4.03-9 9 0 4.96 4.05 9 9 9zm0-16.5c4.15 0 7.5 3.36 7.5 7.5s-3.35 7.5-7.5 7.5-7.5-3.37-7.5-7.5 3.38-7.5 7.5-7.5z"/></svg></span></a>
        </div>
      </div><!-- /.company -->



    </div><!-- /.fadingo -->