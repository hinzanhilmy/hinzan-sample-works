    <script src="<?php echo JS_PATH;?>jquery.twbsPagination.js"></script>
    
 	<?php $link = array(0=> 'category', 1=> 'sub-category');?>
    <div class="container-fluid banner box" style="background-image: url('<?php echo TEMPLATE_REC_PATH;?>img/banner-1.jpg');background-position: 0 -120px;">
      <div class="container">
      		<?php if($_SESSION['SEARCH']['cat'] == 1){?>
      			<h1>DRESS SPARE PARTS</h1>    
      		<?php } else {?>
      			<h1>LEATHER SPARE PARTS</h1>    
      		<?php }?>
      </div>
    </div>
    
    <!-- /.inner-page banner -->
    <div class="container-fluid breadcrumb"  data-spy="affix" data-offset-top="100">
        <div class="container">
        	<a href="/">Home</a> 
        		<span class="glyphicon glyphicon-menu-right"></span>Spare Parts
        </div>
    </div>

    <div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->
        <!-- inner-content block ================================================== -->
        <div class="inner-content">
          <div class="container-fluid">
            <div class="container">
              <div class="row m-details box">
                <div class="col-lg-7">
                  <form class="form-item dashboardform" name="trd_list">
						<div class="row">
						<section class="dashboard-search clearfix">			
						  <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12"><input class="form-text form-control" value="" type="text" name="name" placeholder="Name | Model No" /></div>
						  <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 submit"><input type="submit" class="btn green" value="Search" name="search" /></div>
			
						</section>
						</div>
					 </form>
					 
					 <hr />
					 <div class="pagecontent">Loading..</div>
				     <div class="pagination"></div>
				     <?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'tradelist.php','total'=> $fw->traders()->count()));?>
                </div>
              </div>
            </div><!-- /.container -->
          </div>
        </div><!-- /.subsidiaries -->

    </div><!-- /.fadingo -->
    
    
    <script>
		$(document).ready(function (){
			$.post('<?php echo AJAX_PATH;?>do_session.php', $(this).serialize());
			$('form[name="trd_list"]').submit(function (data){
					$.ajax({
							type: 'post',
							url : '<?php echo AJAX_PATH;?>do_session.php',
							data: $(this).serialize(),
							dataType: 'html',
							success: function (data){ location.reload();}
					});
					return false;
			});
		});
	</script>