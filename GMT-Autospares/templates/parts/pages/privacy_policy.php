<!-- Inner page Banner
 ================================================== -->
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>Privacy Policy</h1>
    </div>
</div>
<!-- /.inner-page banner -->


<div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->

    <!-- inner-content block
    ================================================== -->
    <div class="inner-content">
        <div class="container-fluid">
            <div class="container">

                <div class="row">
					<div class="col-md-12">
					
						

      <p>This Privacy Policy describes how we collect and use information provided through our website.
By visiting our Website, you consent us to collect, use, store, delete and disclosure of your information to relative parties and government departments and authorities without your consent.
Our Privacy Policy is effective form the date last modified – Please see modified date on the website. Privacy Policy will be modified anytime without any notification to any parties in advance. 
Privacy modifications will be effective from the date of published on our website.
</p>
	<h3>Collection of Personal Information</h3>
      <p>By accepting this Privacy Policy you expressly consent to our use and disclosure of your personal information in the manner prescribed in this Privacy Policy. This Privacy Policy is incorporated into and subject to the terms of the Terms and Conditions. Subject to the requirements of applicable local law, we strive to provide a consistent set of privacy practices.</p>

      <h3>Information We Collect</h3>
nal emails may be sent to you from us. Users have the option to unsubscribe from the newsletter at any time and at their sole discretion.</p>
<p>Following Information you voluntarily provide by visiting our web Site will be stored securely.</p>
<ol>
	<li>Name</li>
	<li>Address details</li>
	<li>Email addresses</li>
	<li>Phone contact numbers</li>
	<li>Other information provided by you</li>
</ol>
<p>
We will not be requesting or storing information such as</p>
<ol>
	<li>Bank information</li>
	<li>Credit and Debit card information or account details</li>
	<li>Financial details or affiliated information</li>
	<li>Health related information</li>
</ol>
<h3>Cookies</h3>
<p>
	Cookies are used in the Website to make your online experience more convenient and enjoyable. Cookies are small bits of electronic information that are stored on your computer about your visit to the Website.
	No personal information is received or recorded in the cookies used in our Website.
</p>
    
	<h3>
		Log file information
	</h3> 
<p>
The Webhost server automatically records information that your browser sends whenever you visit our Website site. 
Information recorded includes are your Internet Protocol address, your browser type and version, and the duration of the visit to our Website. This information is used to monitor and analyse user behaviours and patterns.
Messages
</p>

<p>
When you send messages through the Site, E.g. Product and information enquires etc., the content of your messages could be used and retained in order to process your request.
Use of Your Personal Information
You personal information that is collected through the Site will not be used to send you unsolicited emails or other messages. Promotional materials that are relevant to you might be sent to you only with your consent. 

</p>

<h3>Third Parties Who Receive Your Information</h3>
<p>

When you provide information through the Site, this information is transmitted through the Internet and stored on servers owned and maintained by a third party service provider/s. This service provider has access to your information stored on these servers. In some cases, a separate company maintains and operates the Site and, under these circumstances, this company also will have access to your information stored on the servers hosting the Site.
</p>

<h3>
Sharing of Information
</h3>
<p>
Your personal information will not be shared with any third parties without your consent, except,
</p>
	
	<ol>
	<li>As necessary to respond to a request or inquiry that you have submit through our Website. </li>
	<li>To comply with local, federal and International law enforcement agencies and regulations, legal processes, and or a request by law enforcement or governmental authorities.</li>
	<li>To protect the rights, property or safety of others and  </li>
	<li>If the business associated with the Site (including your personal information collected through the Site) is sold through a sale of stock or assets.</li>
</ol>
	
	
	<h3>Security</h3>
	<p>
The personal information that you provide is stored on our contractors servers, which are located in secure facilities with restricted access, and protected by protocols and procedures designed and guaranteed by our contractor/s to ensure the security of this information. However, no server, computer or communications network or system, or data transmission over the Internet is guaranteed to be secure due to illegal hacker activities. Therefore, any information you transmit and provide through our Website is at your own risk.

</p>

	<h3>Warranty Policy</h3>
	<p>
		Goods and Services warranty: 
	</p>
	<p>
		The standard warranty period commences from date of purchase and warranty is limited to manufacturer warranty at all times and is limited to manufacturers design, faults and workmanship and warranty does not apply for usual wear and tear. Warranty is limited to12 months on tools sold by GMTAutospares.
		Any variation to above mentioned warranty must be discussed and agreed by GMTAutospares management in writing. 
		<br /><br />
		Please note that warranty does not include freight charges, labor costs and travel expenses incurred by the purchaser. Items claimed to be defective are to be returned at the purchase’s own risk and freight paid for by the purchaser. No partial or full responsibility is taken by GMTAutospares management or their subsidiaries for loss or damages occurred during return of the product(s). 
		The warranty will be deemed null and void, if correct operation and maintenance procedure of the product as instructed in the operations manuals are not followed. 
		<br /><br /> Warranty does not include or cover any consequential losses or damages resulting from failure of parts or equipments.Warranty becomes immediately null and void If/when the purchaser makes any design changes (fully or partially) to the product or uses non-genuine or used replacement parts.
		GMTAutospares complies with Australian Consumer Law warranty policy at all times.
	</p>
	
	
	 
	<h3>GMT delivery policy</h3>
	<p>
	Products purchased and paid via online are dispatched to clients via courier services within Australia. Delivery could take up to 3 business days for interstate. Products are dispatched from our warehouse based in Melbourne. Dispatches within greater Melbourne\ CBD and suburbs might be same day (subject to confirmation). 
	Occasionally, dispatches can be originated by our suppliers from overseas. In such cases, delivery period can be up to 28 business days.
	</p>
	
<p>	
Deliver partners:
<br/><br/>
We have partnered with following delivery service organizations for prompt and excellent service to our customers.
TNT Couriers , 
Australia Post and 
Courier Please

 
Services: 
<br/><br/>
Once product\s are dispatched customers can track their orders.
 
Payments:
<br/><br/>
Please note that products will only be organized for dispatches once payment is made in full via Pay pal, Epay or via credit card (by telephone).  
Important:
<br/><br/>

<ol>
	<li>GMT Autospares will endeavor to deliver products promptly. However, are not responsible for delays in delivery by the courier and other unforeseen circumstances including lost or stolen items and products that are out of control of GMT Autpspares. Customers are advised to contact the delivery partner directly for information if\when delivery is delayed. </li>
	<li>All orders dispatched require a signature of receipt at the address of collection. GMT Autospares will not take request\s that products are to be left at outside of the address (premises). </li>
	<li>GMT are not responsible for any charges incurred (imposed by the courier companies) by non collection at the time of first delivery attempt.  </li>
</ol>

	






 
					

					</div>
					
				

                </div>

            </div><!-- /.container -->
        </div>
    </div><!-- /.subsidiaries -->

</div>