<?php
	//this function move to /content/nav.php for cart count issu
	//$fw->cart()->add($_REQUEST);
?>
<!-- Inner page Banner
 ================================================== -->
<div class="container-fluid banner box" style="background-image: url('img/banner-1.jpg');background-position: 0 -120px;">
    <div class="container">
        <h1>My Order</h1>
    </div>
</div>
<!-- /.inner-page banner -->


<div id="fadingo"><!-- fadingo for page on scroll element fade in. All elements to fade in goes in here with .hideme -->
    <!-- inner-content block
    ================================================== -->
    <div class="inner-content">
        <div class="container-fluid">
       <div class="container">
       <?php echo $fw->show_session_message();?>
    <div class="row order-detail">
		<div class="well">
        <div class="list-group">
        <?php if(!$_REQUEST['submit_order']){ ?>
        	<?php if(sizeof($fw->cart()->list_cart()) > 0){?>
			        <?php foreach($fw->cart()->list_cart() as $c){?>
			          <?php $total += $c['price'] * $c['qun'];?>
			          <form method="post" action="/vieworder.html">
			          	  <input type="hidden" name="id" value="<?php echo $c['id'];?>" />
				          <a href="#" class="list-group-item">
				                <div class="media col-md-3">
				                    <figure class="pull-left">
				                        <img class="media-object img-rounded img-responsive" style="height:200px;"  src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_LARGE . $c['image'];?>" alt="<?php echo $c['title'];?>" >
				                    </figure>
				                </div>
				                <div class="col-md-3">
				                  <h4 class="list-group-item-heading"> <?php echo $c['title'];?> </h4>
				                    <p class="list-group-item-text"><?php echo $c['desc'];?>                     
				                    </p>
				                </div>
				                <div class="col-md-6">
								
							
									<div class="col-md-3">
										AUD<?php echo $c['price'] * $c['qun'];?>
				                    </div>		                
															
									<div class="col-md-3">
				                    
										<input type="text" name="qun" value="<?php echo $c['qun'];?>" style="width:123px; height:36px" /> 
										<!--  select name="qun">
										<option> Select Quantity </option>
										<?php
		
		//								for ($i = 1; $i <= $c['qoh']; $i++){ ?>
											<option <?php //echo ($c['qun']== $i)? 'selected="selected"' : '';?>><?php echo $i ?></option>
										<?php //} ?>
										</select -->
										
				                    
				                    
									</div>	
									
									
									
									<div class="col-md-6" style="padding-right:15px;">
										<button type="submit" name="action" class="btn btn-default" value="add"> Update </button>
										<button type="submit" name="action" class="btn btn-default" value="remove"> Remove </button>
				                    </div>	
									
									
									
				                </div>    
							
				          </a>
			          </form>
					<?php }?>
			
			
			
			<section style="padding:20px 0px;">
				<?php
					$pureTotal = $total;
					$total = number_format(round($total, 1) , 2);
					$gsp   = number_format( round( ($pureTotal / 100) * GSP , 1) , 2);
					$grandtotal = number_format( round( ($pureTotal + $gsp) , 1) , 2);
				?>
				<div class="row" style="padding:5px 0px;">
					<div class="col-md-12 col-xs-12" style="text-align:right;">
						Subtotal (AUD) <?php echo $total;?>
					</div>
				</div>
				
				<div class="row" style="padding:5px 0px;">
					<div class="col-md-12 col-xs-12" style="text-align:right;">
						GST(AUD) <?php echo $gsp;?>
					</div>
				</div>
				
				<div class="row" style="padding:5px 0px;">
				
					<div class="col-md-12 col-xs-12" style="text-align:right;font-weight:bold">
						<span style="border-bottom: 3px double;border-top: 3px double; " "> Total (AUD) <?php echo $grandtotal;?> </span>
					</div>
				</div>
			</section>
			<?php } else {?>
					<div class="alert alert-warning">Empty Cart!. Please choose a <a href='/products.html'>item</a>.</div>
			<?php }?>			
		<?php }?>
	</div>
	<?php if(sizeof($fw->cart()->list_cart()) > 0){?>
	<form method="get" action="/vieworder.html">
		<div class="row">
			<?php if(!$_REQUEST['submit_order']){ ?>
				<div class="col-md-offset-9 col-md-3">
					<button type="submit" class="btn btn-default btn-lg btn-block" name="submit_order" value="true"> Submit Your Order </button>
				</div>
			<?php } else {?>				
				<?php $fw->cart()->send();?>
				<div class="col-md-offset-9 col-md-3">
					<h2>Your Order has been submited.</h2>
				</div>
			<?php }?>
		</div>
	</form>
	<?php }?>
</div><!-- /.container -->
        </div>
    </div><!-- /.subsidiaries -->

</div>