<!-- Slider Banner================================================== -->

    <div class="h-b-s clearfix container-fluid" id="container">
      
	  
	  <!--
	  <div id="next" alt="Next" title="Next">
        <div class="arrow-right"></div>
      </div>
      <div id="prev" alt="Prev" title="Prev">
        <div class="arrow-left"></div>
      </div>

       <!-- <div id="slider"> 

            <div class="slide" style="background-image: url('<?php echo TEMPLATE_REC_PATH;?>img/truck03.jpg');">
                <div class="slide-copy">
                    <div class="copy-wraper">
                        <div class="copy-container">
                            <div class="container text-center">
                                <h1 style="display:inline-block; padding:5px" class="gmt-red-white-div">Spare Parts</h1>
                                <p>QUALITY TOOLS FOR SERIES TRADES</p>
                            </div>
                        </div>
                    </div>
                 </div>
                <div class="img1"></div>
            </div>

            <div class="slide" style="display: none; background-image: url('<?php echo TEMPLATE_REC_PATH;?>img/truck02.jpg');">
                <div class="slide-copy">
                    <div class="copy-wraper">
                        <div class="copy-container">
                            <div class="container text-center">
								<h1 style="display:inline-block; padding:5px" class="gmt-red-white-div">Spare Parts</h1>
                                <p>QUALITY SPECIALITY TOOLS AND PARTS WITH PROMPT DELIVERY</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="img2"></div>
            </div>

            <div class="slide" style="display: none; background-image: url('<?php echo TEMPLATE_REC_PATH;?>img/truck01.jpg');">
                <div class="slide-copy">
                    <div class="copy-wraper">
                        <div class="copy-container">
                            <div class="container text-center">
                                <h1 style="display:inline-block; padding:5px" class="gmt-red-white-div">Spare Parts</h1>
                                <p>Excellent Service Gurantee</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="img3"></div>
            </div>




        </div>
		-->
	
		
    </div>

		<div class="container" style="padding-top:30px;">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="alert alert-info" role="alert">
						we are updating to serve you better. Please do not buy anyting!
					</div>	
				</div>
			</div>
			
		
		
		</div>

    <!-- /.slider banner -->

<!--Slider banner js
    ======================-->
<script>

  $(document).ready(function(){
    // options
    var speed = 500; //transition speed - fade
    var autoswitch = true; //auto slider options
    var autoswitch_speed = 8000; //auto slider speed

    // add first initial active class
    $(".slide").first().addClass("active");

    // hide all slides
    $(".slide").hide;

    // show only active class slide
    $(".active").show();

    // Next Event Handler
    $('#next').on('click', nextSlide);// call function nextSlide

    // Prev Event Handler
    $('#prev').on('click', prevSlide);// call function prevSlide

    // Auto Slider Handler
    if(autoswitch == true){
      setInterval(nextSlide,autoswitch_speed);// call function and value 4000
    }

    // Switch to next slide
    function nextSlide(){
      $('.active').removeClass('active').addClass('oldActive');
      if($('.oldActive').is(':last-child')){
        $('.slide').first().addClass('active');
      } else {
        $('.oldActive').next().addClass('active');
      }
      $('.oldActive').removeClass('oldActive');
      $('.slide').fadeOut(speed);
      $('.active').fadeIn(speed);
    }

    // Switch to prev slide
    function prevSlide(){
      $('.active').removeClass('active').addClass('oldActive');
      if($('.oldActive').is(':first-child')){
        $('.slide').last().addClass('active');
      } else {
        $('.oldActive').prev().addClass('active');
      }
      $('.oldActive').removeClass('oldActive');
      $('.slide').fadeOut(speed);
      $('.active').fadeIn(speed);
    }
  });

</script>
<!--Slider banner js END ==========-->