<?php include ('../../../system/main.php'); ?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'  && $_SESSION['SUSERTYPE'] != "DATAENTRY"){  echo json_encode(array('status'=>FALSE,'message'=> 'Please use Administrative Login.')); exit();}?>
<?php 
	if($fw->products()->edit($_REQUEST)){
		$id = $_REQUEST['id'];
		$fw->set_session_message(array('text'=> 'Done.', 'type'=>TRUE));
		echo json_encode(array('status'=>TRUE,'message'=> 'Done !','jredirect'=> true, 'jredirecturl'=> HTTP_PATH . 'tools/lpanel/edit_product.html?id=' . $id));
	} else {
		echo json_encode(array('status'=>TRUE,'message'=> 'Fail !','jredirect'=> true, 'jredirecturl'=> HTTP_PATH . 'tools/lpanel/edit_product.html?id=' . $id));
	}
?>