<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $userdetails = $fw->users()->get($_REQUEST);?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit User <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>

<form role="form" class="form-group-sm" style="width:450px;" method="post" name="edituser">
	<input type="hidden" value="<?php echo $_REQUEST['id'];?>" name="id" />
	<p>Full Name</p>
	<p> <?php echo $ui->input_text(array('name'=>'fullname','value'=>$userdetails['fullname'],'hint'=>'User Full Name','class'=>'required form-control'));?></p>
	<p>User Name</p>
	<p> <?php echo $ui->input_text(array('name'=>'username','value'=>$userdetails['username'],'hint'=>'User Name','class'=>'required form-control'));?></p>
	<p>Password</p>
	<p> <?php echo $ui->input_text(array('name'=>'password','value'=>'','hint'=>'Password', 'class'=>'form-control'));?></p>

	<p>Account Type</p>
	<p>
		<input type="radio" value="ADMIN" name="accountrype" <?php echo ($userdetails['account_type']=='ADMIN')? 'checked="checked"' : '';?>> Administrator  <br />
		<input type="radio" value="USER" name="accountrype"  <?php echo ($userdetails['account_type']=='USER')? 'checked="checked"' : '';?>> User <br />
		<input type="radio" value="NOLOG" name="accountrype" <?php echo ($userdetails['account_type']=='NOLOG')? 'checked="checked"' : '';?>> No Access <br />
	</p>

	<p> <?php echo $ui->input_button_primary(array('name'=>'AddUser','type'=>'Submit','value'=>'Edit User'));?></p>
</form>
<?php echo $ajax->submitForm(array('form'=>'edituser','get'=>'#sys_message', 'do'=>'do_edituser.php'));?>