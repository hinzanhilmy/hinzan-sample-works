<?php include ('../../../../system/main.php'); ?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>Order ID#</th>
			<th>Date And Time</th>
			<th>Customer Details</th>
			<th>Total Items</th>
			<th>Invoice Status</th>
			<th><img src="https://cdn-images-1.medium.com/max/1209/1*QV850NqOX36GFidUDZgs8w.png" style="width:100px;" /></th>
			<td>&nbsp;</td>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->cart()->orders_pagination($next) as $d){?>
			<tr>
				<td><?php echo $d['id'];?></td>
				<td><?php echo $d['date'];?></td>
				<td><?php echo $fw->users()->getbyid(array('id'=> $d['user_id']) )->fullname;?>
					<br />
					<small><?php echo $fw->users()->getbyid(array('id'=> $d['user_id']) )->email;?></small>
				</td>
				<td><?php echo $d['total_count'];?></td>
				<td align="center">
					<?php if($d['status'] == 'REJECTED'){?>
						<label class="btn btn-danger"><?php echo $d['status'];?></label>
					<?php } else if ($d['status'] == 'INVOICED'){?>
						<label class="btn btn-success"><?php echo $d['status'];?></label>
					<?php } else {?>
						<label class="btn btn-warning"><?php echo $d['status'];?></label>
					<?php }?>
				</td>
				<td>
					<?php echo $d['eway_message'];?>
				</td>
				<td align="center"><a class="btn btn-primary" href="/tools/lpanel/customer_modify_order.html?id=<?php echo $d['id'];?>&userid=<?php echo $d['user_id']?>">View</a></td>
			</tr>
			<?php }?>
		</tbody>
</table>
<?php //echo $ajax->fancyBoxByClass('fancybox');?>