<?php include ('../../../../system/main.php'); ?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>#</th>
			<th>Topic</th>
			<th>&nbsp;</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->news()->pagination($next) as $d){?>
			<tr>
				<td><?php echo $d['id'];?></td>
				<td><?php echo $d['topic'];?></td>
				<td>
					<a class="btn btn-success fancybox fancybox.ajax" href="<?php echo AJAX_PATH?>ui_do_editknow.php?id=<?php echo $d['id'];?>">Edit</a>
					<a title="Do you want to delete knowledge base article?" class="delete btn btn-danger" href="id=<?php echo $d['id'];?>">Delete</a>
				</td>
			</tr>
			<?php }?>
		</tbody>
</table>
<?php echo $ajax->link_post(array('class'=>'delete','do'=>'do_delete_article.php', 'get'=>'#sys_message'))?>
<?php echo $ajax->fancyBoxByClass('fancybox');?>