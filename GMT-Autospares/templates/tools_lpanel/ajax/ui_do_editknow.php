<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $news = $fw->news()->get($_REQUEST);?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit Knowledge <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>

<form role="form" class="form-group" method="post" name="edituser">
	<input type="hidden" value="<?php echo $_REQUEST['id'];?>" name="id" />
	<p> <?php echo $ui->input_button_primary(array('name'=>'AddUser','type'=>'Submit','value'=>'Save'));?></p>

	<p>Language</p>
	<p> 
	  	<select name="lng" class="form-control" class="required">
			<option value="">-Select Language-</option>
			<?php foreach($fw->meta()->get() as $m){?>
			<option value="<?php echo $m['id']?>" <?php echo ($news['meta_lng'] == $m['id'])? 'selected="selected"' : '';?>><?php echo $m['name'];?></option>
			<?php }?>
		</select>
	</p>
	<p>Title</p>
	<p> <?php echo $ui->input_text(array('name'=>'title','value'=>$news['topic'],'hint'=>'User Name','class'=>'required form-control'));?></p>
	
	<p>Title</p>
	<p> <textarea name="description" class="input-group" rows="6" cols="30"><?php echo $news['description'];?></textarea></p>

		<div class="form-group input-group">
			  		<ul class="thumbnails" id="edit_files">
		  				<?php foreach($fw->news()->getimagebyid($id) as $image){?>
		  					<li>
		  						<label><?php echo $image['image'];?></label>
		  						<input type="checkbox" name="update_delete[<?php echo $image['id'];?>]" /> Delete
		  					</li>
		  				<?php }?>
			  		</ul>
			  		<div style="clear:both;"></div>
					<input type="file" name="upload" id="edit_upload" />	
		</div>		
	<p> <?php echo $ui->input_button_primary(array('name'=>'AddUser','type'=>'Submit','value'=>'Save'));?></p>
		
			
</form>
<?php echo $ajax->submitForm(array('form'=>'edituser','get'=>'#sys_message', 'do'=>'do_editknow.php'));?>
<?php echo $ajax->uploadify('edit_upload','edit_files');?>