<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>Product</th>
			<th>Date</th>
			<th>Views</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->audit()->get_row($id) as $d){?>
			<?php $product = $fw->products()->loadProduct($d['product_id']);?>
			<tr>
				<td>
					<a target="blank" href="<?php echo HTTP_PATH;?>machinery-detail/<?php echo $d['product_id'];?>/<?php echo str_replace(" ","-",$product[0]['title']);?>.html" class="item">
					<?php echo $product[0]['title'];?>
					</a>
				</td>
				<td><?php echo $d['date'];?></td>
				<td><?php echo $d['count'];?></td>
			</tr>
			<?php }?>						
		</tbody>
</table>