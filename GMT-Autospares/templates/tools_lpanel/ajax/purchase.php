<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $p = $fw->products()->get_edit($_REQUEST['productid']);?>

<h3>Order</h3>
<hr />

<form method="post" name="order">
	<input type="hidden" value="<?php echo $_REQUEST['productid'];?>" name="productid" />
	<input type="hidden" value="<?php echo $_REQUEST['v'];?>" name="vendorid" />
	<p>Quantity</p>
	<p> 
		<input type="number" class="form-control" value="<?php echo $p['re_orderlevel'];?>" name="qun" />
	</p>
	<p> <?php echo $ui->input_button_primary(array('name'=>'EditMeta','type'=>'Submit','value'=>'Order'));?></p>
</form>
<?php echo $ajax->submitForm(array('form'=>'order','get'=>'#sys_message', 'do'=>'do_purchase.php'));?>