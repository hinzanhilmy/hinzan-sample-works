<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>Date</th>
			<th>IP Address</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->audit()->no_of_logins($id) as $d){?>
			<tr>
				<td><?php echo $d['date_time'];?></td>
				<td><?php echo $d['ip'];?></td>
			</tr>
			<?php }?>						
		</tbody>
</table>