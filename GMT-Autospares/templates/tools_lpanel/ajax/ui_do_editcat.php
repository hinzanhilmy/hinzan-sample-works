<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $c = $fw->catagories()->pagination(0,$id); $editmeta = $editmeta[0]; $c = $c[0];?>

<h3>Edit</h3>
<hr />
<form method="post" name="editmeta">
	<input type="hidden" value="<?php echo $_REQUEST['id'];?>" name="id" />
	<?php if($c['parent'] != 0){?>
	<div class="form-group">
		<label>Change Category</label>
  		<select class="form-control" name="maincatid">
			<option value="">-select-</option>
			<?php foreach($fw->meta()->getMainCats(0,1) as $ch){?>
				<option value="<?php echo $ch['id'];?>" <?php echo ( $ch['id'] == $c['parent'])? 'selected="selected"' : '';?> ><?php echo $ch['name'];?></option>
			<?php }?>
		</select>
    </div>    
    
	<?php }?>    	
	<p>
		<input class="form-control" type="text" name="name" value="<?php echo $c['label'];?>" />
	</p>

	<p> <?php echo $ui->input_button_primary(array('name'=>'EditMeta','type'=>'Submit','value'=>'Edit'));?></p>
</form>
<?php echo $ajax->submitForm(array('form'=>'editmeta','get'=>'#sys_message', 'do'=>'do_editcat.php'));?>
<?php echo $ajax->uploadify_single('edit_upload','edit_files');?>