<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>Sales Order Id</th>
			<th>Customer</th>
			<th>Qun</th>
			<th>Date Order</th>
			<td>&nbsp;</td>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->purchase()->order_history($id) as $d){?>
			<tr>
				<td>
					<?php echo $d['order_id'];?> <br />
				</td>
				<td>
					<?php echo $d['fullname'];?> <br />
					<small><?php echo $d['email'];?></small>
				</td>
				<td><?php echo $d['qun'];?></td>
				<td><?php echo $d['order_date'];?></td>
				<td><a href="/lpanel/customer_modify_order.html?id=<?php echo $d['order_id'];?>&userid=<?php echo $d['user_id'];?>">Sales Order</a></td>
			</tr>
			<?php }?>						
		</tbody>
</table>