<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php 
	if($fw->vendor()->order($_REQUEST)){
			$q = http_build_query($_SESSION['temp_request']);
			$fw->set_session_message(array('text'=> 'Done !', 'type'=>TRUE));
			echo json_encode(array('status'=>TRUE,'message'=> 'Done !','jredirect'=> true, 'jredirecturl'=> HTTP_PATH . 'tools/lpanel/purchase_orders.html?' . $q));
	} else {
			echo json_encode(array('status'=>TRUE,'message'=> 'Fail !','jredirect'=> true, 'jredirecturl'=> HTTP_PATH . 'tools/lpanel/purchase_orders.html?' . $q));
	}
?>