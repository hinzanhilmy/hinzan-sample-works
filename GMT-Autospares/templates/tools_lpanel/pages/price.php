<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>

		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Color Managment <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
        
<form role="form" class="form-group-sm" style="width:450px;" method="post" name="addnews">
  <div class="form-group">
  	<select name="brand"  class="form-control" class="required" required="required">
		<option value="">Select Brand</option>
		<option>Dulux</option>
		<option>Multilac</option>
		<option>Nipolac</option>
		<option>Robialac</option>
		<option>Jat</option>
	</select>	
  </div>
  
  <div class="form-group">
  	<select name="main" class="form-control" class="required"  required="required">
		<option value="">-Select Main Category-</option>
		<option value="INTERIOR">INTERIOR</option>
		<option value="EXTERIOR">EXTERIOR</option>
		<option value="METAL AND WOOD">METAL AND WOOD</option>
		<option value="UNDERCOATS">UNDERCOATS</option>
	</select>
  </div> 
  
  <div class="form-group">
  	<label>Ltr</label>
  	<input type="number" name="ltr" value="0" placeholder="ltr" required="required"  class="form-control" /> 
  </div>
  
  <div class="form-group">
  	<label>Price</label>
  	<input type="number" name="price" value="0" placeholder="Price" required="required"  class="form-control" /> 
  </div>
    
  <div class="form-group">
  <?php echo $ui->input_button_primary(array('name'=>'AddNews','type'=>'Submit','value'=>'Add New'));?>
  </div>
</form>
<?php echo $ajax->submitForm(array('form'=>'addnews','get'=>'#sys_message', 'do'=>'do_addprice.php'));?>
<hr />
<div class="pagination"></div>
<div class="pagecontent"></div>
<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'ui_price_list.php','total'=> $fw->color()->getTotal()));?>