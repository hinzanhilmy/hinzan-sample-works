<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>

		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Stock Alert <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
        
        <table class="table table-bordered table-hover">
			<thead>
			<tr>
				<th>Product</th>
				<th>Stock</th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
				<?php foreach($fw->products()->outofstock() as $d){?>
				<tr>
					<td>
						<img class='media-object img-rounded img-responsive' style='height:100px;'  src="<?php echo UPLOAD_THUMB_PATH . IMAGE_SIZE_LARGE . $d['file']; ?>" />
						<br /><?php echo $d['title'];?></td>
						
					<td><label class="alert alert-danger"><?php echo $d['qoh'];?></label></td>
					<td><a class="btn btn-primary" href="/lpanel/purchase_orders.html?products[]=<?php echo $d['id'];?>&showvendors=Show+Vendors<?php echo $d['id'];?>">Order</a></td>
				</tr>
				<?php }?>
			</tbody>
		</table>