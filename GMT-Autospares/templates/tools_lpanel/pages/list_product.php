<?php if($_SESSION['SUSERTYPE'] == 'ADMIN'){?>
<?php 
if($_REQUEST['delete_btn']){
	$selected = $_REQUEST['selected'];
	foreach($selected as $s){
		$fw->products()->delete($s);
	}
	$_REQUEST['btnlogin'] = 'Search';
	echo $fw->row_message(TRUE, 'Deleted Selected Products');
}

if($_REQUEST['sort_btn']){
	$fw->products()->sort($_REQUEST);
	echo $fw->row_message(TRUE, 'Sorted Products');
	$_REQUEST['btnlogin'] = 'Search';
}
?>
     <div class="col-lg-12">
        <h1 class="page-header">
           List Product <small><?php echo STATIC_COMPANY_NAME;?></small>
        </h1>
     </div>     
		 
<form method="get">       
	<div class="row">
                    <div class="col-lg-6">
                            <div class="form-group">
                            	<select class="form-control" name="catagory">
								<option value="0">-select-</option>
								<?php $fw->meta()->getDrowMultiCataogriesForSelect(0,1,$_REQUEST['catagory']);?>
							</select>
                            </div>
                            
                            <div class="form-group">
                            	<input type="text" name="search_text" value="<?php echo $fw->xss()->safe($_REQUEST['search_text']);?>" class="form-control" />
                            	<p class="help-block">Search product text</p>
                            </div>                            
                            
                            <div class="form-group">
                                <?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Search','type'=>'submit'));?>
                            </div>
                     </div>    
        </div>
        <?php if($_REQUEST['btnlogin'] == "Search"){?>
		<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>#</th>
			<th>Category</th>
			<th>Title</th>
			<th>Description</th>
			<th>Date</th>
			<th>Quantity</th>
			<th>
				<?php echo $ui->input_button_primary(array('name'=>'sort_btn','value'=>'Set Sort','type'=>'submit'));?>
			</th>
			<th>
				<?php  echo $ui->input_button_primary(array('name'=>'delete_btn','value'=>'Delete','type'=>'submit')); ?> 
			</th>
			<th>
				
			</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->products()->get($_REQUEST) as $p){?>
			<tr>
				<td><?php echo $p['id'];?></td>
				<td>
					<small>
					<?php 
						$cat = explode(",",$p['catagories_relation']);
						$result = array();
						foreach($cat as $s){
							$name = $fw->meta()->getCatNameById($s);
							$result [] =  $name['name']; 
						}
						foreach($result as $rs){
							echo  " &raquo; " . $rs;
						}
					?>
					</small> 
				</td>
				<td>
					<?php echo $p['title'];?>
					<?php if($p['new_arrivals'] == 1){?>
						<label class="label label-success">New Arrivals</label>
					<?php }?>
				</td>
				<td><?php echo $p['desc'];?></td>
				<td><small><?php echo date('Y', time($p['timestamp']));?></small></td>
				<td><?php echo $p['qoh'];?></td>
				<td><input type="number" name="sort[<?php echo $p['id'];?>]" class="form-control" placeholder="Sort" value="<?php echo $p['sort'];?>" /></td>
				<td style="width:180px;">
					<input name="selected[]" type="checkbox" value="<?php echo $p['id'];?>"/>
					<a class="delete btn btn-danger" js-message="Do you want to delete this item?" href="id=<?php echo $p['id'];?>">Delete</a>
			 		<a class="btn btn-success" target="blank" href="edit_product.html?id=<?php echo $p['id'];?>"> Edit</a>
				</td>
				
				<td>
					<?php if($p['status'] == 1){?>
						<a href="#" class="btn btn-danger status" value="0" id="<?php echo $p['id'];?>">Inactive</a>
					<?php } else {?>
						<a href="#" class="btn btn-success status" value="1" id="<?php echo $p['id'];?>">Active</a>
					<?php }?>					
				</td>
			</tr>
			<?php }?>
		</tbody>
		</table>
		<?php } else {?>
			<div class="pagination"></div>
			<div class="pagecontent"></div>
			<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'ui_products.php','total'=> $fw->products()->total()));?>
		<?php }?>
</form>

<?php echo $ajax->link_post(array('class'=> 'delete', 'do'=> 'delete_product.php', 'get'=> 'sys_message'));?>

<script>
	$(document).ready(function (data){
		var count=0;
		$('input[type=checkbox]').change(function (data){
			if($(this).attr('checked')){
				count++;
				$('button').text('Delete Selected (' + count + ')');
			} else {
				--count;
				$('button').text('Delete Selected (' + count + ')');
			
			}
		});
	});
</script>
<?php }?>


		<script>
			$(document).ready(function (){
				$.post('<?php echo AJAX_PATH;?>do_admin_seller_list.php', $(this).serialize());
				$('form[name="form_product_list"]').submit(function (data){
						$.ajax({
								type: 'post',
								url : '<?php echo AJAX_PATH;?>do_admin_seller_list.php',
								data: $(this).serialize(),
								dataType: 'html',
								success: function (data){ location.reload();}
						});
						return false;
				});
			});
		</script>
		
		<script language="javascript" type="text/javascript">
			$(document).ready(function (data){
				$('.status').click(function (data){
					
					$.ajax({
						 method : 'post',
						 dataType : 'json',
						 url : '<?php echo AJAX_PATH;?>product_status.php',
						 data : 'id=' + $(this).attr('id') + '&status=' +  $(this).attr('value'),
						 context:this,				 
						 success : function (data){					 	
								 if($(this).attr('value') == 1){
									 $(this).attr('value', '0').removeClass('btn-success').addClass('btn-danger').text('Inactive');
								 } else {
									 $(this).attr('value', '1').removeClass('btn-danger').addClass('btn-success').text('Active');
								 } 						 
							 }
						});
					return false;
				});
			});
		</script>