     
     <div class="col-lg-12">
        <h1 class="page-header">
           Purchase orders <small><?php echo STATIC_COMPANY_NAME;?></small>
        </h1>
     
<form method="post" name="productadd">
		<div class="col-lg-6">
            <div class="form-group">
	    		<select name="products[]" class="product" style="width:500px;" multiple="multiple">
	    			<?php  foreach($fw->products()->all() as $p){?>
	   					<option value="<?php echo $p['id'];?>" <?php echo in_array($p['id'], $_REQUEST['products'])? 'selected="selected"' : '';?>><?php echo $p['id'] . ' -' . $p['title'];?></option>
	   				<?php }?>	
	    		</select>
	    	</div>	    	
            
            <div class="form-group">
                 <?php echo $ui->input_button_primary(array('name'=>'showvendors','value'=>'Show Vendors','type'=>'submit'));?>
        	</div>	    	
		</div>
</form>

<?php if($_REQUEST['showvendors']){?>
		<?php $_SESSION['temp_request'] = $_REQUEST;?>
		<?php foreach($_REQUEST['products'] as $p){?>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Vendor</th>
					<th>Product</th>
					<th>Current Quantity</th>
					<th>Last Order</th>
					<th>Last Order Item / Price</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>				
					<?php foreach($fw->vendor()->byproduct($p) as $v){?>
						<tr>
							<td><?php echo $v['fullname'];?></td>
							<td><?php echo $v['title'];?></td>
							<td><?php echo $v['qoh'];?></td>
							<td><?php echo $v['or_date'];?></td>
							<td><?php echo $v['or_quantity'];?> / <?php echo $v['or_price'];?></td>
							<td><a href="<?php echo AJAX_PATH;?>purchase.php?productid=<?php echo $p?>&v=<?php echo $v['id'];?>" class="btn btn-success editmeta fancybox.ajax">Purchase</a>
								&nbsp;
								<?php if($v['purchase_orders_id']!=""){?>
									<?php if(trim($v['or_status']) != "RECEIVED"){?>
										<a href="<?php echo AJAX_PATH;?>purchase_status.php?purchase_orders_id=<?php echo $v['purchase_orders_id'];?>&pid=<?php echo $p;?>" class="btn btn-danger editmeta fancybox.ajax">
											<?php echo $v['or_status']?>
										</a>
									<?php } else {?>
										<label class="btn btn-default"><?php echo $v['or_status'];?></label>
									<?php }?>	
								<?php }?>
							</td>
						</tr>
					<?php }?>
					
					<tr>
							<td colspan="6" align="center">		
								<a href="<?php echo AJAX_PATH;?>add_vendors.php?product_id=<?php echo $p;?>" class="btn btn-info editmeta fancybox.ajax">
											Add New Vendor
								</a>								
							</td>
						</tr>
			</tbody>
		</table>
		<?php }?>
	
<?php }?>
</div>	
<?php echo $ajax->fancyBoxByClass('editmeta');?>
<?php echo $ajax->populateParentsChildsValue(array('parent'=> 'id' , 'child'=> 'sub', 'do'=> 'json_sub.php'));?>
<?php //echo $ajax->submitForm(array('form'=>'productadd','do'=>'do_add_product.php', 'get'=>'#sys_message'));?>