<?php if($_SESSION['SUSERTYPE'] == 'ADMIN'){?>
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
        
<div class="row">
	<div><h4><b>Today sales</b></h3></div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo sizeof($fw->orders()->summeryitem());?></div>
                                        <div>Number of Bills</div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                         <div class="huge"><font size="3">Rs.</font><?php echo $fw->orders()->getStat()->total_payment;?></div>
                                        <div>Total Payments</div>
                                        <div><?php echo  date("Y/m/d") ;?></div>
                                    </div>
                                </div>
	                            </div>
	                            <a href="#">
                               <div class="panel-footer">
                                    <span class="pull-left">
                                    	<table style="width:230px; font-size:11px;" class="table" style="font-size:11px;">
                                    		<tr>
                                    			<td><font color="blue"><b>Name</b></td>
                                    			<td><font color="blue"><b>Total</b></td>
                                    			<td><font color="blue"><b>Due</b></td>
                                    			</tr>
                                    			<?php foreach($fw->customer()->summery() as $c){?>
                                    			<?php 
                                    				$total += $c['total_amount'];
                                    				$pay   += $c['total_payment'];
                                    				$due   += $c['total_due'];

                                    			?>
                                    			
                                    			<tr>
                                    				<td><?php echo substr($c['customer_name'], 0, 5);?></td>
                                    				<td>Rs.<?php echo number_format($c['total_amount'], 2 , '.', '');?></td>
                                    				<td>Rs.<?php echo number_format($c['total_due'], 2 , '.', '');?></td>
                                    			</tr>
                                    			<?php }?>
                                    	</table>
                                    </span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                    	<div class="huge"><font size="3">Rs.</font><?php echo number_format($total);?></div>
                                        <div>Sales Summery</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                	<table style="width:230px; font-size:11px;" class="table" style="font-size:11px;">
                                    			<tr><td><b>Payment</b></td><td>Rs.<?php echo number_format($pay, 2 , '.', '');?></td></tr>
                                    			<tr><td><b>Due Amount</b></td><td>Rs.<?php echo number_format($due, 2 , '.', '');?></td></tr>
                                    </table>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                
<div class="row"> 
<div><h4><b>Total sales</b></h3></div>       
				<div class="col-lg-3 col-md-6">
				                        <div class="panel panel-red">
				                            <div class="panel-heading">
				                                <div class="row">
				                                    <div class="col-xs-3">
				                                        <i class="fa fa-user fa-5x"></i>
				                                    </div>
				                                    <div class="col-xs-9 text-right">
				                                        <div class="huge"><?php echo $fw->products()->getStat()->quntity;?></div>
				                                        <div>Stock</div>
				                                    </div>
				                                </div>
				                            </div>
				                            <?php foreach($fw->products()->getproduct() as $p){?>
											<?php if($p['quntity_id'] == 0){?>
				                            <a href="lpanel/edit-product.html?id=<?php echo $p['id'];?>">
				                                <div class="panel-footer">
				                                    <span class="pull-left">(<b><?php echo $p['quntity'];?></b>) <?php echo $p['product_name'];?></span>
				                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
				                                    <div class="clearfix"></div>
				                                </div>
				                            </a>
											<?php }?>
				                            <?php }?>
				                        </div>
				                    </div>
				<div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                   <?php foreach($fw->orders()->summerytot() as $c){
                                				 
                                    				$totals += $c['total_amount'];
                                    				$pays   += $c['total_payment'];
                                    				$dues   += $c['total_due'];
									}?><div class="col-xs-9 text-right">
                                    	<div class="huge"><font size="3">Rs.</font><?php echo number_format($totals);?></div>
                                        <div>Sales Summery</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                	<table style="width:230px; font-size:11px;" class="table" style="font-size:11px;">
                                       			<tr><td><b>Payment</b></td><td>Rs.<?php echo number_format($pays, 2 , '.', '');?></td></tr>
                                    			<tr><td><b>Due Amount</b></td><td>Rs.<?php echo number_format($dues, 2 , '.', '');?></td></tr>
                                    </table>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>                    
</div>       
<?php }?>