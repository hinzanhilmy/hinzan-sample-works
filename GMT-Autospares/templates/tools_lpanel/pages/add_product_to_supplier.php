     
     <div class="col-lg-12">
        <h1 class="page-header">
           Add Product To Vendor <small><?php echo STATIC_COMPANY_NAME;?></small>
        </h1>
     </div>
<form method="post" name="productadd" class="">
		<div class="col-lg-6">

			<div class="form-group">
	    		<select name="users" class="product" style="width:500px;">
	    			<?php  foreach($fw->users()->getbyType('SUPPLIER') as $p){?>
	   					<option value="<?php echo $p['id'];?>"><?php echo $p['fullname'];?></option>
	   				<?php }?>	
	    		</select>
	    	</div>
	    	
            <div class="form-group">
	    		<select name="products" class="product" style="width:500px;" multiple="multiple">
	    			<?php  foreach($fw->products()->all() as $p){?>
	   					<option value="<?php echo $p['id'];?>"><?php echo $p['title'];?></option>
	   				<?php }?>	
	    		</select>
	    	</div>	    	
            
            <div class="form-group">
                 <?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Save','type'=>'submit'));?>
        	</div>	    	
		</div>
		
		
</form>
<?php echo $ajax->populateParentsChildsValue(array('parent'=> 'id' , 'child'=> 'sub', 'do'=> 'json_sub.php'));?>
<?php echo $ajax->uploadify_multi_resizeimage_withwatermark('upload','files');?>
<?php echo $ajax->submitForm(array('form'=>'productadd','do'=>'do_add_product.php', 'get'=>'#sys_message'));?>