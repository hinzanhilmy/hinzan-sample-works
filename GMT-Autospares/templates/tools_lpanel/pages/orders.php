<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Orders Managment <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
<div class="pagination"></div>
<div class="pagecontent"></div>
<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'ui_orders.php','total'=> $fw->cart()->total_orders()));?>