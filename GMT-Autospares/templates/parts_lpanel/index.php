<?php include_once ('header/header.php');?>

<!-- <div id="sys_message" class="sys_message"></div>  -->

<div id="wrapper">

		<?php include_once 'menu/menu.php';?>

		<div id="page-wrapper">

			<?php echo $fw->show_session_message();?>

			<?php if($_REQUEST['btnlogin']){?>

				<?php if($fw->users()->login($_REQUEST)){

							//echo $fw->users()->bootsrapMessage('Login','Success !.', 'success');

					  } else {

					  		//echo $fw->users()->bootsrapMessage('Login','Fail !.', 'danger');

					  }

				 ?>

			<?php }?>



			<div class="container-fluid">

				<?php if($fw->users()->isLogin() == FALSE){?>

					<?php include_once ('pages/login.php');?>

				<?php } else {?>
						<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:8px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>

						<?php switch($page){
							case 'users': include_once ('pages/users.php'); break;
							case 'category-management': include_once ('pages/category-management.php'); 	break;
							case 'meta-management': 	include_once ('pages/meta-management.php'); 		break;
							case 'add_product': 		include_once ('pages/add_product.php');				break;
							case 'list_product': 		include_once ('pages/list_product.php');			break;
							case 'edit_product':		include_once ('pages/edit_product.php');			break;
							case 'customers':			include_once ('pages/customers.php');					break;		
							case 'orders': 				include_once ('pages/orders.php');					break;
							case 'purchase_orders': include_once ('pages/purchase_orders.php');				break;
							case 'feedback':		include_once ('pages/feedback.php');					break;							
							case 'audit': 			include_once ('pages/audit.php');					break;							
							case 'customer_modify_order': include_once ('pages/customer_modify_order.php');		break;	
							case 'new_order': 	include_once ('pages/new_order.php');						break;						
							case 'stock':		include_once ('pages/stock.php');							break;					
							case 'stock_status': include_once ('pages/stock_status.php');					break;
							case 'purchase_orders_history': include_once ('pages/purchase_orders_history.php');	break;				
							
							default : include_once ('pages/orders.php');									break;
						
						}	
						?>
				<?php }?>
			</div>
		</div>
</div>
<?php include_once ('footer/footer.php');?>