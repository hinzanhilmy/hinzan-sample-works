<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $price = $fw->price()->get($_REQUEST);?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>

<form role="form" class="form-group-sm" style="width:450px;" method="post" name="editprice">
  <div class="form-group">
  	<select name="brand"  class="form-control" class="required" required="required">
		<option selected="selected"><?php echo $price['main'] ?></option>
		<option>Dulux</option>
		<option>Multilac</option>
		<option>Nipolac</option>
		<option>Robialac</option>
		<option>Jat</option>
	</select>	
  </div>
  
  <div class="form-group">
  	<select name="main" class="form-control" class="required"  required="required">
		<option selected="selected"><?php echo $price['brand'] ?></option>
		<option>INTERIOR</option>
		<option>EXTERIOR</option>
		<option>METAL AND WOOD</option>
		<option>UNDERCOATS</option>
	</select>
  </div> 
  
  <div class="form-group">
  	<label>Ltr</label>
  	<input type="number" name="ltr" value="<?php echo $price['ltr'];?>" placeholder="ltr" required="required"  class="form-control" /> 
  </div>
  
  <div class="form-group">
  	<label>Price</label>
  	<input type="number" name="price" value="<?php echo $price['price'];?>" placeholder="Price" required="required"  class="form-control" /> 
  </div>
    
  <div class="form-group">
  <?php echo $ui->input_button_primary(array('name'=>'AddNews','type'=>'Submit','value'=>'Edit'));?>
  </div>
</form>
<?php echo $ajax->submitForm(array('form'=>'editprice','get'=>'#sys_message', 'do'=>'do_editnews.php'));?>