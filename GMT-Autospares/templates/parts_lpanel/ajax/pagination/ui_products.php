<?php include ('../../../../system/main.php');?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>#</th>
			<th>Category</th>
			<th>Title</th>
			<th>Description</th>
			<th>Date Added</th>
			<th>Quantity</th>
			<td>&nbsp;</td>
			<th>	
			</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->products_parts()->admin_pagination($next) as $p){?>
			<tr>
				<td><?php echo $p['id'];?></td>
				<td>
					<small>
					<?php 
						$cat = explode(",",$p['catagories_relation']);
						$result = array();
						foreach($cat as $s){
							$name = $fw->meta()->getCatNameById($s);
							$result [] =  $name['name']; 
						}
						foreach($result as $rs){
							echo  " &raquo; " . $rs;
						}
					?>
					</small> 
				</td>
				<td><?php echo $p['title'];?>
					<?php if($p['new_arrivals'] == 1){?>
						<label class="label label-success">New Arrivals</label>
					<?php }?>
				</td>
				<td><?php echo $p['desc'];?></td>
				<td><?php echo $p['timestamp'];?></td>
				<td><?php echo $p['qoh'];?></td>
				<td style="width:140px;">
					<a class="editmeta btn btn-success" target="blank" href="edit_product.html?id=<?php echo $p['id'];?>">Edit</a>
					<a class="delete btn btn-danger" js-message="Do you want to delete this item?" href="id=<?php echo $p['id'];?>" class="btn btn-primary" >Delete</a>
				</td>
				<td>
					<?php if($p['status'] == 1){?>
						<a href="#" class="btn btn-danger status" value="0" id="<?php echo $p['id'];?>">Inactive</a>
					<?php } else {?>
						<a href="#" class="btn btn-success status" value="1" id="<?php echo $p['id'];?>">Active</a>
					<?php }?>					
				</td>
			</tr>
			<?php }?>
		</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function (data){
		$('.status').click(function (data){
			
			$.ajax({
				 method : 'post',
				 dataType : 'json',
				 url : '<?php echo AJAX_PATH;?>product_status.php',
				 data : 'id=' + $(this).attr('id') + '&status=' +  $(this).attr('value'),
				 context:this,				 
				 success : function (data){					 	
						 if($(this).attr('value') == 1){
							 $(this).attr('value', '0').removeClass('btn-success').addClass('btn-danger').text('Inactive');
						 } else {
							 $(this).attr('value', '1').removeClass('btn-danger').addClass('btn-success').text('Active');
						 } 						 
					 }
				});
			return false;
		});
	});
</script>

<?php echo $ajax->fancyBoxByClass('editmeta');?>
<?php echo $ajax->link_post(array('class'=> 'delete', 'do'=> 'delete_product.php', 'get'=> 'sys_message'));?>