<?php include ('../../../../system/main.php'); ?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>#</th>			<th>Category</th>
			<th>Topic</th>
			<th>Description</th>		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->color()->pagination($next) as $d){?>
			<tr>
				<td>
					<a class="btn btn-success fancybox fancybox.ajax" href="<?php echo AJAX_PATH?>ui_do_editnews.php?id=<?php echo $d['id'];?>">Edit</a>
					<a title="Do you want to delete news article?" class="delete btn btn-danger" href="id=<?php echo $d['id'];?>">Delete</a>
				</td>
				
				<td><?php echo $d['cat'];?></td>				<td><?php echo $d['topic'];?></td>								<td><?php echo $d['description'];?></td>				
				
			</tr>
			<?php }?>
		</tbody>
</table>
<?php echo $ajax->link_post(array('class'=>'delete','do'=>'do_delete_article.php', 'get'=>'#sys_message'))?>
<?php echo $ajax->fancyBoxByClass('fancybox');?>