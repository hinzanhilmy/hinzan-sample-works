<?php include ('../../../../system/main.php');?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->catagories()->pagination($next) as $d){?>
			<tr>
				<td><?php echo $d['id'];?></td>
				<td>
					<small>
					<?php 
						if(NULL!=$fw->meta()->getCatNameById($d['parent'])){
							$cat = $fw->meta()->getCatNameById($d['parent']);
							echo  '<b>' . $cat['name']  . '</b> &raquo; ' . $d['label']; 
						} else {
							echo $d['label'];
						}	
					
					?>
					</small>
				</td>				
				<td><span class="label"><a class="editmeta  btn btn-success fancybox.ajax" style="color:white;" href="<?php echo AJAX_PATH;?>ui_do_editcat.php?id=<?php echo $d['id'];?>">Edit</a></span></td>
				<td><span class="label"><a class="editmeta btn btn-danger fancybox.ajax" href="<?php echo AJAX_PATH;?>do_cat_action.php?id=<?php echo $d['id'];?>&action=deletename">Delete</a></span></td>
			</tr>
			<?php }?>
		</tbody>
</table>
<?php echo $ajax->fancyBoxByClass('editmeta');?>