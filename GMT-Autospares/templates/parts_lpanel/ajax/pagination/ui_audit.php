<?php include ('../../../../system/main.php');?>

<table class="table table-bordered table-hover">
                            	<thead>
                            	<tr>
                            		<th>Customer Name</th>
                            		<th>Product Views</th>
                            		<th>No Of Logins</th>
                            		<th>Last Login</th>
                            		<th>Last Order</th>
                            	</tr>
                            	</thead>
                            	
                            	<tbody>
                            		<?php foreach($fw->audit()->pagination($next) as $a){?>
                            		<?php $logins = $fw->audit()->no_of_logins($a['userid']);?>
                            		<tr>
	                            		<td>
	                            			<a class="editmeta fancybox.ajax" target="blank" href="<?php echo AJAX_PATH;?>ui_do_edituser_cus.php?id=<?php echo $a['userid'];?>">
	                            				<?php echo $a['fullname'];?> (<?php echo $a['company'];?>)
	                            			</a>
	                            		</td>
	                            		<td>
	                            			<a class="editmeta fancybox.ajax" target="blank" href="<?php echo AJAX_PATH;?>ui_audit_login.php?id=<?php echo $a['userid'];?>">
	                            				<?php echo $a['count'];?>
	                            			</a>	
	                            		</td>
	                            		
	                            		<td>
	                            			<a class="editmeta fancybox.ajax" target="blank" href="<?php echo AJAX_PATH;?>ui_audit_no_login.php?id=<?php echo $a['userid'];?>">
	                            				<?php echo sizeof($logins);?>
	                            			</a>	
	                            		</td>
	                            		
	                            		<td><?php echo $logins[0]['date_time'];?></td>
	                            		<td>
	                            			<a class="editmeta fancybox.ajax" target="blank" href="<?php echo AJAX_PATH;?>ui_view_order.php?id=<?php echo $a['order_id'];?>">
	                            				<?php echo $a['order_date'];?>
	                            			</a>
	                            		</td>
                            		</tr>	
                            		<?php }?>
                            	</tbody>
                            </table>
                            
<?php echo $ajax->fancyBoxByClass('editmeta');?>