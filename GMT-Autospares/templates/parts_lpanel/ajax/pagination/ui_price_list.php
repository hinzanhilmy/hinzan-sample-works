<?php include ('../../../../system/main.php'); ?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>#</th>
			<th>Brand</th>
			<th>Category</th>
			<th>Price</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($fw->price()->pagination() as $d){?>
			<tr>
				<td>
					<a class="btn btn-success fancybox fancybox.ajax" href="<?php echo AJAX_PATH?>ui_do_editprice.php?id=<?php echo $d['id'];?>">Edit</a>
					<a title="Do you want to delete news article?" class="delete btn btn-danger" href="id=<?php echo $d['id'];?>">Delete</a>
				</td>
				
				<td><?php echo $d['brand'];?></td>
				<td><?php echo $d['main'];?></td>
				<td><?php echo $d['price'];?></td>				
				
			</tr>
			<?php }?>
		</tbody>
</table>
<?php echo $ajax->link_post(array('class'=>'delete','do'=>'do_delete_article.php', 'get'=>'#sys_message'))?>
<?php echo $ajax->fancyBoxByClass('fancybox');?>