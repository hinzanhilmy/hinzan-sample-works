<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $paint = $fw->color()->get($_REQUEST);?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>

<form role="form" name="edituser" class="form-group-sm" style="width:450px;" method="post">

   <div class="form-group">
   	<input type="hidden" name="id" value="<?php echo $paint['id'];?>" />
  </div>
  
  <div class="form-group">
  	<select name="brand"  class="form-control" class="required" required="required">
		<option value="">Select Brand</option>
		<option selected="selected"><?php echo $paint['brand'];?></option>
		
		<option>Dulux</option>
		<option>Multilac</option>
		<option>Nipolac</option>
		<option>Robialac</option>
		<option>Jat</option>
	</select>	
  </div>
  
  <div class="form-group">
  	<select name="main" class="form-control" class="required"  required="required">
		<option value="">-Select Main Category-</option>
		<option selected="selected"><?php echo $paint['main'];?></option>
		<option value="INTERIOR">INTERIOR</option>
		<option value="EXTERIOR">EXTERIOR</option>
		<option value="METAL AND WOOD">METAL AND WOOD</option>
		<option value="UNDERCOATS">UNDERCOATS</option>
	</select>
  </div>
    
  <!-- <div class="form-group">
  	<select name="sub" class="form-control" class="required"  required="required">
		<option value="">-Select Sub Category-</option>
		<option selected="selected"><?php echo $paint['sub'];?></option>
		<option>Pentalite Plus</option>
		<option>Ambiance</option>
		<option>Light and Space</option>
		<option>SuperKote</option>
		<option>Supreme 3 in 1-Standard</option>
		<option>Supreme 3 in 1-Master Palette</option>
	</select>
  </div> -->
  
   <!--  <div class="form-group">
  	<select name="main_color"  class="form-control" class="required" required="required">
		<option value="">Select Main Color</option>
		<option selected="selected"><?php echo $paint['color'];?></option>
		<option style="background-color: red;">REDS</option>
		<option style="background-color: orange;">ORANGES</option>
		<option style="background-color: yellow;">YELLOWS</option>
		<option style="background-color:#b2882e;">WORM NAUTRALS</option>
		<option style="background-color:blue;">BLUES</option>
		<option style="background-color:violet;">VIOLETS</option>
		<option style="background-color:#82939f;">COOL NEUTRALS</option>
		<option>WHITE</option>
	</select>	
  </div> -->
  
 
  <!-- <div class="form-group">
  	<input name="color" type="color" class="form-control" value="<?php echo $paint['color_sub'];?>" />	
  </div> -->
  
 
  <!-- <div class="form-group">
    <?php echo $ui->input_text(array('name'=>'description','hint'=>'Description','class'=>'required form-control', 'value'=> $paint['description']));?>
  </div> -->
  
  <!-- <div class="form-group">
    <input name="price" type="number" class="form-control" value="<?php echo $paint['price'];?>" />
  </div> -->
  	  		  <div class="form-group input-group">
  	  		  		
			  		<ul class="thumbnails" id="editfile">
			  			<?php foreach($fw->color()->get_images($id) as $i){?>
			  				<li style="list-style:none;"><img src="<?php echo UPLOAD_THUMB_PATH . $i['file'];?>"></li>
			  		<?php }?>			  			
			  		</ul>
			  		<div style="clear:both;"></div>
					<input type="file" name="edit_upload" id="edit_upload" />	
		    </div> 
  
  <div class="form-group">
  <?php echo $ui->input_button_primary(array('name'=>'AddNews','type'=>'Submit','value'=>'Edit'));?>
  </div>
</form>
<?php echo $ajax->uploadify_multi_resizeimage_withwatermark('edit_upload','editfile');?>
<?php echo $ajax->submitForm(array('form'=>'edituser','get'=>'#sys_message', 'do'=>'do_editnews.php'));?>