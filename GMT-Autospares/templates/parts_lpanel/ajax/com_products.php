<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php // foreach($fw->products()->admin_get_orderproduct($id) as $p){?>
<?php //}?>
<form method="post" name="form_com_products">
<input class="btn btn-success" type="submit" name="selected_products" value="Add Selected Products"/>
<br />
<hr />
<table id="comp_products" class="table" cellspacing="0">
        <thead>
            <tr>
                <th></th>
                <th>Catagory</th>
                <th>Title</th>
                <th>Desc</th>
                <th>QOH</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th>Catagory</th>
                <th>Desc</th>
                <th>QOH</th>
            </tr>
        </tfoot>
        <tbody>
        <?php foreach($fw->products()->admin_get_orderproduct() as $p){?>
            <tr>
                <td>
                	<input type="checkbox" name="com_product[]" value="<?php echo $p['id'];?>" />
                	<?php 
                		$product = $fw->products()->loadProduct($p['id']);
                		$image['file'] =  $product[0]['image'];
                	?>
                	<span class="n-r-img"><img style="width:75px;" src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_THUMB . $image['file'];?>"></span>
                </td>
                <td>
                	<?php 
						$cat = explode(",",$p['catagories_relation']);
						$result = array();
						foreach($cat as $s){
							$name = $fw->meta()->getCatNameById($s);
							$result [] =  $name['name']; 
						}
						foreach($result as $rs){
							echo  " &raquo; " . $rs;
						}
					?>
                </td>
                <td><?php echo $p['title'];?></td>
                <td><?php echo nl2br(substr(htmlspecialchars($p['desc']),0, 100));?>..</td>
                <td><?php echo $p['qoh'];?></td>
            </tr>
        <?php }?>    
        </tbody>
    </table>
 </form>   
    
<script>
$(document).ready(function() {
    $('#comp_products').DataTable();
} );
</script>
<?php echo $ajax->submitForm(array('form'=>'form_com_products','get'=>'#sys_message', 'do'=>'com_do_product.php'));?>