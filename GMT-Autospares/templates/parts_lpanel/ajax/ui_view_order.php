<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>Product</th>
			<th>Unit Price</th>
			<th>Qun</th>
			<th>Total Price</th>
		</tr>
		</thead>
		<tbody>
			<?php $total = 0;?>
			<?php foreach($fw->cart()->order_list($id) as $d){?>
			<?php $product = $fw->products()->loadProduct($d['product_id']);?>
			<?php $total += $d['price'] * $d['qun'];?>
			<tr>
				<td><?php echo $product[0]['title'];?></td>
				<td><?php echo $d['price'];?></td>
				<td><?php echo $d['qun'];?></td>
				<td><?php echo $d['price'] * $d['qun'];?></td>								
			</tr>
			<?php }?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Total Price</td>
				<td><?php echo $total;?></td>								
			</tr>			
		</tbody>
</table>
<?php echo $ajax->submitForm(array('form'=>'edituser','get'=>'#sys_message', 'do'=>'do_edituser_cus.php'));?>