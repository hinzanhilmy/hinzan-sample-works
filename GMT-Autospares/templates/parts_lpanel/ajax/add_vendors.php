<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php 
$p = $fw->vendor()->get_order($_REQUEST['purchase_orders_id']);?>
<h3>Add Vendor</h3>
<hr />
<form method="post" name="status_order">
	<input type="hidden" value="<?php echo $_REQUEST['product_id'];?>" name="id" />
	<p> 
				<select name="vendors[]" class="vendors" style="width:500px;" multiple="multiple">
	    			<?php  foreach($fw->users()->getbyType('SUPPLIER') as $p){?>
	   					<option value="<?php echo $p['id'];?>" <?php echo in_array($p['id'], $_REQUEST['products'])? 'selected="selected"' : '';?>><?php echo $p['id'] . ' -' . $p['fullname'];?></option>
	   				<?php }?>	
	    		</select>
	</p>
	<p> <?php echo $ui->input_button_primary(array('name'=>'EditMeta','type'=>'Submit','value'=>'Add To Product'));?>
	
	&nbsp; &nbsp; &nbsp;
	<!--  a href="<?php echo AJAX_PATH;?>add_vendors.php?product_id=<?php echo $p;?>" class="btn btn-info editmeta fancybox.ajax">
				Add New Vendor
	</a -->
	<a target="blank" href="/lpanel/users.html" class="btn btn-info">
				Add New Vendor
	</a>
	</p>							
								
</form>
  <script type="text/javascript">

  	$(document).ready(function (data){
  	  $('.vendors').selectize({ create: false});
  	});

  </script>
<?php echo $ajax->submitForm(array('form'=>'status_order','get'=>'#sys_message', 'do'=>'add_vendor_toproduct.php'));?>