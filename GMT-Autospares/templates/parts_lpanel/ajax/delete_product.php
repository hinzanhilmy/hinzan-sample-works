<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php 
	$id = $_REQUEST['id'];
	if($fw->products()->delete($id)){
		echo json_encode(array('status'=>TRUE,'message'=> 'Done !','jredirect'=> false, 'jredirecturl'=> HTTP_PATH . $template . '/know.html'));
	} else {	
		echo json_encode(array('status'=>TRUE,'message'=> 'Fail !','jredirect'=> false, 'jredirecturl'=> HTTP_PATH . $template . '/know.html'));	
	}
?>