<?php include ('../../../system/main.php'); ?>
<?php $f = $fw->feedback()->get(array('id'=> $id));?>
<?php $f = $f[0];?>
<div class="col-lg-14" style="width:800px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Customer Feedback
                        </div>
                        <!-- /.panel-heading -->		
                        <div class="panel-body">
                            <label>Member</label>
                            <p><?php echo $f['member'];?></p>
                            
                            <label>Suggesion Type</label>
                            <p><?php echo $f['suggestion_type'];?></p>
                            
                            <label>Full Name</label>
                            <p><?php echo $f['full_name'];?></p>
                            
                            <label>Email</label>
                            <p><?php echo $f['email'];?></p>
                            
                            <label>Phone</label>
                            <p><?php echo $f['phone'];?></p>
                            
                            <label>Message</label>
                            <p><?php echo nl2br($f['description']);?></p>
                            <a class="btn delete btn-danger btn-block" href="id=<?php echo $f['id'];?>" js-message="Do you want to delete this?.">Delete</a>                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
</div>                             
<?php echo $ajax->link_post(array('class'=> 'delete', 'do'=> 'feedback_delete.php'));?>