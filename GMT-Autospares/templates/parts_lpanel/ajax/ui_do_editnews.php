<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $paint = $fw->color()->get($_REQUEST);?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>

<form role="form" name="editwork" class="form-group-sm" style="width:450px;" method="post">
   <div class="form-group">
   	<input type="hidden" name="id" value="<?php echo $paint['id'];?>" />
  </div>
  
  <div class="form-group">
  	  	<select name="brand"  class="form-control" class="required" required="required">			
			<?php foreach($fw->meta()->getByCode('Portfolio') as $m){?>
					<option <?php echo ($paint['cat'] == $m['name'])? 'selected="selected"' : ''?>><?php echo $m['name'];?></option>
			<?php }?>	
		</select>
  </div>
  <div class="form-group">
  	<input name="topic" type="text" class="form-control" placeholder="Topic" value="<?php echo $paint['topic'];?>" />	
  </div>
  <div class="form-group">
  	<textarea name="description" class="form-control" placeholder="Description"><?php echo $paint['description'];?></textarea>	
  </div>
 
  	  		  <div class="form-group input-group">
  	  		  		
			  		<ul class="thumbnails" id="editfile">
			  			<?php foreach($fw->color()->get_images($id) as $i){?>
			  				<li style="list-style:none;"><img src="<?php echo UPLOAD_THUMB_PATH . $i['file'];?>"></li>
			  		<?php }?>			  			
			  		</ul>
			  		<div style="clear:both;"></div>
					<input type="file" name="edit_upload" id="edit_upload" />	
		    </div> 
  
  <div class="form-group">
  <?php echo $ui->input_button_primary(array('name'=>'AddNews','type'=>'Submit','value'=>'Edit'));?>
  </div>
</form>
<?php echo $ajax->uploadify_multi_resizeimage_withwatermark('edit_upload','editfile');?>
<?php echo $ajax->submitForm(array('form'=>'editwork','get'=>'#sys_message', 'do'=>'do_editnews.php'));?>