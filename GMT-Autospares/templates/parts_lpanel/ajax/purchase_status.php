<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php 
$p = $fw->vendor()->get_order($_REQUEST['purchase_orders_id']);?>
<h3>Order</h3>
<hr />
<form method="post" name="status_order">
	<input type="hidden" value="<?php echo $_REQUEST['purchase_orders_id'];?>" name="id" />
	<input type="hidden" value="<?php echo $_REQUEST['pid'];?>" name="pid" />
	<p>Received Quantity</p>
	<p> 
		<input type="number" class="form-control required" value="<?php echo $p['quantity'];?>" name="qun" />
	</p>
	<p>Received Item Price</p>
	<p> 
		<input type="number" class="form-control required" value="0" name="price" />
	</p>
	
	<p>Received Date</p>
	<p> 
		<input type="date" class="form-control required " value="0" name="date" />
	</p>
	
	<p>Status</p>
	<p> 
		<select name="status" class="form-control">
			<?php foreach($p_status as $s){?>
				<option value="<?php echo $s;?>" <?php echo ($s == $p['status']) ? 'selected="selected"' : '';?>><?php echo $s;?></option>
			<?php }?>
		</select>
	</p>
	
	<p> <?php echo $ui->input_button_primary(array('name'=>'EditMeta','type'=>'Submit','value'=>'Order'));?></p>
</form>
<?php echo $ajax->submitForm(array('form'=>'status_order','get'=>'#sys_message', 'do'=>'do_purchase_status.php'));?>