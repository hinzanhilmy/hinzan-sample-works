<?php include ('../../../system/main.php');?>
<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<?php $userdetails = $fw->users()->get($_REQUEST);?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit User <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>

<form role="form" class="form-group-sm" style="width:450px;" method="post" name="edituser">
	<input type="hidden" value="<?php echo $_REQUEST['id'];?>" name="id" />
	<p>Full Name</p>
	<p> <?php echo $ui->input_text(array('name'=>'fullname','value'=>$userdetails['fullname'],'hint'=>'User Full Name','class'=>'required form-control'));?></p>
	<p>User Name</p>
	<p> <?php echo $ui->input_text(array('name'=>'username','value'=>$userdetails['username'],'hint'=>'User Name','class'=>'required form-control'));?></p>
	
	<p>Company</p>
	<p> <?php echo $ui->input_text(array('name'=>'company','value'=>$userdetails['company'],'hint'=>'Company','class'=>'required form-control'));?></p>
	
	<p>Email</p>
	<p> <?php echo $ui->input_text(array('name'=>'email','value'=>$userdetails['email'],'hint'=>'Email','class'=>'required email form-control'));?></p>
	
	<p>Street No</p>
	<p> <?php echo $ui->input_text(array('name'=>'streetno','value'=>$userdetails['streetno'],'hint'=>'Street No','class'=>'required form-control'));?></p>
	
	<p>Street Name</p>
	<p> <?php echo $ui->input_text(array('name'=>'streetname','value'=>$userdetails['streetname'],'hint'=>'Street Name','class'=>'required form-control'));?></p>
	
	<p>Postal Code</p>
	<p> <?php echo $ui->input_text(array('name'=>'postal_code','value'=>$userdetails['postal_code'],'hint'=>'Postal Code','class'=>'required form-control'));?></p>
	
	<p>Suburb</p>
	<p> <?php echo $ui->input_text(array('name'=>'suburb','value'=>$userdetails['suburb'],'hint'=>'Suburb','class'=>'required form-control'));?></p>
	
	<p>State</p>
	<p> <?php echo $ui->input_text(array('name'=>'state','value'=>$userdetails['state'],'hint'=>'State','class'=>'required form-control'));?></p>
	
	<p>ABN</p>
	<p> <?php echo $ui->input_text(array('name'=>'abn','value'=>$userdetails['abn'],'hint'=>'ABN','class'=>'required form-control'));?></p>
	
	
	<p>Password</p>
	<p> <?php echo $ui->input_text(array('name'=>'password','value'=>'','hint'=>'Password', 'class'=>'required form-control'));?></p>

	<p>Account Type</p>
	<p><font color="red">Please select user type</font></p>
	<p>
		<input type="radio" value="ADMIN" name="accountrype" <?php echo ($userdetails['account_type']=='ADMIN')? 'checked="checked"' : '';?>> Administrator  <br />
		<input type="radio" value="USER" name="accountrype"  <?php echo ($userdetails['account_type']=='USER')? 'checked="checked"' : '';?>> User (CUSOMTER)<br />
		<input type="radio" value="USER" name="accountrype"  <?php echo ($userdetails['account_type']=='SUPPLIER')? 'checked="checked"' : '';?>> Vendor<br />
		<input type="radio" value="NOLOG" name="accountrype" <?php echo ($userdetails['account_type']=='NOLOG')? 'checked="checked"' : '';?>> No Access <br />
	</p>

	<p> <?php echo $ui->input_button_primary(array('name'=>'AddUser','type'=>'Submit','value'=>'Edit User'));?></p>
</form>
<?php echo $ajax->submitForm(array('form'=>'edituser','get'=>'#sys_message', 'do'=>'do_edituser_cus.php'));?>