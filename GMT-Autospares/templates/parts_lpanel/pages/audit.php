		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Audit Managment <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
        
<div class="col-lg-14">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Audit trail
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="pagecontent">Loading</div>
							<div class="pagination"></div>
							<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'ui_audit.php','total'=> $fw->audit()->total()));?>                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
</div>