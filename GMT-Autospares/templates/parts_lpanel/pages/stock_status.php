     
     <div class="col-lg-12">
        <h1 class="page-header">
           Stock Status <small><?php echo STATIC_COMPANY_NAME;?></small>
        </h1>
     
<!-- form method="post" name="productadd">
		<div class="col-lg-6">
            <div class="form-group">
	    		<select name="id" class="form-group" style="width:500px;" multiple="multiple">
	    			<?php  foreach($fw->users()->getbyType('SUPPLIER') as $p){?>
	   					<option value="<?php echo $p['id'];?>" <?php echo in_array($p['id'], $_REQUEST['products'])? 'selected="selected"' : '';?>><?php echo $p['id'] . ' -' . $p['fullname'];?></option>
	   				<?php }?>	
	    		</select>
	    	</div>	    	
            
            <div class="form-group">
                 <?php echo $ui->input_button_primary(array('name'=>'showhistory','value'=>'Show History','type'=>'submit'));?>
        	</div>	    	
		</div>
</form -->

<?php //if($_REQUEST['showhistory']){?>		
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Product</th>
					<th style="text-align:right;">Quantity On Hand</th>
					<th style="text-align:right;">Committed</th>
					<th style="text-align:right;">Available Qun.</th>
					<th style="text-align:right;">Reorder Level</th>
					<th>Reorder</th>
				</tr>
			</thead>
			<tbody>				
					<?php foreach($fw->purchase()->stock_status() as $v){?>
						<tr>
							<td><?php echo $v['title'];?></td>
							<td align="right"><?php echo $v['qoh'];?></td>
							<td align="right"><a class="fancybox.ajax editmeta" href="<?php echo AJAX_PATH?>stock_status_orderhistory.php?id=<?php echo $v['id'];?>"><?php echo $v['order_qun'];?></a></td>
							<td align="right"><?php echo ($v['qoh']- $v['order_qun']);?></td>
							<td align="right">
								<?php echo $v['re_orderlevel'];?>
							</td>
							<td><a class="btn btn-sm <?php echo ($v['re_orderlevel'] >= $v['qoh'])? 'btn-danger' : 'btn-info';?>" href="/lpanel/purchase_orders.html?products[]=<?php echo $v['id'];?>&showvendors=Show+Vendors">Reorder</a></td>
						</tr>
					<?php }?>
			</tbody>
		</table>
	
<?php // }?>
</div>	
<?php echo $ajax->fancyBoxByClass('editmeta');?>
<?php echo $ajax->populateParentsChildsValue(array('parent'=> 'id' , 'child'=> 'sub', 'do'=> 'json_sub.php'));?>
<?php //echo $ajax->submitForm(array('form'=>'productadd','do'=>'do_add_product.php', 'get'=>'#sys_message'));?>