		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Customers Managment <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
        
<div class="col-lg-14">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Customer Feedback
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                               <?php foreach($fw->feedback()->get() as $f){?>
	                               	<?php  if($f['status'] == 0){?>
		                                <a href="<?php echo AJAX_PATH;?>read_feedback.php?id=<?php echo $f['id'];?>" class="list-group-item fancybox.ajax popup">
		                                    <i class="fa fa-twitter fa-fw"></i> <?php echo $f['suggestion_type'];?> - <code><?php echo substr($f['description'], 0, 60)?>..</code>
		                                    	<span class="pull-right text-muted small"><em><b><?php echo $f['date'];?></b></em>
		                                    </span>
		                                </a>
	                               <?php } else {?>
		                                <a href="<?php echo AJAX_PATH;?>read_feedback.php?id=<?php echo $f['id'];?>" class="list-group-item fancybox.ajax popup">
		                                    <i class="fa fa-twitter fa-fw"></i> <?php echo $f['suggestion_type'];?> - <code><?php echo substr($f['description'], 0, 60)?>..</code>
		                                    	<span class="pull-right text-muted small"><em><?php echo $f['date'];?></em>
		                                    </span>
		                                </a>                               
	                               <?php }?>    
                               <?php }?>                             
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
</div>
<?php echo $ajax->fancyBoxByClass('popup');?>