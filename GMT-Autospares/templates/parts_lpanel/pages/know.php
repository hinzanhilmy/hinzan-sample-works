<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>

		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Knowledge Managment <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
        
<form role="form" class="form-group-sm" style="width:450px;" method="post" name="addnews">

   <div class="form-group">
   	<label>Knowledge</label>
   	<input type="hidden" value="KNOWLEDGE" name="article_type" />
  </div>
  <div class="form-group">
  	<select name="lng" class="form-control" class="required">
		<option value="">-Select Language-</option>
		<?php foreach($fw->meta()->get() as $m){?>
		<option value="<?php echo $m['id']?>"><?php echo $m['name'];?></option>
		<?php }?>
	</select>
  </div>
	
  <div class="form-group">
    <?php echo $ui->input_text(array('name'=>'title','hint'=>'Title','class'=>'required form-control'));?>
  </div>
  <div class="form-group">
    <textarea  name="description" class="form-control" rows="10"></textarea>
  </div>
  
		  		<div class="form-group input-group">
			  		<ul class="thumbnails" id="files"></ul>
			  		<div style="clear:both;"></div>
					<input type="file" name="upload" id="upload" />	
		    </div>
  
  <div class="form-group">
  <?php echo $ui->input_button_primary(array('name'=>'AddNews','type'=>'Submit','value'=>'Add News'));?>
  </div>
</form>
<?php echo $ajax->uploadify('upload','files');?>
<?php echo $ajax->submitForm(array('form'=>'addnews','get'=>'#sys_message', 'do'=>'do_addknow.php'));?>

<hr />

<div class="pagination"></div>
<div class="pagecontent"></div>
<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'ui_know.php','total'=> $fw->news()->total_know()));?>