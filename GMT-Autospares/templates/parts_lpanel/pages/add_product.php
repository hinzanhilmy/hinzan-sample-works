     
     <div class="col-lg-12">
        <h1 class="page-header">
           Add Product <small><?php echo STATIC_COMPANY_NAME;?></small>
        </h1>
     </div>
<form method="post" name="productadd" id="productadd" class="">
		<div class="col-lg-6">

		<div class="form-group">
	    		<label>IS NEW ARRIVALS</label>
	    		<input name="new_arrivals" type="checkbox" value="1" />
	    	</div>	

			<div class="form-group">
					<label>Category</label>
			  		<select class="form-control required" name="id">
						<option value="">-select-</option>
						<?php foreach($fw->meta()->getMainCats() as $m){?>
							<option value="<?php echo $m['id'];?>"><?php echo $m['name'];?></option>
						<?php }?>
					</select>
	    	</div>
	    	
	    	<div class="form-group">
					<label>Sub Category</label>
			  		<select class="form-control required" name="sub">
						<option value="">-select-</option>						
					</select>
	    	</div>

	    	<div class="form-group">
	    		<label>Title</label>
	    		<input id="ptitle" type="text" class="form-control" name="title" placeholder="" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Highlight Note</label>
	    		<textarea  class="form-control" name="highlight" rows="6" cols=""></textarea>
	    	</div>	    	
	    	
	    	<div class="form-group">
	    		<label>Description</label>
	    		<textarea  class="form-control richtext required" name="description" rows="6" cols=""></textarea>
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Keywords (AAAA, BBBBB, etc..)</label>
	    		<textarea  class="form-control required" name="keywords" rows="6" cols=""></textarea>
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Selling Price (AUD)</label>
	    		<input type="number" class="form-control required" name="price" placeholder="0.00" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Cost Price (AUD)</label>
	    		<input type="number" class="form-control" name="d_price" placeholder="0.00" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Quantity on hand</label>
	    		<input type="text" class="form-control required" name="qoh" placeholder="Quantity on hand" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Re Order Point</label>
	    		<input type="text" class="form-control required" name="reorder_level" placeholder="Re order point" />
	    	</div>
	    	
			<div class="form-group">
							  <label>Images</label>
				              <ul class="thumbnails clearfix" id="files"></ul>                               
                              <div class="clearfix">
                                <div class="col-lg-6">
                                  <div class="uploader-btn">
                                    <input type="file" name="upload" id="upload" class="file-input" />
                                    <!-- <button class="btn-upload" name="action" value="upload"><span>Add Photos</span><span class="spin"></span></button> -->
                                  </div>
                                </div>                                
                              </div>
            </div>
             
            <div class="form-group">
	    		<label>More Detail</label>
	    		<textarea  class="form-control richtext required" name="performances" id="performances" rows="6" cols=""></textarea>
	    	</div>	    	
            
            <div class="form-group">
                 <?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Save','type'=>'submit'));?>
        	</div>	    	
		</div>
		
		
</form>
<?php echo $ajax->populateParentsChildsValue(array('parent'=> 'id' , 'child'=> 'sub', 'do'=> 'json_sub.php'));?>
<?php echo $ajax->uploadify_multi_resizeimage_withwatermark('upload','files');?>
<?php echo $ajax->submitForm(array('form'=>'productadd','do'=>'do_add_product.php', 'get'=>'#sys_message'));?>