     
     <div class="col-lg-12">
        <h1 class="page-header">
           Purchase orders history <small><?php echo STATIC_COMPANY_NAME;?></small>
        </h1>
     
<form method="post" name="productadd">
		<div class="col-lg-6">
            <div class="form-group">
	    		<select name="id" class="form-group customer" style="width:500px;" multiple="multiple">
	    			<option value="0" selected="selected">- All Vendors -</option>
	    			<?php  foreach($fw->users()->getbyType('SUPPLIER') as $p){?>
	   					<option value="<?php echo $p['id'];?>" <?php echo ($p['id'] == $_REQUEST['id'])? 'selected="selected"' : '';?>><?php echo $p['id'] . ' -' . $p['fullname'];?></option>
	   				<?php }?>	
	    		</select>
	    		
	    		<select name="product" class="form-group customer" style="width:500px;" multiple="multiple">
	    			<option value="0" selected="selected">- All Products -</option>
	    			<?php  foreach($fw->products()->all() as $p){?>
	   					<option value="<?php echo $p['id'];?>" <?php echo ($p['id'] == $_REQUEST['product'])? 'selected="selected"' : '';?>><?php echo $p['id'] . ' -' . $p['title'];?></option>
	   				<?php }?>	
	    		</select>
	    		
	    	</div>	    	
            
            <div class="form-group">
                 <?php echo $ui->input_button_primary(array('name'=>'showhistory','value'=>'Show History','type'=>'submit'));?>
        	</div>	    	
		</div>
</form>

<?php if($_REQUEST['showhistory']){?>
		<?php 
			$pending = 0;
			$decline = 0;
			$recived = 0;
			$total = 0;		
		?>		
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Product</th>
					<th>Quantity</th>
					<th>Recived Quantity</th>
					<th>Item Price</th>
					<th>Purchase Date</th>
					<th>Recived Date</th>
					<th>Status</th>
					<th>Admin User</th>
				</tr>
			</thead>
			<tbody>				
					<?php foreach($fw->purchase()->history($_REQUEST) as $v){?>
						<tr>
							<td><?php echo $v['product_id'];?></td>
							<td><?php echo $v['quantity'];?></td>
							<td><?php echo $v['recived_quantity'];?></td>
							<td><?php echo $v['price'];?></td>
							<?php 
								$total +=$v['price'];
							?>
							<td><?php echo $v['order_date'];?></td>
							<td><?php echo $v['recived_date'];?></td>
							<td><?php echo $v['status'];?></td>	
							<?php 
								switch($v['status']){ case 'PENDING': $pending++; break; case 'DECLINE': $decline++; break; case 'RECEIVED': $recived++; break; }
							?>						
							<td>
								<?php 
									$u = $fw->users()->get(array('id'=>$v['admin_user_id']));
								?>
								<?php echo $u['fullname'];?> <br />
								<?php echo $u['email'];?>
							</td>													
						</tr>
					<?php }?>
			</tbody>
		</table>
	
<?php }?>
</div>	
<?php echo $ajax->fancyBoxByClass('editmeta');?>
<?php echo $ajax->populateParentsChildsValue(array('parent'=> 'id' , 'child'=> 'sub', 'do'=> 'json_sub.php'));?>
<?php //echo $ajax->submitForm(array('form'=>'productadd','do'=>'do_add_product.php', 'get'=>'#sys_message'));?>