<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:8px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            New Order <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
<?php 	$action = $_REQUEST['action'];
		switch($action){
			case 'Delete':	
				$_REQUEST = array_merge($_REQUEST, array('id'=> $_SESSION['SE_NEW_ORDERID']));
				if($fw->cart()->admin_delete($_REQUEST)){
					echo $fw->row_message(TRUE, 'Order Item DELETED');
				} else {
					echo $fw->row_message(FALSE, 'Order Item NOT DELETED');
				}
				break;
			case 'Update':
				$_REQUEST = array_merge($_REQUEST, array('id'=> $_SESSION['SE_NEW_ORDERID']));
				if($fw->cart()->admin_update($_REQUEST)){
					echo $fw->row_message(TRUE, 'Order Item Updated');
				} else {
					echo $fw->row_message(FALSE, 'You cannot update. <b>QOH</b> <u>Out Of Stock</u>.');
				}
			break;

			case 'reject': 
				$_REQUEST = array_merge($_REQUEST, array('id'=> $_SESSION['SE_NEW_ORDERID']));
				$fw->cart()->reject($_REQUEST);
				echo $fw->row_message(TRUE, 'Rejected !');
			break;

			case 'proceed':
				$_REQUEST = array_merge($_REQUEST, array('id'=> $_SESSION['SE_NEW_ORDERID']));
				$fw->cart()->proceed($_REQUEST);
				echo $fw->row_message(TRUE, 'Proceed !');
			break;
			
			case 'new':
				unset($_SESSION['SE_NEW_ORDERID']);
				unset($_SESSION['SE_NEW_USERID']);
				$id = NULL;
			break;
				
			case 'Create New Order':
				$fw->cart()->new_order($_REQUEST);				
				echo $fw->row_message(TRUE, 'Order Created !');	
			break;
		}
		
		$id = $_SESSION['SE_NEW_ORDERID'];

?>

<form method="post" name="productadd" action="new_order.html">
		<div class="col-lg-6">
		    
	    	
            <div class="form-group">
            	<label><a class="editmeta fancybox.ajax" href="<?php echo AJAX_PATH;?>com_products.php">  Select Product(s) </a></label>
	    		<!-- select name="add_products[]" class="product required"  required="required" style="width:500px;" multiple="multiple">
	    			<?php  //foreach($fw->products()->admin_get_orderproduct($id) as $p){?>
	   					<option value="<?php echo $p['id'];?>" <?php echo in_array($p['id'], $_REQUEST['products'])? 'selected="selected"' : '';?>><?php echo $p['id'] . ' -' . $p['title'];?></option>
	   				<?php //}?>	
	    		</select  -->
	    		
	    		<?php foreach($_SESSION['COM_PRODUCT'] as $c){?>
	    				<?php 
	    					$product = $fw->products()->loadProduct($c);
	    				?>
	    				
	    				<div class="row">
					    <div class="col-md-5" style="float:left;">
					      <div class="thumbnail">
					          <img src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_THUMB . $product[0]['image'];?>" alt="Lights" style="width:100%">
					          <div class="caption">
					            <p><?php echo $product[0]['title'];?></p>
					            <p><input type="checkbox" value="<?php echo $c?>" name="add_products[]" checked="checked" /></p>
					          </div>
					      </div>
					    </div>
					    </div>
	    		<?php }?>
	    		
	    		
	    	</div>	

	<div class="form-group">
            	<label>Select Customer</label>
	    		<select name="userid" class="customer required"  required="required" style="width:500px;" multiple="multiple">
	    			<?php  foreach($fw->users()->getbyType('USER') as $p){?>
	    				<option <?php echo ( ($_SESSION['SE_NEW_USERID'] == $p['id'])? 'selected="selected"' : '');?> value="<?php echo $p['id'];?>" <?php echo in_array($p['id'], $_REQUEST['products'])? 'selected="selected"' : '';?>><?php echo $p['fullname'];?></option>
	   				<?php }?>	
	    		</select>
	    	</div>
    	
            <div class="form-group">
            	<input type="radio" value="2" name="price_type"> - Cost Price
            	<input type="radio" value="1" name="price_type" checked="checked"> - Selling Price            	            	
            </div>
            
            <div class="form-group">
                 <input type="submit" name="action" value="Create New Order" class="btn btn-primary" />
        	</div>	    	
		</div>
</form>

<div style="clear:both;"></div>
<?php if($fw->users()->getbyid(array('id'=> $_SESSION['SE_NEW_USERID']) )->fullname !="") {?>
<div class="well">
<table class="table table-bordered table-hover">
	<tr>
		<th>Customer Name</th>
		<td><?php echo $fw->users()->getbyid(array('id'=> $_SESSION['SE_NEW_USERID']) )->fullname;?></td>
	</tr>
	
	<tr>
		<th>Email</th>
		<td><?php echo $fw->users()->getbyid(array('id'=> $_SESSION['SE_NEW_USERID']) )->email;?></td>
	</tr>	
</table>
</div>

<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>Product</th>
			<th>Unit Price</th>
			<th>Qun</th>
			<th>Total Price</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
			<?php $total = 0;?>
			<?php foreach($fw->cart()->order_list($id) as $d){?>
			<?php $product = $fw->products()->loadProduct($d['product_id']);?>
			<?php $total += $d['price'] * $d['qun'];?>			
			<tr>
				<form method="post">
					<input type="hidden" name="product_id" value="<?php echo $d['product_id'];?>" />
					<td>
						<a target="blank" href="edit_product.html?id=<?php echo $d['product_id'];?>" class="item">
							<?php echo $product[0]['title'];?>
						</a>
					</td>
					<td align="right"><?php echo number_format($d['price'], 2);?></td>
					<td><input type="text" name="qun" class="form-control" value="<?php echo $d['qun'];?>" /></td>
					<td align="right"><?php echo number_format($d['price'] * $d['qun'], 2);?></td>
					<td align="right">
						<input type="submit" name="action" value="Delete" class="btn btn-danger">
						&nbsp;
						<input type="submit" name="action" value="Update" class="btn btn-primary">
						
					</td>	
				</form>						
			</tr>			
			<?php }?>
			<?php $total_with_gsp = number_format( (($total/100) * GSP) + $total , 2); ?>
			<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Subtotal (AUD)</td>
						<td align="right"><?php echo number_format($total,2);?></td>
				   </tr>
				  <tr>
						<td></td>
						<td></td>
						<td></td>
						<td>GST(AUD)</td>
						<td align="right"><?php echo number_format(($total/100) * GSP , 2); ?></td>
				   </tr>
				   <tr>
						<td></td>
						<td></td>
						<td></td>
						<td><b>Total (AUD)</b></td>
						<td align="right"><b><?php echo $total_with_gsp;?></b></td>
				   </tr>
				</tbody>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td></td>
				<td align="center">
					<a href="new_order.html?id=<?php echo $id?>&action=reject" class="btn btn-danger">Reject Order</a>
					&nbsp;
					<a href="new_order.html?id=<?php echo $id?>&action=proceed" class="btn btn-success">Proceed Order</a>
					
				</td>								
			</tr>			
		</tbody>
</table>
<?php }?>
<?php echo $ajax->fancyBoxByClass('editmeta');?>