<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:8px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
		<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Modify Order <small><?php echo STATIC_COMPANY_NAME;?></small>
                        </h1>
                    </div>
        </div>
<?php $action = $_REQUEST['action'];
		switch($action){
			case 'Delete':	
				if($fw->cart()->admin_delete($_REQUEST)){
					echo $fw->row_message(TRUE, 'Order Item DELETED');
				} else {
					echo $fw->row_message(FALSE, 'Order Item NOT DELETED');
				}
				break;
			case 'Update':
				if($fw->cart()->admin_update($_REQUEST)){
					echo $fw->row_message(TRUE, 'Order Item Updated');
				} else {
					echo $fw->row_message(FALSE, 'You cannot update. <b>QOH</b> <u>Out Of Stock</u>.');
				}
			break;

			case 'reject': 
				$fw->cart()->reject($_REQUEST);
				echo $fw->row_message(TRUE, 'Rejected !');
			break;

			case 'proceed':
				$fw->cart()->proceed($_REQUEST);
				echo $fw->row_message(TRUE, 'Proceed !');
			break;

			case 'payment_declined':
				$fw->cart()->payment_declined($_REQUEST);
				echo $fw->row_message(TRUE, 'Payment declined!');
				break;
			
			case 'Add Product':
				$fw->cart()->admin_add_product_invoice($_REQUEST);
				echo $fw->row_message(TRUE, 'Added Product !');					
					
		}

?>

<form method="post" name="productadd">
		<div class="col-lg-6">
            <div class="form-group">
	    		<select name="add_products[]" class="product" style="width:500px;" multiple="multiple">
	    			<?php  foreach($fw->products()->admin_get_orderproduct($id) as $p){?>
	   					<option value="<?php echo $p['id'];?>" <?php echo in_array($p['id'], $_REQUEST['products'])? 'selected="selected"' : '';?>><?php echo $p['id'] . ' -' . $p['title'];?></option>
	   				<?php }?>	
	    		</select>
	    	</div>	    	
            <div class="form-group">
            	<input type="radio" value="2" name="price_type"> - Cost Price
            	<input type="radio" value="1" name="price_type" checked="checked"> - Selling Price            	            	
            </div>
            
            <div class="form-group">
                 <input type="submit" name="action" value="Add Product" class="btn btn-primary" />
        	</div>	    	
		</div>
</form>

<div style="clear:both;"></div>
<div class="well">
<table class="table table-bordered table-hover">
	<tr>
		<th>Customer Name</th>
		<td><?php echo $fw->users()->getbyid(array('id'=> $_REQUEST['userid']) )->fullname;?></td>
	</tr>
	
	<tr>
		<th>Email</th>
		<td><?php echo $fw->users()->getbyid(array('id'=> $_REQUEST['userid']) )->email;?></td>
	</tr>	
</table>
</div>

<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th>Product</th>
			<th>Unit Price</th>
			<th>Qun</th>
			<th>Total Price</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
			<?php $total = 0;?>
			<?php foreach($fw->cart()->order_list($id) as $d){?>
			<?php $product = $fw->products()->loadProduct($d['product_id']);?>
			<?php $total += $d['price'] * $d['qun'];?>			
			<tr>
				<form method="post">
					<input type="hidden" name="product_id" value="<?php echo $d['product_id'];?>" />
					<td>
						<a target="blank" href="edit_product.html?id=<?php echo $d['product_id'];?>" class="item">
							<?php echo $product[0]['title'];?>
						</a>
					</td>
					<td align="right"><?php echo number_format($d['price'], 2);?></td>
					<td><input type="text" name="qun" class="form-control" value="<?php echo $d['qun'];?>" />
						<?php if($d['agreed'] == 1 && $d['overstock_status'] == 1){ ?>
							<small style="color:green;">Agreed over stock</small>
						<?php } else if ($d['agreed'] == 0 && $d['overstock_status'] == 1){ ?>
							<small style="color:red;">Not Agreed over stock</small>
						<?php } ?>
					</td>
					<td align="right"><?php echo number_format($d['price'] * $d['qun'], 2);?></td>
					<td align="right">
						<input type="submit" name="action" value="Delete" href="customer_modify_order.html?id=<?php echo $id;?>&action=delate" class="btn btn-danger">
						&nbsp;
						<input type="submit" name="action" value="Update" href="customer_modify_order.html?id=<?php echo $id;?>&action=delate" class="btn btn-primary">
						
					</td>	
				</form>						
			</tr>			
			<?php }?>

			<?php
				$pureTotal = $total;
				$total = number_format(round($total, 1) , 2);
				$gsp   = number_format( round( ($pureTotal / 100) * GSP , 1) , 2);
				$grandtotal = number_format( round( ($pureTotal + $gsp) , 1) , 2);
			?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Total Price</td>
				<td align="right"><?php echo $total;?></td>								
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td></td>
				<td align="center">
					<a href="customer_modify_order.html?id=<?php echo $id?>&action=payment_declined" class="btn btn-warning">Payment Declined</a>
					&nbsp;
					<a href="customer_modify_order.html?id=<?php echo $id?>&action=reject" class="btn btn-danger">Reject Order</a>
					&nbsp;
					<a href="customer_modify_order.html?id=<?php echo $id?>&action=proceed" class="btn btn-success">Proceed Order</a>
					
				</td>								
			</tr>			
		</tbody>
</table>
<?php echo $ajax->submitForm(array('form'=>'edituser','get'=>'#sys_message', 'do'=>'do_edituser_cus.php'));?>