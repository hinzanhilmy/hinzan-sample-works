<?php $p = $fw->products()->get_edit($id);?>
<div class="col-lg-12">
        <h1 class="page-header">
           Add Product <small><?php echo STATIC_COMPANY_NAME;?></small>
        </h1>
     </div>
<form method="post" name="productadd" class="admin">
		<input type="hidden" name="id" value="<?php echo $id;?>" />
		<div class="col-lg-6">

		<div class="form-group">
	    		<label>IS NEW ARRIVALS</label>
	    		<input name="new_arrivals" type="checkbox" value="1" <?php echo ($p['new_arrivals']==1)? 'checked="checked"' : ''?> />
	    	</div>

			<div class="form-group">
					<label>Category</label>
			  		<select class="form-control required" name="catid">
						<option value="">-select-</option>
						<?php $fw->meta()->getDrowMultiCataogriesForSelect(0,1,$p['catagories_id']);?>
					</select>
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Title</label>
	    		<input type="text" class="form-control" name="title" value="<?php echo $p['title'];?>" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Highlight Note</label>
	    		<textarea  class="form-control" name="highlight" rows="6" cols=""><?php echo $p['highlight'];?></textarea>
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Description</label>
	    		<textarea  class="form-control richtext" name="description" rows="6" cols=""><?php echo $p['desc'];?></textarea>
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Keywords (AAAA, BBBBB, etc..)</label>
	    		<textarea  class="form-control required" name="keywords" rows="6" cols=""><?php echo $p['keywords'];?></textarea>
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Selling Price (AUD)</label>
	    		<input type="number" value="<?php echo $p['price'];?>" class="form-control required" name="price" placeholder="0.00" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Cost Price (AUD)</label>
	    		<input type="number" value="<?php echo $p['dealer_price'];?>" class="form-control required" name="d_price" placeholder="0.00" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Quantity on hand</label>
	    		<input type="text" class="form-control required" value="<?php echo $p['qoh'];?>" name="qoh" placeholder="Quantity on hand" />
	    	</div>
	    	
	    	<div class="form-group">
	    		<label>Re Order Point</label>
	    		<input type="text" class="form-control required" name="reorder_level" value="<?php echo $p['re_orderlevel'];?>" placeholder="Re order point" />
	    	</div>
	    	
			<div class="form-group">
							  <label>Images</label>
				              <ul class="row thumbnails clearfix" id="files">
								<?php foreach($fw->products()->getProductImagesByProductId($id) as $image){?>
									<li>
					  					<div class="da-img"><img src="<?php echo UPLOAD_THUMB_PATH;?><?php echo IMAGE_SIZE_THUMB . $image['file'];?>" alt="<?php echo $image['file'];?>" /></div>
					  					<div class="row">
		                                	<input type="checkbox" name="update_delete[<?php echo $image['id'];?>]" /> <label style="margin-right: 30px;"><span class="icon-trashcan"></span> Delete</label>
		                                	<input type="radio" name="default_image" value="<?php echo $image['file'];?>"  <?php echo ($image['default'] == 1 ? 'checked="checked"': '');?> /> <label>Default</label>
		                                </div>
					  				</li>
					  				<?php }?>
								</ul>
						                               
                              <div class="clearfix">
                                <div class="col-lg-6">
                                  <div class="uploader-btn">
                                    <input type="file" name="upload" id="upload" class="file-input" />
                                    <!-- <button class="btn-upload" name="action" value="upload"><span>Add Photos</span><span class="spin"></span></button> -->
                                  </div>
                                </div>                                
                              </div>
            </div>
            
			<div class="form-group">
	    		<label class="label label-success">The main performances & parameters</label>
	    		<textarea  class="form-control richtext required" name="performances" id="performances" rows="6" cols=""><?php echo $p['performances'];?></textarea>
	    	</div>


            <div class="form-group">
                 <?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Save Edit','type'=>'submit'));?>
        	</div>	    	
		</div>
		
		
</form>
<?php echo $ajax->uploadify_multi_resizeimage_withwatermark('upload','files');?>
<?php echo $ajax->submitForm(array('form'=>'productadd','do'=>'do_edit_product.php', 'get'=>'#sys_message'));?>