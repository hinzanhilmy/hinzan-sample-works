<?php if($_SESSION['SUSERTYPE'] != 'ADMIN'){  exit("<h3 style='background:red; color:white; padding:4px; font-size:11px;'>Your are not authorized to access this page.</h3>");}?>
<h3>Category Management</h3>
<hr />
<form method="post" name="meta" style="width:450px;">
	<p>Select Catagory</p>
			<p>
				<select class="form-control" name="id">
					<option value="">- add new top level -</option>
					<?php $fw->meta()->getDrowMultiCataogriesForSelect(0,1);?>
				</select>
			</p>
			
			<p>Catagory Name</p>
			<p><input type="text" name="name" value="" class="span3 form-control" required="required" /></p>
			
			<div class="form-group">
							  <label>Icon</label>
				              <ul class="thumbnails clearfix" id="files"></ul>                               
                              <div class="clearfix">
                                <div class="col-lg-6">
                                  <div class="uploader-btn">
                                    <input type="file" name="upload" id="upload" class=" form-control" />
                                    <!-- <button class="btn-upload" name="action" value="upload"><span>Add Photos</span><span class="spin"></span></button> -->
                                  </div>
                                </div>                                
                              </div>
            </div>
            
			
			<p> <?php echo $ui->input_button_primary(array('name'=>'AddMeta','type'=>'Submit','value'=>'Add Category'));?></p>
</form>
<script language="javascript" type="text/javascript">
	$(document).ready(function (data){
		$('.other').hide();
		$('input[name=code]').click(function (data){
			if($(this).val() == "OTHER"){
				$('.other').show(400);
			} else {
				$('.other').hide(400);
			}
		});
	});
</script>
<?php echo $ajax->uploadify_single('upload','files');?>
<?php echo $ajax->submitForm(array('form'=>'meta','get'=>'#sys_message', 'do'=>'do_category.php'));?>

<hr />
<h3>Search Catagory</h3>

	<form method="post">
	<div class="row">
                    <div class="col-lg-6">
                            <div class="form-group">
                            	<select class="form-control" name="cat">
									<?php foreach($fw->meta()->getMainCats() as $c){?>
										<option value="<?php echo $c['id'];?>"><?php echo $c['name'];?></option>
									<?php }?>
								</select>
                            </div>
                            
                            <div class="form-group">
                            	<input type="text" name="search_text" value="<?php echo $fw->xss()->safe($_REQUEST['search_text']);?>" class="form-control" />
                            	<p class="help-block">Search product text</p>
                            </div>                            
                            
                            <div class="form-group">
                                <?php echo $ui->input_button_primary(array('name'=>'btn_search','value'=>'Search &amp; Batch Edit','type'=>'submit'));?>
                            </div>
                     </div>    
	</div>
	</form>        
	<?php if($_REQUEST['btn_search']){?>
		<form method="post" name="form-sort">
				<input type="hidden" name="cat" value="<?php echo $_REQUEST['cat'];?>" />
				<input type="hidden" name="search_text" value="<?php echo $_REQUEST['search_text'];?>" />
				<table class="table table-bordered table-hover">
				<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Order</th>
					<th></th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
					<?php $x=1;?>
					<?php foreach($fw->catagories()->search($_REQUEST) as $d){?>
					<tr>
						<td><?php echo $x++;?></td>
						<td>
							<small>
							<?php 
								if(NULL!=$fw->meta()->getCatNameById($d['parent'])){
									$cat = $fw->meta()->getCatNameById($d['parent']);
									echo  '<b>' . $cat['name']  . '</b> &raquo; ' . $d['label']; 
								} else {
									echo $d['label'];
								}	
							
							?>
							</small>
						</td>
						<td><input type="number" class="form-control" style="width: 100px;" name="sort[<?php echo $d['id'];?>]" value="<?php echo $d['sort'];?>" /></td>				
						<td><span class="label"><a class="editmeta  btn btn-success fancybox.ajax" style="color:white;" href="<?php echo AJAX_PATH;?>ui_do_editcat.php?id=<?php echo $d['id'];?>">Edit</a></span></td>
						<td><span class="label"><a class="editmeta btn btn-danger fancybox.ajax" href="<?php echo AJAX_PATH;?>do_cat_action.php?id=<?php echo $d['id'];?>&action=deletename">Delete</a></span></td>
					</tr>
					<?php }?>
				</tbody>
		</table>
				<div class="form-group">
                 <?php echo $ui->input_button_primary(array('name'=>'btnlogin','value'=>'Edit Sort','type'=>'submit'));?>
        		</div>	
		</form>
		<?php echo $ajax->submitForm(array('form'=>'form-sort','do'=>'do_edit_sort.php', 'get'=>'#sys_message'));?>
		
		<?php echo $ajax->fancyBoxByClass('editmeta');?>
	<?php } else {?>	
			<div class="pagination"></div>
			<div class="pagecontent"></div>
			<?php echo $ajax->twbsPagination(array('content'=>'.pagecontent','pagination'=> '.pagination', 'do'=>'ui_category.php','total'=> $fw->catagories()->total()));?>
	<?php  }?>