<!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

                <a class="navbar-brand" href="/parts/lpanel/index.html"><?php echo STATIC_COMPANY_NAME;?> Admin</a>

            </div>

            <!-- Top Menu Items -->

            <?php if(!$fw->users()->isLogin() == FALSE){?>
            <ul class="nav navbar-right top-nav">

               <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo ($_SESSION['SUSERNAME']!="") ? $_SESSION['SUSERNAME'] : 'Login';?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/parts/lpanel/logout.html"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <?php }?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <div class="collapse navbar-collapse navbar-ex1-collapse">

                <ul class="nav navbar-nav side-nav">
                	<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
			          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#salesorders" data-parent="#exampleAccordion">
			            <i class="fa fa-fw fa-wrench"></i>
			            <span class="nav-link-text">Sales Order</span>
			          </a>
			          <ul class="sidenav-second-level collapse" id="salesorders">
				    <li>
			              <a href="/parts/lpanel/new_order.html?action=new">New</a>
			            </li>

			            <li>
			              <a href="/parts/lpanel/orders.html">Sales order</a>
			            </li>
			            <li>
			              <a href="/parts/lpanel/audit.html">Audi Trail</a>
			            </li>
			          </ul>
			        </li>
                    
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
			          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
			            <i class="fa fa-fw fa-wrench"></i>
			            <span class="nav-link-text">Customer Managment</span>
			          </a>
			          <ul class="sidenav-second-level collapse" id="collapseComponents">
			            <li>
			              <a href="/parts/lpanel/customers.html">Manage</a>
			            </li>
			            <li>
			              <a href="/parts/lpanel/feedback.html">Feedback</a>
			            </li>
			          </ul>
			        </li>
			        
                    
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
			          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#PurchaseOrders" data-parent="#PurchaseOrders">
			            <i class="fa fa-fw fa-wrench"></i>
			            <span class="nav-link-text">Inventry Managment</span>
			          </a>
			          <ul class="sidenav-second-level collapse" id="PurchaseOrders">
			            <li>
			              <a href="/parts/lpanel/stock.html">Stock Alerts</a>
			            </li>
			            <li>
			              <a href="/parts/lpanel/stock_status.html">Stock Status</a>
			            </li>
			          </ul>
		    </li>
	
        	    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
			          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#Purchase" data-parent="#PurchaseOrders">
			            <i class="fa fa-fw fa-wrench"></i>
			            <span class="nav-link-text">Purchase Orders</span>
			          </a>
			          <ul class="sidenav-second-level collapse" id="Purchase">
			            <li>
			              <a href="/parts/lpanel/purchase_orders.html">New</a>
			            </li>
			            <li>
			              <a href="/parts/lpanel/purchase_orders_history.html">Purchase History</a>
			            </li>
			          </ul>
			        </li>                    
                    
                    </li>
                    
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
			          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#Products" data-parent="#PurchaseOrders">
			            <i class="fa fa-fw fa-wrench"></i>
			            <span class="nav-link-text">Products</span>
			          </a>
			          <ul class="sidenav-second-level collapse" id="Products">
			            <li>
			               <a href="/parts/lpanel/add_product.html"><i class="fa fa-fw fa-user"></i> Add Product</a>
			            </li>
			            <li>
			              <a href="/parts/lpanel/list_product.html"><i class="fa fa-fw fa-user"></i> View Products</a>
			            </li>
			          </ul>
			        </li>
			        
			        
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
			          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#System" data-parent="#PurchaseOrders">
			            <i class="fa fa-fw fa-wrench"></i>
			            <span class="nav-link-text">System</span>
			          </a>
			          <ul class="sidenav-second-level collapse" id="System">
			            <li>
			              <a href="/parts/lpanel/users.html"><i class="fa fa-fw fa-user"></i> Users Manage</a>
			            </li>
			            <li>
			              <a href="/parts/lpanel/category-management.html"><i class="fa fa-fw fa-user"></i> Category</a>
			            </li>
			          </ul>
			        </li>
		    <li>
                        <a href="/parts/lpanel/logout.html"><i class="fa fa-fw fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

        </nav>