<?php
class vendor extends db {	
	public function __construct(){
		parent::__construct();
	}
	
	public function byproduct($id){
		$sql = "SELECT *, (SELECT `status` FROM `purchase_orders` WHERE `product_id` = `supplier_products`.`product_id`  AND `users_id` = `supplier_products`.`users_id` ORDER BY `id` DESC LIMIT 1) as `or_status`,
							(SELECT `id` FROM `purchase_orders` WHERE `product_id` = `supplier_products`.`product_id`  AND `users_id` = `supplier_products`.`users_id` ORDER BY `id` DESC LIMIT 1) as `purchase_orders_id`,
						  (SELECT `title` FROM `product` WHERE `id` = `supplier_products`.`product_id` LIMIT 1) AS `title`,
						  (SELECT `qoh` FROM `product` WHERE `id` = `supplier_products`.`product_id` LIMIT 1) AS `qoh`,
						  (SELECT `quantity` FROM `purchase_orders` WHERE `product_id` = `supplier_products`.`product_id`  AND `users_id` = `supplier_products`.`users_id`  ORDER BY `id` DESC LIMIT 1) as `or_quantity`,
						  (SELECT `price` FROM `purchase_orders` WHERE `product_id` = `supplier_products`.`product_id` AND `users_id` = `supplier_products`.`users_id` ORDER BY `id` DESC LIMIT 1) as `or_price`,
						  (SELECT `order_date` FROM `purchase_orders` WHERE `product_id` = `supplier_products`.`product_id`  AND `users_id` = `supplier_products`.`users_id` ORDER BY `id` DESC LIMIT 1) as `or_date`,
						  (SELECT `modified_date` FROM `purchase_orders` WHERE `product_id` = `supplier_products`.`product_id`  AND `users_id` = `supplier_products`.`users_id`  ORDER BY `id` DESC LIMIT 1) as `or_prdate`
				  FROM `supplier_products` 
					JOIN `users` ON
						`users`.`id` = 	 `supplier_products`.`users_id`
						AND `users`.`account_type` = 'SUPPLIER'
				WHERE `product_id` = '$id';";
		return parent::query($sql);
	}
	
	public function order($data){
		global $fw;
		$productid = $data['productid'];
		$vendorid = $data['vendorid'];
		$qun = $data['qun'];
		$date = date('Y-m-d');
		$admin_userid = $_SESSION['SUSERID'];
		
		$sql = "INSERT INTO `purchase_orders` (`users_id`, `product_id`, `quantity`, `status`, `order_date`, `admin_user_id`) 
					VALUES('$vendorid', '$productid', '$qun', 'PENDING', '$date', '$admin_userid');";
		parent::query($sql);
		$orderid = parent::getLastId();
		
		$user = $fw->users()->getbyid(array('id'=>$vendorid));
		$product = $fw->products()->get_edit($productid);
		
		$html_table = '<table width="100%" border="0" cellpadding="8" cellspacing="0" style="border: 1px solid #EAEAEA;border-top: 4px solid #F7F7F7;">
							<tr>
								<td  style="border-top: 1px solid #EAEAEA; border-bottom: 1px solid #EAEAEA; border-right: 1px solid #EAEAEA; text-align: center; color: #9E9E9E;">Product Name</td>
								<td  style="border-top: 1px solid #EAEAEA; border-bottom: 1px solid #EAEAEA; border-right: 1px solid #EAEAEA; text-align: center; color: #9E9E9E;">Quantity</td>
							</tr>
							<tr>
								<td style="border-right: 1px solid #EAEAEA; vertical-align: top; line-height: 20px;text-align:center;">'.$product['title'].'</td>
								<td style="border-right: 1px solid #EAEAEA; vertical-align: top; line-height: 20px;text-align:center;">'.$qun.'</td>
							</tr>
						</table>';	
		
		$mail_data = array(	'fullname'=> $user->fullname, 
						  	'email'=> $user->email,
							'company'=> $user->company,
							'address_streetno'=> $user->streetno,
							'address_streetname'=> $user->streetname,
							'postal_code'=> $user->postal_code,
							'suburb'=> $user->suburb,
							'state'=> $user->state,				
							'abn'=> $user->abn,
							'phone'=> $user->phone,
							'message'=> $html_table, 
							'subject'=> 'GMT Order',
							'pdf_name'=> 'GMT_VO_' . date('Ydm_his'),
							'id'=> 'GMT/V/O/' . $orderid);		
		
		$fw->pdf()->set_vendor_perches_order($mail_data);
		$fw->mail()->send_pdf_tovendor($mail_data);
		return TRUE;
	}
	
	public function get_order($id){
		$sql = "SELECT * FROM `purchase_orders` WHERE `id` = '$id';";
		return parent::query2($sql);
	}
	
	public function add_vendor_toproduct($data){
		$vendors = $data['vendors'];
		$id = $data['id'];
		foreach($vendors as $v){
			$sql = "INSERT INTO `supplier_products` (`users_id`, `product_id`)
						VALUES ('$v', '$id');";
			parent::query($sql);
		}
		return parent::status();
	}
}
?>