<?php
class Ajax{
		
	public function submitForm($data=array()){
		
		//form - name attribute
		//do   - ajax page do_
		//get - ajax result
		//do_pages
		//fancybox - open fancy box
		//inline_result_id - inline result
		//inline_result_url - inline result load url
		$ajax_path = ($data['ajax_path']!="")?$data['ajax_path'] :  AJAX_PATH;
		$emailCheckUrlForRegister = $ajax_path."do_email_check.php";		
		$checkProductDuplicate = $ajax_path."do_product_check.php";

		if($data['form']=="register"){
			$rules = 'rules:{
								email:{
								email:true,
								remote: {
									url:\''.$emailCheckUrlForRegister.'\',
									type: "post",
											data:{
												email: function(){
													return $(\'#register :input[name="email"]\').val()
												}
											}
										}
								}
							},
							messages:{
								email:{
									remote:"This email address has been registered or invalid"
								}
							},';
		}


		if($data['form']=="productadd"){
				$rules = 'rules:{
							title:{
								required : true,
								remote: {
									url:\''.$checkProductDuplicate.'\',
									type: "post",
										data: {
											title: function (){
												return $(\'#productadd :input[name="title"]\').val()
											}
										}
									}
									
								}
							},
							messages:{
								title:{
									remote: "Product already exists.",
									required : "Please fill the product name"
								}
							},';
		}		
		
		return '<script language="javascript" type="text/javascript">
					$(document).ready(function (data){						
						$(\'form[name='.$data['form'].']\').validate({						
							// End rules for add new products							
							'. $rules .'
							
					submitHandler: function(form) {
						$.ajax({
							type : \'post\',
							url	   : \'' . $ajax_path . ''. $data['do'].'\',
							data   : $(form).serialize(),
							dataType : \'json\',
							success : function(data){
								if(data.status){
									$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message information").addClass("success").fadeIn(1000).delay(2000).fadeOut(1000);
									if(data.inline_result){ 
										$(data.inline_result_id).hide().load(data.inline_result_url).slideDown(600);
										$(\'form[name='.$data['form'].']\').hide();
									}						
													
									if(data.fancybox){
											$.fancybox.open({
												 type: \'ajax\',
												\'href\'	:	data.fancyboxurl,
												fitToView	: true,
												autoSize	: true,
												closeClick	: true,
												openEffect	: \'none\',
												closeEffect	: \'none\'
											});
										}
									if(data.jredirect){
										window.location = data.jredirecturl;
									}	
								} else {
									
									$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message success").addClass("information").fadeIn(1000).delay(2000).fadeOut(1000);
									if(data.jredirect){
										window.location = data.jredirecturl;
									}	
								}
							},
							error : function(data) { alert("missing file"); }
						});
						
						return false;
					 }
					});
					});
				</script>';
	}
	
	public function link_post($data){
		return '<script language="javascript">
					$(document).ready(function (data){		
							$(".'.$data['class'].'").on(\'click\', function (data){
								if(confirm($(this).attr(\'js-message\'))){	
										$(this).parent().parent().fadeOut(300);								
										$.ajax({
											type : \'post\',
											url	   : \'' . AJAX_PATH . ''. $data['do'].'\',
											data   : $(this).attr(\'href\'),
											dataType : \'json\',
											conext : true,
											success : function(data){
												if(data.status){												
													if(!data.fancybox){
														
													}												
													$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message information").addClass("success").fadeIn(1000).delay(2000).fadeOut(1000);
												} else {
													$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message success").addClass("information").fadeIn(1000).delay(2000).fadeOut(1000);
												
												}
												if(data.jredirect){
													window.location = data.jredirecturl;
												}
												
												if(data.fancybox){
													$.fancybox.open({
														 type: \'ajax\',
														\'href\'	:	data.fancyboxurl,
														fitToView	: true,
														autoSize	: true,
														closeClick	: true,
														openEffect	: \'none\',
														closeEffect	: \'none\'
													});
												}
											}
											/*error : function (data){ alert(\'error - link_post ajax\');}*/
									});
									return false;
								} else { return false; }
							});
							
					});
				</script>';
	}

	public function authForm($data=array()){
		//form - name attribute
		//do   - ajax page do_
		//get - ajax result
		//do_pages
		//fancybox - open fancy box
		//inline_result_id - inline result
		//inline_result_url - inline result load url
		$ajax_path = ($data['ajax_path']!="")?$data['ajax_path'] :  AJAX_PATH;
		
		return '<script language="javascript" type="text/javascript">
					$(document).ready(function (data){								
						$(\'form[name='.$data['form'].']\').validate({
								submitHandler: function(form) {	
									if($(\'button[name=submit]\').attr(\'is-login\')=="false"){
										 $.fancybox.open({
									 		    href : \'' . $ajax_path . 'ui_login.php\',
												type: \'ajax\',
												modal:false
										  });
									 } else {
									  	$.ajax({
											type : \'post\',
											url	   : \'' . $ajax_path . ''. $data['do'].'\',
											data   : $(form).serialize(),
											dataType : \'json\',
											success : function(data){
												if(data.status){
													$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message information").addClass("success").fadeIn(1000).delay(2000).fadeOut(1000);
													if(data.inline_result){ 
														$(data.inline_result_id).hide().load(data.inline_result_url).slideDown(600);
														$(\'form[name='.$data['form'].']\').hide();
													}						
																	
													if(data.fancybox){
															$.fancybox.open({
																 type: \'ajax\',
															 	\'href\'	:	data.fancyboxurl,
															 	fitToView	: false,
																autoSize	: true,
																closeClick	: false,
																openEffect	: \'none\',
																closeEffect	: \'none\'
															});
														}
													if(data.jredirect){
														window.location = data.jredirecturl;
													}	
												} else {
													
													$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message success").addClass("information").fadeIn(1000).delay(2000).fadeOut(1000);
													if(data.jredirect){
														window.location = data.jredirecturl;
													}	
												}
											},
											error : function(data) { alert("wbt: do missing file"); }
										});
									} // auth
									return false;
								 }
						});
					});
				</script>';
	}

	public function submitFormWithValidate($data=array()){
		//form - name attribute
		//do   - ajax page do_
		// get - ajax result
	return '<script language="javascript" type="text/javascript">
					$(document).ready(function (data){
						$(\'form[name='.$data['form'].']\').validate({
								submitHandler: function(form) {
									$.ajax({
										method : \'post\',
										url	   : \'' . AJAX_PATH . ''. $data['do'].'\',
										data   : $(form).serialize(),
										dataType : \'json\',
										success : function(data){
											if(data.status){
												$(data.innerclass).attr(\'is-login\',\'true\');
												$(data.innerclass).attr(\'is-upgrade\',\'true\');
												$(\'button\').attr(\'is-login\',\'true\');
												$(\'a\').attr(\'is-login\',\'true\');
												$(\'a\').attr(\'account_type\', data.account_type);
												
												$(\'.quick-links-nlog\').removeClass(\'col-lg-1 col-md-1 col-sm-1 col-xs-2\');
												$(\'.quick-links-nlog\').addClass(\'col-lg-3 col-md-4 quick-links logedinmenu\');
												
												$(\'.logedinsearchbar\').removeClass(\'col-lg-9 col-md-9 col-sm-9 col-xs-10 search-bar\');
												$(\'.logedinsearchbar\').addClass(\'col-lg-7 col-md-6 col-sm-8 col-xs-10\');

												$("div").removeClass(\'quick-links-nlog\');
												
												$.post(\'' . AJAX_PATH . 'ui_logedinmenu.php\' , function( data ) {
												  $(\'.logedinmenu\').hide().fadeIn(300).html( data );
												});
														
												$.post(\'' . AJAX_PATH . 'ui_logedinmenu_navigation.php\' , function( data ) {
												  $(\'.navigation .gn-menu\').html( data );
												});

												$(\'.navigation_nlog\').show();	

												$.fancybox.close();
												if(data.fancybox){
															$.fancybox.open({
																type: \'ajax\',
															 	href	:	data.fancyboxurl,
															 	fitToView	: false,
																autoSize	: true,
																closeClick	: false,
																openEffect	: \'none\',
																closeEffect	: \'none\'
															});
												}
												
												if(data.jredirect){
													location = data.jredirecturl;
												}
												
												if(data.is_hide=="true"){
													//$("a[is-hide=true]").fadeOut(100);
												}
												
											} else {
												$(data.message).html("<div class=block-error>Username or password is incorrect</div>");
											}
										},
										error : function(data) {alert(\'missing file\'); }
									});
									return false;
								 }
						});
					});
				</script>';
	}
	public function link_post_noconfirm($data){
		return '<script language="javascript">
					$(document).ready(function (data){
							$(".'.$data['class'].'").click(function(data){
										$.ajax({
											type : \'post\',
											url	   : \'' . AJAX_PATH . ''. $data['do'].'\',
											data   : $(this).attr(\'href\'),
											dataType : \'json\',
											conext : true,
											success : function(data){
												if(data.status){
													$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message information").addClass("success").fadeIn(1000).delay(2000).fadeOut(1000);
												} else {
													$(\''. $data['get'] .'\').html(data.message).removeClass("sys_message success").addClass("information").fadeIn(1000).delay(2000).fadeOut(1000);
	
												}
												if(data.jredirect){
													window.location = data.jredirecturl;
												}
											}
											/*error : function (data){ alert(\'error - link_post ajax\');}*/
									});
									return false;
							});
				
					});
				</script>';
	}
	
	public function select($data){
		//http://brianreavis.github.io/selectize.js/
		$id = $data['id'];
		return '<script type="text/javascript" language="javascript">
				$("#'.$id.'").selectize({
				    		maxItems: null
				});
		</script>';
	}
	
	public function select_free($data){
		$id = $data['id'];
		return '<script type="text/javascript" language="javascript">
			$("#'.$id.'").selectize({
				maxItems: null,
				persist: false,
				create: function (input){
					value: input,
					text: input
				}
			});
		</script>';
	}

	public function magic_select($data){
		//http://nicolasbize.com/magicsuggest/doc.html	
		$id = $data['id'];
		return '<script type="text/javascript" language="javascript">
					$("#'.$id.'").magicSuggest({
						placeholder: \'Ex: iphone, entertainment, electricity\',
						/*maxSelection : 3,*/
						/* data: \'' . AJAX_PATH . ''. $data['do'].'\',
  						minChars: 1 */	
					});
		</script>';
	}
	
	public function formValidate($data){
		return '<script type="text/javascript" language="javascript">
					$(document).ready(function (data){
						$(\'form[name='.$data['form'].']\').validate()
					});
				</script>';
	}
	
	public function fancybox_close($data){
		return '<script language="javascript" type="text/javascript">
		$(document).ready(function (data){
			$(\'input[name='.$data['name'].']\').click( function (data){
				try {$.fancybox.close();} catch(e){}
			});
		});
		</script>';
	}
	
	public function showHide($click='',$id=''){
		return '<script language="javascript" type="text/javascript">
					$(document).ready(function (data){
						$(\''.$click.'\').click( function (data) {
							$(\''.$id.'\').hide().fadeIn(1000);
						});
					});
				</script>';
	}
	
	public function loadPage($data=array()){
		//form - name attribute
		//do   - ajax page do_
		//get - ajax result
		return '<script language="javascript" type="text/javascript">
					$(document).ready(function (data){
							$(\'form[name='.$data['form'].']\').submit(function (data){
								$.ajax({
									method : \'post\',
									url	   : \'' . AJAX_PATH . ''. $data['do'].'\',
									data   : $(this).serialize(),
									dataType : \'html\',
									success : function(data){
										$(\''. $data['get'] .'\').hide().html(data).slideDown(800);
									},
									error : function(data) {alert(\'missing file\'); }
								});
								return false;
							});
					});
				</script>';
	}
	
	public function populateParentsChilds($data=array()){
		//parent
		//do
		//child
		
		return '<script type="text/javascript" language="javascript">
					$(document).ready(function (data){
						// if refreshed
						$(\'select[name='.$data['child'].']\').append("<option value=" + 0 + ">-select-</option>");	
								$.ajax({
									type : \'post\', 
									dataType : \'json\',
									url	 : \'' . AJAX_PATH . ''. $data['do'].'\',
									data : \'catagory=\' + $(\'select[name='.$data['parent'].']\').val(),
									success : function(data){
										$.each(data, function(index,value){
											$(\'select[name='.$data['child'].']\').append("<option value=" + value.id + ">" + value.name + "</option>");
										});
									}
						});
													
						$(\'select[name='.$data['parent'].']\').change(function (data){
							$(\'select[name='.$data['child'].']\').empty();
								$(\'select[name='.$data['child'].']\').append("<option value=" + 0 + ">-select-</option>");	
								$.ajax({
									type : \'post\', 
									dataType : \'json\',
									url	 : \'' . AJAX_PATH . ''. $data['do'].'\',
									data : $(this).serialize(),
									success : function(data){
										$.each(data, function(index,value){
											$(\'select[name='.$data['child'].']\').append("<option value=" + value.id + ">" + value.name + "</option>");
										});
									}
								});
						});
					});
					</script>';
	} 
	
	public function populateParentsChildsValue($data=array()){
		//parent
		//do
		//child
	
		return '<script type="text/javascript" language="javascript">
					$(document).ready(function (data){
					$(\'select[name='.$data['parent'].']\').change(function (data){
					$(\'select[name='.$data['child'].']\').empty();
                                        $(\'select[name='.$data['child'].']\').append("<option value=" + 0 + ">-select-</option>");
					$.ajax({
					type : \'post\',
					dataType : \'json\',
					url	 : \'' . AJAX_PATH . ''. $data['do'].'\',
					data : $(this).serialize(),
					success : function(data){
					$.each(data, function(index,value){
					$(\'select[name='.$data['child'].']\').append("<option value=" + value.value + ">" + value.name + "</option>");
				});
				}
				});
				});
				});
				</script>';
	}
	
	public function tagAutoComplete($data){
		$id = $data['id'];		
		return '<script type="text/javascript">
        $(document).ready(function() {
        	$(\''. $id .'\').tokenInput("http://shell.loopj.com/tokeninput/tvshows.php", {
                theme: "facebook"
            });
        });
        </script>';
	}
	
	public function fancyBoxAuto($url){
		return '<script type="text/javascript">
				 $(document).ready(function (data){
						$.fancybox.open({
					 		    href : \''.$url.'\',
								type: \'ajax\',
								modal:false
						});
					});
				</script>';
	}
	
	public function fancyBox($id){
		return '<script type="text/javascript">
					 $("#'.$id.'").fancybox({
						  	\'overlayShow\'	: true,
							\'transitionIn\'	: \'fade\',
							\'transitionOut\'	: \'elastic\',
							\'width\'			: \'75%\',
							\'height\'			: \'100%\'
						});
				</script>';
	}
	
	public function fancyBoxByRel($name){
		return '<script type="text/javascript">
					 $("a[rel='.$name.']").fancybox({
						  	\'overlayShow\'	: true,
							\'transitionIn\'	: \'fade\',
							\'transitionOut\'	: \'fade\',
							\'width\'			: \'75%\',
							\'height\'			: \'100%\',
							\'hideOnOverlayClick\' : true,
							\'type\': \'inline\'
						});
				</script>';
	}
	
	public function fancyBoxByClass($name){
		return '<script type="text/javascript">
					 $(".'.$name.'").fancybox({
				  			fitToView	: false,
							autoSize	: true,
							closeClick	: false,
							openEffect	: \'none\',
							closeEffect	: \'none\'
						});
				</script>';
	}
	
	
	public function mask($id,$patten){
		return '<script language="javascript" type="text/javascript">
					$(document).ready(function (data){
						 $("#'.$id.'").mask("'.$patten.'");
					});
				</script>';
	}
	
	public function uploadify_Auto($id,$response){
		return '<script type="text/javascript" language="javascript">
				$(document).ready(function() {
				  $(\'#'.$id.'\').uploadify({
					\'uploader\'  : \'' . PATH_3DPARTY . 'uploadify/uploadify.swf\',
					\'script\'    : \'' . PATH_3DPARTY . 'uploadify/uploadify_image_resize_watermark_small.php\',
					\'cancelImg\' : \'' . PATH_3DPARTY . 'uploadify/cancel.png\',
					\'folder\'    : \'' . UPLOAD_ITEM_PATH . '\',
					\'auto\'      : true,
					\'multi\'    : false,
					\'fileSizeLimit\' : \'70KB\',
					\'uploadLimit\' : 1,
					\'fileExt\'     : \'*.jpg;*.png;*.gif;*.jpeg\',
					\'onComplete\'  : function(event, ID, fileObj, response, data) {
						$(\'#'.$response.'\').empty();
						$(\'#'.$response.'\').append(\'<li class="span4"><div class="thumbnail"><img src="'. UPLOAD_THUMB_PATH .'\' + response + \'"><input type="checkbox" name="images[]" value="\' + response + \'" checked /> Select</div></li>\');
					}
				  });				  	
				});
				</script>';
	}
	
	public function uploadify($id,$response){
		//<a href="javascript:$('#file_upload').uploadifyUpload($('.uploadifyQueueItem').last().attr('id').replace('file_upload',''));">Upload Last File</a>
		return '<script type="text/javascript" language="javascript">
				$(document).ready(function() {
				  $(\'#'.$id.'\').uploadify({
					\'uploader\'  : \'' . PATH_3DPARTY . 'uploadify/uploadify.swf\',
					\'script\'    : \'' . PATH_3DPARTY . 'uploadify/uploadify.php\',
					\'cancelImg\' : \'' . PATH_3DPARTY . 'uploadify/cancel.png\',
					\'folder\'    : \'' . UPLOAD_CV_PATH . '\',
					\'auto\'      : true,
					\'multi\'	  : false,
					\'queueSizeLimit\' : 0,
					\'fileExt\'     : \'*.jpg;*.png;*.gif;*.jpeg;*.JPG\',
					\'onComplete\'  : function(event, ID, fileObj, response, data) {
						 $(\'#'.$response.'\').empty();
						 $(\'#'.$response.'\').append(\'<li class="span6"><div class="thumbnail"><img src="'. UPLOAD_FOLDER_PATH .'\' + response + \'"><br /><input type="hidden" name="hidden_images[]" value="\' + response + \'"/><input type="checkbox" name="images[]" value="\' + response + \'" checked /></div></li>\');
					}
				  });
				  	
				});
				</script>';
	}
	
	public function uploadify_multi_resizeimage_withwatermark($id,$response,$manual_profile_id=0){
		if($manual_profile_id == 0){
			$_SESSION['manual_profile_id'] = $_SESSION['SLOGINUSERID'];
		} else {
			$_SESSION['manual_profile_id'] = $manual_profile_id;
		}		
		return '<script type="text/javascript" language="javascript">
				$(document).ready(function() {
					$(\'#'.$id.'\').html5Uploader({
						 postUrl: \'' . PATH_3DPARTY . 'up/uploadify_image_resize_watermark.php\',
						 onSuccess: function (e,file,response){
						 	if(response!="FALSE"){							 								
									$(\'#'.$response.'\').append(\'<li class="span2"><a href="\' + response + \'" class="delete_image" title="Remove this Image"><span class="icon-closed-deals"></span></a><div class="thumbnail"><div class="da-img"><img src="'. UPLOAD_HTTP_PATH_TEMP . IMAGE_SIZE_THUMB .'\' + response + \'"></div><br /> <hr /> <input type="hidden" name="hidden_images[]" value="\' + response + \'"/><div class="check-upload"><input type="checkbox" name="images[]" value="\' + response + \'" checked />Upload</div><div class="check-default"><input type="radio" name="default_image" value="\' + response + \'" checked/> Default</div></div></li>\');
									$("b").remove(); 	$(\'#'.$id.'\').val("");
							} else {
								$(\'#'.$response.'\').append("<b class=\'status\'>Upload fail.</b>");		
							}
						},
						onClientLoadStart : function (e){
				           $(\'#'.$response.'\').append("<b class=\'status\'>Uploading...</b>");
				        }
					});
				});
				</script>';	
	}

	public function uploadify_single($id,$response,$manual_profile_id=0){
		if($manual_profile_id == 0){
			$_SESSION['manual_profile_id'] = $_SESSION['SLOGINUSERID'];
		} else {
			$_SESSION['manual_profile_id'] = $manual_profile_id;
		}
		return '<script type="text/javascript" language="javascript">
					$(document).ready(function() {
						$(\'#'.$id.'\').html5Uploader({
							postUrl: \'' . PATH_3DPARTY . 'up/uploadify_image_resize_watermark.php\',
						onSuccess: function (e,file,response){
						if(response!="FALSE"){
							$(\'#'.$response.'\').html(\'<li class="span6"><a href="\' + response + \'" class="delete_image" title="Remove this Image"><span class="icon-closed-deals"></span></a><div class="thumbnail"><div class="da-img"><img src="'. UPLOAD_HTTP_PATH_TEMP . IMAGE_SIZE_THUMB .'\' + response + \'"></div><br /><input type="hidden" name="hidden_images[]" value="\' + response + \'"/><div class="check-upload"><input type="checkbox" name="images[]" value="\' + response + \'" checked />Upload</div><div class="check-default"></div></div></li>\');
						$("b").remove(); 	$(\'#'.$id.'\').val("");
							} else {
								$(\'#'.$response.'\').append("<b class=\'status\'>Upload fail.</b>");
								}
							},
							onClientLoadStart : function (e){
								$(\'#'.$response.'\').append("<b class=\'status\'>Uploading...</b>");
								}
								});
							});
					</script>';
	}
	public function uploadify_CV($id,$response,$email){
		
		return '<script type="text/javascript" language="javascript">
					$(document).ready(function() {
						$(\'#'.$id.'\').html5Uploader({
							postUrl: \'' . PATH_3DPARTY . 'up/uploadify_cv.php\',
						onSuccess: function (e,file,response){
						
						console.log(file);	
							
						if(response!="FALSE"){
							//console.log(\'Hinzan\'+file);
							//console.log(response);
							$(\'#'.$response.'\').html(\'<div><input type="hidden" name="hidden_images" value="\' + response + \'"/>\'+file.name+\'</div>\');
							} else {
								$(\'#'.$response.'\').append("<b class=\'status\'>Upload fail.</b>");
								}
							},
							onClientLoadStart : function (e){
							$(\'#'.$response.'\').append("<b class=\'status\'>Uploading...</b>");
								}
								});
							});
					</script>';
	}
	
	public function setAutoSize($id,$normal,$width){
		return '<script type="text/javascript" language="javascript">
				$(document).ready(function() {
					$(\'#'.$id.'\').focus(function(){						
						$(this).animate({
							width: '.$width .'
						}, 800 )
					});
					$(\'#'.$id.'\').blur(function(){						
						$(this).animate({
							width: '.$normal .'
						}, 800 )
					});
				});
				</script>';
	}
	
	public function setAutoSizeHeight($id,$normal,$height){
		return '<script type="text/javascript" language="javascript">
				$(document).ready(function() {
					$(\'#'.$id.'\').focus(function(){						
						$(this).animate({
							height: '.$height .'
						}, 800 )
					});
					$(\'#'.$id.'\').blur(function(){						
						$(this).animate({
							height: '.$normal .'
						}, 800 )
					});
				
				});
				</script>';
	}
	public function bootpage($data){
		$contentpanel = $data['content'];
		$paginationpanel = $data['pagination'];
		$total = round($data['total'] / MAX_ITEMS_PER_PAGE, 0);
		$do 	= $data['do'];
		
		$searchform = $data['form'];
		
    	return '<script type="text/javascript" language="javascript">
    				$("'.$contentpanel.'").load("'. AJAX_PATH . 'pagination/' . $do .'", {next: 0});    				
    				$(\''.$paginationpanel.'\').bootpag({
					    total: '.$total.',
					    page: 1,
					    maxVisible: 10,
					    href: "#ss-{{number}}", 	
					    leaps: true,
					    next: \'next\',
					    prev: \'prev\'
				    }).on(\'page\', function(event, num){
				    	var filter_meta_code = $(\'#filter_meta_code\').val();
					    $("'.$contentpanel.'").hide().load("'. AJAX_PATH . 'pagination/' . $do .'", {next:(num - 1) * '. MAX_ITEMS_PER_PAGE .'}).fadeIn(200); 
					});
				</script>';
	}
	
	public function facebook_share($data){		
		$summary = urlencode ( $data ['summary'] );
		$url = urlencode ( $data ['url'] );
		$image = urlencode ( $data ['image'] );
		$title = urlencode ( $data ['title'] );
		
		$ch = curl_init('http://graph.facebook.com/'.'http://riyasakwala.com/257/dpa-00257.html');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
			$like_results = curl_exec($ch);

		$like_array = json_decode ( $like_results, true );
		$like_count = (int)$like_array ['shares'];
		
		$link = "http://www.facebook.com/sharer.php?s=100&p[title]=$title&p[summary]=$summary&p[url]=$url&p[images][0]=$image";
		return $result = array('link'=> $link, 'count'=> ($like_count>0) ? $like_count : 0);
	}
	
	public function ajaxStatus(){
		return '<script type="text/javascript" language="javascript">
				$(document).ready(function() {
							$.ajaxSetup({
								beforeSend:function(){
									$(\'#sys_message\').show().html(\'Loadin Please Wait...\');
								},
								complete:function(){
									$(\'#sys_message\').fadeOut(500);
								}
							});
				});
				</script>';
	}
	
	public function twbsPagination($data){
		$contentpanel = $data['content'];
		$paginationpanel = $data['pagination'];
		$total = ceil($data['total'] / MAX_ITEMS_PER_PAGE_SITE);
		$do 	= $data['do'];
		return '
			<script type="text/javascript" language="javascript">
				$("'.$contentpanel.'").load("'. AJAX_PATH . 'pagination/' . $do .'", {next: 0});
				$(\''.$paginationpanel.'\').twbsPagination({
					totalPages: '.$total.',
					visiblePages: 10,
					onPageClick: function (event, num) {
						$("'.$contentpanel.'").hide().load("'. AJAX_PATH . 'pagination/' . $do .'", {next:(num - 1) * '. MAX_ITEMS_PER_PAGE_SITE .'}).fadeIn(200);
					}
				});
			</script>';
	}
}
?>