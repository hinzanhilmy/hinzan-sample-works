<?php
class FW{
	public $request;

	public function __construct($class){
		foreach($class as $object=>$c){
			$classname = $c;
			if(is_file(SYSTEM_CLASS_PATH . $c . '.php')){
				include_once (SYSTEM_CLASS_PATH . $c . '.php');
				if(FW_REPORTS){
					echo $this->message(TRUE, "MANUAL loaded  ". SYSTEM_CLASS_PATH . $classname . '.php' ." class");
				}
			}  else {
				echo $this->message(FALSE, "cannot call $classname class");
			}
		}
		return;
	}
	
	public function __call($class,$args=array()){
		$classname = $class;
		if(is_file(SYSTEM_CLASS_PATH . $class . '.php')){
			include_once (SYSTEM_CLASS_PATH . $class . '.php');
				$class = new $class($args);
			if(FW_REPORTS){
				echo $this->message(TRUE, "MANUAL loaded  ". SYSTEM_CLASS_PATH . $classname . '.php' ." class");
			}
		}  else {
				echo $this->message(FALSE, "cannot call $classname class");
		}
		return $class;
	}
	
	public function message($type=TRUE,$message="",$buyPage=FALSE){
		if($type == TRUE){
			return "<div class='block-success'>$message</div>";
		}else if ($buyPage == TRUE){
		    return "<div class='block-error' style='margin:5px 0px 20px 0px'>$message</div>"; // Hilmy 25/11/2015		
		}
		else {
			return "<div class='block-error'>$message</div>";
		}
	}
	
	public function row_message($type=TRUE,$message=""){
		if($type == TRUE){
			return '<div class="alert alert-success">'.$message.'</div>';
		} else {
			return '<div class="alert alert-danger">'.$message.'</div>';
		}
	}

	public function set_session_message($data){
		$type = $data['type'];		
		$text = $data['text'];
		if($text != ""){		
			switch($type){
				case TRUE: $_SESSION['session_message'] = '<div class="alert alert-success">
																		'.$text.'
														   </div>'; 
				break;
				case FALSE:
					$_SESSION['session_message'] = '<div class="alert alert-danger">
														'.$text.'
													</div>';
				break;
			}
		}
	}
	
	public function show_session_message(){
		if($_SESSION['session_message']!=""){
			return $_SESSION['session_message'];
		} else {
			return FALSE;
		}
	}
	
	public function session_message_clear(){
		$_SESSION['session_message'] = "";
		unset($_SESSION['session_message']);
	}
	
	public function set_form_data($data=array()){
		global $fw;
		$result = array();
		foreach($data as $key=>$d){
			if(is_array($d)){
				foreach($d as $k=>$a){
					$result[$k] = $fw->xss()->safe($a);
				}
			} else {
				$result[$key] = $fw->xss()->safe($d); 
			}	
		}
		$this->request = $result;
	}

	public function wbt_message($type,$message){
		if($type){
			return '<div class="row">
			    		<section class="dashboard-search clearfix" style="margin:0px 35px 0px 35px;">
			    			<div class="col-lg-12 col-md-9 col-sm-9 col-xs-12">
								<div class="alert alert-success">'.$message.'</div>
							</div>
						</section>
					</div>';
		} else {
			return '<div class="row">
			    		<section class="dashboard-search clearfix" style="margin:0px 35px 0px 35px;">
			    			<div class="col-lg-12 col-md-9 col-sm-9 col-xs-12">
								<div class="alert alert-error">'.$message.'</div>
							</div>
						</section>
					</div>';
		}
	}

	public function safe($val){
		htmlentities( $val, ENT_QUOTES, 'utf-8' );
		return $val;
	}

}
?>