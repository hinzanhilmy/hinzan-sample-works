<?php
class catagories extends DB{

	public function __construct(){
		parent::__construct();
	}
	
	public function put($data){
		$catid = ($data['id']=="")? 0: strip_tags($data['id']);
		$name = strip_tags($data['name']);
		$sql = "INSERT INTO `catagories` (`label`,`parent`) VALUES ('$name','$catid');";
		parent::query($sql);
		
		$catid = parent::getLastId();
		if($catid!=0){
			$data = array_merge($data,array('id'=>$catid));		
			$this->product_image_add($data);
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
	public function  product_image_add($data){
		$id = $data['id'];
		
		if(sizeof($data['images']) > 0){
			$sql = "DELETE FROM `catagory_images` WHERE `meta_cat_id` = '$id';";
			parent::query($sql);
		}
		
		foreach ($data['images'] as $key=>$m){
			$image = $m;
			copy(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_THUMB.$image, UPLOAD_ITEM_FULL_PATH. IMAGE_SIZE_THUMB.$image);
			copy(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_LARGE.$image, UPLOAD_ITEM_FULL_PATH. IMAGE_SIZE_LARGE.$image);
	
			unlink(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_THUMB.$image);
			unlink(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_LARGE.$image);

			$sql = "INSERT INTO `catagory_images` (`meta_cat_id`,`file`) VALUES  ('$id','$image');";
			parent::query($sql);
		}
		return TRUE;
	}
	
	public function deleteSelectedImages($data,$id){
		foreach($data as $d){
			list($imageId, $file) = explode("_", $d);
			$sql = "DELETE FROM `catagory_images` WHERE `id`='$imageId' AND `meta_cat_id` = '$id';";
			parent::query($sql);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_THUMB. $file);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_LARGE. $file);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_EXTRALARGE. $file);
		}
		parent::query($sql);
		return TRUE;
	}
	
	public function resetDefaultImage($productId){
		$sql = "UPDATE `catagory_images` SET `default`='0' WHERE `meta_cat_id` = '$productId';";
		@parent::query($sql);
		return TRUE;
	}
	
	public function getProductImagesByProductId($id){
		$sql = "SELECT * FROM `catagory_images` WHERE `meta_cat_id` = '$id';";
		return parent::query($sql);
	}
	
	public function edit($data){
		$id = $data['id'];
		$name = $data['name'];
		$action = $data['action'];
		$maincatid = $data['maincatid'];		
		$this->product_image_add($data);
		$this->deleteSelectedImages($data);
		
		if($action!='deletename'){
			if($maincatid == ""){
				$sql = "UPDATE `catagories` SET `label` = '$name'  WHERE `id`='$id';";
			} else {
				$sql = "UPDATE `catagories` SET `label` = '$name' , parent = '$maincatid' WHERE `id`='$id';";
			}
				
		} else {
			$sql = "DELETE  FROM `catagories` WHERE `id`='$id';";
		}
		parent::query2($sql);
		return TRUE;
	}
	
	public function search($data){
		global $fw;
		$cat = $data['cat'];
		$name = $fw->xss()->safe($data['search_text']);		
		$sql = "SELECT * FROM `catagories` WHERE `label` LIKE '%$name%' AND `parent` = $cat;";
		return parent::query($sql);
	}
	
	public function edit_sort($data){
		$sort = $data['sort'];
		foreach($sort as $k=>$s){
			$sql = "UPDATE `catagories`
						SET `sort` = '$s'
					WHERE `id` = '$k';";
			parent::query($sql);
		}
		return TRUE;
	}
	
	public function pagination($next,$id=0){
		if($id == 0){
			$sql = "SELECT * FROM `catagories` LIMIT $next," . MAX_ITEMS_PER_PAGE . ";";
		} else {
			$sql = "SELECT * FROM `catagories` WHERE `id`='$id';";
		}
		return parent::query($sql);
	}
	
	public function total(){
		$sql = "SELECT * FROM `catagories`;";
		parent::query($sql);
		return parent::getAffectedRows();
	}
}
?>