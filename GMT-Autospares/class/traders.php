<?php
class traders extends dbj{
	public function __construct(){
		parent::__construct();
	}
	
	public function get($data){	
		if($_SESSION['SEARCH']['name']!=""){
			$name = $_SESSION['SEARCH']['name'];
		} else {
			$name = strip_tags($data['name']);
		}
		
		$model = $_SESSION['SEARCH']['model'];
		$cat = $_SESSION['SEARCH']['cat'];
		
		$type = implode("','", $type);
		$next = (int)$_REQUEST['next'];
		
		$sql = "SELECT * FROM `web_data` WHERE (`Name` LIKE '%$name%' OR `ModelNo` LIKE '%$name%') AND Group2Code = '$model' AND `Group1Code` = '$cat'   LIMIT $next," . FRONT_MAX_ITEMS_PER_PAGE . ";";
		return parent::query($sql);
	}
	
	public function count($data){
		if($_SESSION['SEARCH']['name']!=""){
			$name = $_SESSION['SEARCH']['name'];
		} else {
			$name = strip_tags($data['name']);
		}
		
		$model = $_SESSION['SEARCH']['model'];
		$cat = $_SESSION['SEARCH']['cat'];
		
		$type = implode("','", $type);
		$next = (int)$_REQUEST['next'];
		
		$sql = "SELECT * FROM `web_data` WHERE `Name` LIKE '%$name%' AND Group2Code = '$model' AND `Group1Code` = '$cat';";
		parent::query($sql);
		return parent::getRowCount();
	}
	
	public function get_catagory($data){
		
		if($_SESSION['SEARCH']['name']!=""){
			$name = $_SESSION['SEARCH']['name'];
		} else {
			$name = strip_tags($data['name']);
		}
		
		if($_SESSION['SEARCH']['cat']!=""){
			$code = $_SESSION['SEARCH']['cat'];
		} else {
			$code = strip_tags($data['cat']);
		}
		$type = implode("','", $type);
		$next = (int)$_REQUEST['next'];
			
		$sql = "SELECT *  FROM `web_data` WHERE (`Group2Name` LIKE '%$name%' OR `Group2Name` LIKE '%$name%') AND `Group1Code` = '$code' GROUP BY `Group2Name` ORDER BY `Group2Name` ASC LIMIT $next," . FRONT_MAX_ITEMS_PER_PAGE . ";";
		return parent::query($sql);
	}
	
	public function cat_count(){
		if($_SESSION['SEARCH']['name']!=""){
			$name = $_SESSION['SEARCH']['name'];
		} else {
			$name = strip_tags($data['name']);
		}
		
		if($_SESSION['SEARCH']['cat']!=""){
			$code = $_SESSION['SEARCH']['cat'];
		} else {
			$code = strip_tags($data['cat']);
		}
		$type = implode("','", $type);
		$next = (int)$_REQUEST['next'];
			
		$sql = "SELECT `Group2Name` as `name` , count(*) as `count` FROM `web_data` WHERE (`Group2Name` LIKE '%$name%' OR `Group2Name` LIKE '%$name%') AND `Group1Code` = '$code' GROUP BY `Group2Name`;";
		parent::query($sql);
		return parent::getRowCount();
	}
	
	public function total(){
		$sql = "SELECT * FROM `web_data`;";
		parent::query($sql);
		return parent::getRowCount();
	}
	
	public function search($data){
		$next = (int)$_REQUEST['next'];
		if($_SESSION['SEARCH']['search_spare_parts']!=""){
			$search = $_SESSION['SEARCH']['search_spare_parts'];
		} else {
			$search = strip_tags($data['search_spare_parts']);
		}
		$sql = "SELECT * FROM `web_data` WHERE (`ProductID` LIKE '%$search%') ORDER BY `Group2Name` ASC LIMIT $next," . FRONT_MAX_ITEMS_PER_PAGE . ";";
		return parent::query($sql);
	}
	
	public function search_total($data){
		$next = (int)$_REQUEST['next'];
		if($_SESSION['SEARCH']['search_spare_parts']!=""){
			$search = $_SESSION['SEARCH']['search_spare_parts'];
		} else {
			$search = strip_tags($data['search_spare_parts']);
		}
		$sql = "SELECT * FROM `web_data` WHERE (`ProductID` LIKE '%$search%') ORDER BY `Group2Name` ASC;";
		parent::query($sql);
		return parent::getRowCount();
	}
}
?>