<?php
class audit extends db{
	public function __construc(){
		parent::__construct();
	}
	
	public function pagination($next){
		$sql = "SELECT users.id as userid, users.fullname, users.company, users.email,   count(`users_log_products`.`id`) as count , `users_log_products`.`date`, (select order_id.date from order_id where order_id.user_id = `users_log_products`.`users_id` ORDER BY `ID` DESC LIMIT 1) as `order_date`,
						(select order_id.id from order_id where order_id.user_id = `users_log_products`.`users_id` ORDER BY `ID` DESC LIMIT 1) as `order_id`
						from `users_log_products`
						JOIN users ON users.id = `users_log_products`.`users_id`
						GROUP BY `users_log_products`.`users_id` ORDER BY `users_log_products`.`date` DESC	LIMIT $next, ". MAX_ITEMS_PER_PAGE .";";
		return parent::query($sql);
	}
	
	public function no_of_logins($id){
		$sql = "SELECT * FROM `users_log` WHERE `users_id`='$id' ORDER BY `date_time` DESC;";
		return parent::query($sql);
	}
	
	public function total(){
		$sql = "SELECT * FROM `users_log_products` GROUP BY `users_id`;";
		parent::query($sql);
		return parent::getRowCount();
	}
	
	public function get_row($id){
		$sql = "SELECT * FROM `users_log_products` WHERE `users_id` = '$id' ORDER BY `id` DESC;";
		return parent::query($sql);
	}
}
?>