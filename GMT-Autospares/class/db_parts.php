<?php
class db_parts
{
	public $cn;
	private  $rc;
	public function __construct(){
			$this->cn = mysqli_connect(HOST, USER, PASS, DB_PARTS);
			if(mysqli_connect_errno()){
				echo $this->messageSystem('MySqli', FALSE, mysqli_connect_error());
			}
	}

	public function query($sql){	
		$result = array();
		$statement = explode(' ',strtoupper($sql));
		switch ($statement[0]){			
			case 'SELECT':
				$this->rc = $this->cn->query($sql);
				
				while($rw = $this->rc->fetch_array()){
						$result[] =$rw;
				}
				return $result;
				break;
			default:
				$this->rc = $this->cn->query($sql);
				return TRUE;
				break;
		}
		@mysqli_close($this->cn);
	}
	
	public function query_beta($sql)
	{
		$result = array();
		$statement = explode(' ',strtoupper($sql));
		switch ($statement[0]){
			case 'SELECT':
				if(is_resource($this->cn)) {
					$this->rc = mysql_query($sql,$this->cn)or die(mysql_error());
					if($this->getRowCount() > 1){
						while($rw = mysql_fetch_array($this->rc)){
							$result[] =$rw;
						}
					} else {
						return mysql_fetch_array($this->rc);
					}
					return $result;
				} else {
					return FALSE;
				}
				break;
			default:
				$this->rc = mysql_query($sql,$this->cn);
				return TRUE;
				break;
		}
	}
	
	public function query_json($sql)
	{	$result = array();
		$statement = explode(' ',strtoupper($sql));
		switch ($statement[0]){			
			case 'SELECT':
					$this->rc = $this->cn->query($sql) or die(mysqli_error($db));;
					while($rw = $this->rc->fetch_assoc()){
						$result[] =$rw;
					}
					return $result;
				break;
			default:
				$this->rc = mysql_query($sql,$this->cn);
				return TRUE;
				break;
		}
		@mysqli_close($this->cn);
	}
	
	public function query2($sql){
		$result = array();
		$statement = explode(' ',strtoupper($sql));
		switch ($statement[0]){			
			case 'SELECT':
					$this->rc = $this->cn->query($sql) or die(mysqli_error($db));;
					while($rw = $this->rc->fetch_array()){
						$result[] = $rw;
					}
					if(sizeof($result)>1){
						return $result;
					} else {
						return $result[0];
					}
			default:
				$this->rc = $this->cn->query($sql); break;
				return TRUE;
		}
		@mysqli_close($this->cn);
	}
	
	public function getAffectedRows(){
		return $this->cn->affected_rows;
	}
	
	public function getLastId(){
		return $this->cn->insert_id;
	}
	
	public function getRowCount(){
		return $this->rc->num_rows;
	}
	
	public function status(){
		if($this->cn->affected_rows > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function getFileds($table,$filedType='meta'){
		$result = array();		
		$sql = "SELECT * FROM " . $table;
		$rc = mysql_query($sql,$this->cn)or die(mysql_error());		
		$count = 0;
		while($count < mysql_num_fields($rc)){
			$meta = mysql_fetch_field($rc,$count);
			if($meta->primary_key != 1)
				$filed = explode('_',$meta->name);
				if($filed[0] == $filedType){
						$label = ucwords(str_replace(array($filedType."_", "_"), " ",$meta->name));
				  	 	$result[] = array('field'=>$meta->name, 'label'=>$label);
				  }			   			
			$count++;
		}
		return $result;
	}
	
	public function label($name){
		return str_replace('_' , ' ',$name);
	}
	
	public function genarateInsertStatement($data=array(),$table,$otherfileds=array()){
			$otherFildNames = '';
			$otherFildValues = '';		
			for($x=0; $x!=sizeof($otherfileds); $x++){				
				foreach ($otherfileds[$x] as $key=>$value) {
					$otherFildNames .= "`$key`,";
					$otherFildValues .= "'$value',";				
				}
			}
			$sql = "INSERT INTO $table (";
				if($otherFildNames!=""){
					$sql .=$otherFildNames;
				}
				foreach($data as  $key => $filed){
						stristr($key,'ui_') ? $sql .="`$key`," : null;
				}
			$sql = trim($sql,',');
			$sql .=") VALUES(";
				if($otherFildValues!=''){
					$sql .= $otherFildValues;
				}

				foreach($data as  $key => $filed){
						stristr($key,'ui_') ? $sql .="'$filed'," : null;

				}

			$sql = trim($sql,',');		
			$sql .=");";
		return $sql;
	}	

	public function dataSoruce($table){
		$result = array();
		if($table=='countries'){
			$sql = "SELECT `id` as `id` , `printable_name` as `name` FROM `$table` ORDER BY `id` DESC;";
		} else {
			$sql = "SELECT `id` as `id` , `name` as `name` FROM `$table` ORDER BY `id` DESC;";
		}
		
		foreach($this->query($sql) as $row){
			$result[] = $row;	
		}
		return $result;		
	}
	
	public function html_FormFiled($name,$type,$style='',$value='',$datasoruce=array(),$onChange='',$dbfiled='ui'){
		switch ($type){
			case 'submit':
				$html ='<input type="submit" name="'. $name . '" value="'. $value .'" class="'. $style .'"/>';
				break;
			case 'text':
				$html ='<input type="text" name="'. $dbfiled . '_'. $type . '_' . $name . '" value="'. $value .'" class="'. $style .'"/>';
				break;
			case 'select':
				if($onChange==""){
					$html ="<select name='". $dbfiled ."_" . $type . '_' ."$name' class='$style'>";
				} else {
					$html ="<select name='". $dbfiled ."_" . $type . '_' ."$name' class='$style' onchange='$onChange'>";
				}
				
					$html .='<option>select</option>';
					if(sizeof($datasoruce)==0) {						
							$datasoruce = $this->dataSoruce($name);
							foreach($datasoruce as $option){
								$html .= "<option value='".$option['id']."'>" . $option['name'] . "</option>";
							}
					} elseif(sizeof($datasoruce) > 0) {
							foreach($datasoruce as $value){
								$html .= "<option value='".$value['id']."'>" . $value['name'] . "</option>";
							}
					}
					
				$html .="</select>";
				break;
			case 'list':
				break;
			case 'password':
				$html ='<input type="password" name="'. $dbfiled . '_' . $type . '_' . $name . '" value="'. $value .'" class="'. $style .'"/>';
				break;
			case 'textarea':
				$html ='<textarea name="'. $dbfiled . '_'. $type . '_' . $name . '" class="'. $style .'"></textarea>';
				break;
			case 'session':
				$html ='<input type="hidden" name="'. 'session_'. $type . '_' . $name . '" value="'. $_SESSION['USERID'] .'" class="'. $style .'"/>';
				break;
			case 'checkbox':
					$html = '<input type="checkbox" name="' . $name .'" value="'. $value . '">';				
				break;
			case 'radio':
				if(sizeof($datasoruce)==0) {
						$html = '<input type="radio" name="' . $name .'" value="'. $value . '">';
				} else {
					foreach($datasoruce as $key=>$value){
						$html .= '<input type="radio" name="' . $name .'" value="'. $key . '">' . $value['name'] . '<br />';
					}
				}
				break;			
			default:
				$html='undefined type';
				break;
					
		}
		return $html;
	}
	
	public function messageBox($head,$MessageBoxType,$bodyText){
		$msgBox = '<table class="formBox" width="100%" border="0" cellspacing="1" cellpadding="1">
				  <tr>
					<th colspan="2" class="agentLogin">' . $head .'</th>
				  </tr>
				  <tr>
					<td width="6%" align="center"> + </td>
					<td>'. $bodyText .'</td>
					</tr>
			</table>
			<br />';
		return $msgBox;
	}
	public function messageSystem($head,$type,$bodyText){
		$msgBox = '<table style="border:1px solid red;" width="100%" border="0" cellspacing="1" cellpadding="1">
				  <tr>
					<th colspan="2" class="agentLogin" style="background:red;"><font color="white">' . $head .'</font></th>
				  </tr>
				  <tr>
					<td width="6%" align="center"></td>
					<td><li><font size="4">'. $bodyText .'</font></li></td>
					</tr>
			</table>';
		return $msgBox;
	}

	public function newmessage($text,$type=TRUE){
		switch($type){
			case TRUE: 		return '<div id="message_show">' . $text .'</div>';	break;
			case FALSE: 	return '<div id="message_error">' . $text .'</div>';	break;
		}
	}
	
	public function flush_message($text,$type=TRUE){
		switch($type){
			case TRUE: 		return '<div style="background:#FFCCCC !important; color:#333333; z-index:9999; font-size:12px; border:1px solid #FF8282; padding:10px; font-weight: bold; text-align: center; width:100%;">' . $text .'</div>';	break;
			case FALSE: 	return '<div style="!important; color:red; z-index:9999; font-size:12px; padding:10px; font-weight: bold; text-align: center; width:100%;">' . $text .'</div>';	break;
		}
	}
	
	public function jQueryMessage($message='',$type=TRUE){
		switch($type){
			case TRUE: return '<script language="javascript" type="text/javascript">
							$(\'.sys_message\').html(\''.$message .'\').removeClass().addClass(\'success\').slideDown(1000).delay(1000).slideUp(1000);
						</script>'; 
			break;
			case FALSE: return '<script language="javascript" type="text/javascript">
							$(\'.sys_message\').removeClass().addClass(\'information\').html(\''.$message .'\').slideDown(10).delay(1000).slideUp(1000);
						</script>'; 
			break;
		
		}	
	}
}
?>
