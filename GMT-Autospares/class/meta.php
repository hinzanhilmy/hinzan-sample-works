<?php
class meta extends DB{
	
	public function __construc(){
		parent::__construct();
	}		
	
	public function add($data){
		global $fw;
		$name = $fw->xss()->safe($data['name']);
		$code = trim(($data['code']=="OTHER")? $data['newcode'] : $data['code']);
		$title = $fw->xss()->safe($data['title']);
		$sql = "INSERT INTO `meta` (`name`,`code`, `title`) VALUES ('$name','$code', '$title');";
		parent::query($sql);
		
		if(parent::status()){
			if($data['code']=="OTHER"){
				$column = "meta_" . str_replace(" ", "_", strtolower($data['newcode']));		
				$sql = "ALTER TABLE `products` ADD COLUMN `$column` BIGINT(10) NOT NULL COMMENT 'auto gen for meta table code' AFTER `timestamp`;";
				@parent::query($sql);
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function getCode($code=""){
		$code = trim($code);
		if($code == ""){
			$sql = "SELECT DISTINCT `code` FROM `meta`  ORDER BY `code` ASC;";
		} else {
			$sql = "SELECT `id`, `code` FROM `meta` WHERE `code` = '$code';";
		}
		return parent::query($sql);
	}
	
	public function getByCode($code=""){
		$code = trim($code);
		$sql = "SELECT `id`, `name`, `title` FROM `meta` WHERE `code` = '$code' ORDER BY `name` DESC;";
		return parent::query($sql);
	}
	
	public function get_features_by_productid($id){
		$sql = "SELECT (SELECT `name` FROM `meta` WHERE `meta`.`id` = `features`.`meta_id` LIMIT 1) as `class` , (SELECT `title` FROM `meta` WHERE `meta`.`id` = `features`.`meta_id` LIMIT 1) as `title`, `meta_id` FROM `features` WHERE `product_id` = '$id' ORDER BY `sort` ASC;";
		return parent::query($sql);
	}

	public function get_features_valuebyId($product_id, $id){
		$sql = "SELECT * FROM `features` WHERE `product_id` = '$product_id' and `meta_id` = '$id' LIMIT 1;";
		$row = parent::query2($sql);
		return $row['value'];
	}
	
	public function get($id){
		if($id == ""){
			$sql = "SELECT * FROM `meta`;";
		} else {
			$sql = "SELECT * FROM `meta` WHERE `id` = '$id';";
		}
		return parent::query($sql);
	}
	
	public function getValue($id){
		$sql = "SELECT * FROM `meta` WHERE `id` = '$id';";
		$row = parent::query($sql);
		return (object)$row[0];
	}
	
	public function pagination($next){
		$sql = "SELECT * FROM `meta` ORDER BY `code` ASC LIMIT $next," . MAX_ITEMS_PER_PAGE . ";";
		return parent::query($sql);
	}
	
	public function total(){
		$sql = "SELECT * FROM `meta`;";
		parent::query($sql);
		return parent::getAffectedRows();
	}
	
	public function edit($data){
		global $fw;
		$id = $data['id'];
		$name = $fw->xss()->safe($data['name']);
		$code = $fw->xss()->safe($data['code']);
		$title = $fw->xss()->safe($data['title']);
		
		$sql  = "UPDATE `meta` SET `name`= '$name', `code`='$code' , `title` = '$title'
					WHERE `id`='$id';";
		parent::query($sql);
		return TRUE;
	}
	
	public function deleteName($id){
		global $fw;
		$sql = "SELECT * FROM `meta` WHERE `id` = '$id';";
		$row = parent::query($sql);
		if(parent::getRowCount() == 1){
			//$column = "meta_" . str_replace(" ", "_", strtolower($row[0]['code']));
			$sql = "DELETE FROM `meta` WHERE `id` = '$id';";
			parent::query($sql);
		} else {
			$sql = "DELETE FROM `meta` WHERE `id` = '$id';";
			parent::query($sql);
		}
		if(parent::status()){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function deleteCode($id){
		global $fw;
		$sql = "SELECT * FROM `meta` WHERE `id` = '$id';";
		$row = parent::query($sql);
		$code = $row[0]['code'];
		$column = "meta_" . str_replace(" ", "_", strtolower($row[0]['code']));
		if($this->deletefield($column)){
			$sql = "DELETE FROM `meta` WHERE `code` = '$code';";
			parent::query($sql);
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function deletefield($column){
		global $compulsory_meta_fields;
		if(in_array($column, $compulsory_meta_fields)){
			return FALSE;
		} else {
			$sql = "ALTER TABLE `products` DROP COLUMN `$column`;";
			parent::query($sql);
			return TRUE;
		}
	}
	
	public function getMainCats(){
		$sql = "SELECT `label` as `name`, `id`  FROM `catagories` WHERE `parent` = '0' ORDER BY `label` ASC;";
		return parent::query($sql);
	}
	
	public function getCatsByParentId($id){
		$sql = "SELECT `label` as `name`, `id`  FROM `catagories` WHERE `parent` = '$id' ORDER BY `sort` ASC;";
		return parent::query($sql);
	}

	public function getCatImage($id){
		$sql = "SELECT (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` 
					FROM `product` WHERE `catagories_id` = '$id' ORDER BY `id` ASC LIMIT 1;";
		$row = parent::query2($sql);
		return $row['file'];
	}
	
	public function getCatNameById($id){
		$sql = "SELECT `label` as `name`, `id`  FROM `catagories` WHERE `id` = '$id';";
		return parent::query2($sql);
	}
	
	public function getDrowMultiCataogries($parent){
			$sql = "SELECT a.id, a.label, a.link, Deriv1.Count , a.parent
									 FROM `catagories` a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM `catagories` GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.parent='$parent';";
			$result = parent::query($sql);
									
			echo '<ul id="nav" class="dropdown dropdown-vertical">';
			foreach($result as $row) {
				if ($row['Count'] > 0) {
					echo "<li><a class='dir' href='/home/itemlist/" . $row['id'] . "/". strtolower($row['label']) .".html'>" . $row['label'] . "</a>";
						$this->getDrowMultiCataogries($row['id']);
					echo "</li>"; 
				} elseif ($row['Count']==0) {
					echo "<li><a href='/home/itemlist/" . $row['id'] ."/". strtolower($row['label']) .".html'>" . $row['label'] . "</a></li>";
				}
				
			}
			echo '</ul>';
	}
	
	public function getDrowMultiCataogriesForSelect($parent,$level,$selected=""){
			$sql = "SELECT a.id, a.label, a.link, Deriv1.Count , a.parent
									 FROM `catagories` a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM `catagories` GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.parent='$parent';";
			$result = parent::query($sql);
			foreach($result as $row) {
				echo $strSelected = ($selected==$row['id'])? 'selected="selected"' : '';
				
				if ($row['Count'] > 0) {
					echo "<option style='font-weight:bold !important;' value='".$row['id']."'".$strSelected.">". str_repeat('&nbsp;', $level) .$row['label'] . "</option>";
					$this->getDrowMultiCataogriesForSelect($row['id'], $level+1, $selected);
				} elseif ($row['Count']==0) {
					echo "<option value='".$row['id']."'".$strSelected.">". str_repeat('&nbsp;', $level) . $row['label'] . "</option>";
				}
			}
	}
	
	public function getParentByArray($id){
		static $r = array();
		$sql = "SELECT a.id, a.label, a.link, Deriv1.Count , a.parent
					FROM `catagories` a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM `catagories` GROUP BY parent) Deriv1 ON a.id = Deriv1.parent 
				WHERE a.parent='$id';";
		$result = parent::query($sql);
		foreach($result as $row) {
			if ($row['Count'] > 0) {
				$r[] = $row['id'];
				$this->getParentByArray($row['id']);
			} elseif ($row['Count']==0) {
				$r[] = $row['id'];
			}
		}
		return $r;
	}
	
	public function crumb($id){
		static $res = array();
		$sql = "SELECT a.id, a.label, a.link, a.parent FROM `catagories` a  WHERE a.id='$id'";
		$result = parent::query($sql);
		foreach($result as $row) {
			
			if ($this->hasChild($row['parent'])) {
				$res[] = array('label'=>$row['label'], 'id'=> $row['id']);
					$this->crumb($row['parent']);
			} else {
				$res[] = array('label'=>$row['label'], 'id'=>$row['id']);
			}
			
		} 
		return array_reverse($res);
	}
	
	public function catagories_relations_ids($id){
		$return = "";
		$result = $this->crumb($id);
		foreach($result as $r){
			$return .= $r['id'] . ',';
		}
		return trim($return,",");
	}
	
	public function hasChild($parent){
		$sql = "SELECT a.id, a.label, a.link, a.parent FROM `catagories` a  WHERE a.parent='$parent'";
		$row = parent::query($sql);
		if(parent::getRowCount() > 0){ return TRUE;} else { return FALSE;}
	}
	
	public function build_query($data){
		return http_build_query(array(	'catagory'=>	$data['catagory'],
										'search_text'=>	$data['search_text'],
										'year'=>	$data['year'],
										'make'=>	$data['make'],
										'model'=>	$data['model']));
	}
	
	public function build_back_position($data){
		$param = http_build_query(array('catagory'=>$data['catagory'],
				'search_text'=>$data['search_text'],
				'year'=>$data['year'],
				'make'=>$data['make'],
				'model'=>$data['model'],
				'id'=>$data['id']));
		if(strip_tags($data['from']) == "search"){
			return HTTP_PATH . 'search.html?' . $param;
		} else if(strip_tags($data['from'])=="catagory") {
			return HTTP_PATH . 'catagory/' . $data['catagory'] . '/';
		} else {
			return FALSE;
		}
	}
	
	public function metaTag($data){
		global $fw;
		$result = array();
		$page = $data['p'];
		$id = $data['id'];
		switch($page){
			default:	
					$result = array('title'=> 'DPA Riyasakwala - Online Vehicle Sale In Sri Lanka',
									'description'=> 'The Best Place to buy sell or exchange vehicles.',
									'keywords'=> 'price, sri lanka, David Pieris Motor Company Limited, Sri Lanka.');
					break;
			case 'itemdetails':
					$item_details = $fw->vehicles($id)->get();
					$result = array('title'=>  $item_details['title']. ' - ' . $item_details['referance_id'] . ' - DPMC Riyasakwala',
									'keywords'=> $item_details['keywords'] . ',price, sri lanka, David Pieris Motor Company Limited, Sri Lanka',
									'description'=> 'David Pieris Motor Company Limited. The Best Place to buy sell or exchange vehicles.');
					break;
			case 'search': 
					$result = array('title'=> 'Search - DPA  Riyasakwala',
									'description'=> 'The Best Place to buyll or exchange vehicles.',
									'keywords'=> 'price, sri lanka, David Pieris Motor Company Limited, Sri Lanka.');
					break;		
		}
		return (object)$result;
	}
	
	public function verifyImage(){
		$imgX = 100;
		$imgY = 28;
		$image = imagecreatetruecolor(100, 30);
		
		$backgr_col = imagecolorallocate($image, 255,254,193);
		$border_col = imagecolorallocate($image, 208,208,208);
		$text_col = imagecolorallocate($image, 198,11,11);
		
		imagefilledrectangle($image, 0, 0, 100, 30, $backgr_col);
		imagerectangle($image, 0, 0, 99, 29, $border_col);
		
		$font = "VeraSe.ttf"; // it's a Bitstream font check <a rel="nofollow" href="http://www.gnome.org" rel="nofollow">http://www.gnome.org</a> for more
		$font_size = 11;
		$angle = 0;
		$box = imagettfbbox($font_size, $angle, $font, $_SESSION['RANDOM_IMAGE_VALUE']);
		$x = (int)($imgX - $box[4]) / 2;
		$y = (int)($imgY - $box[5]) / 2;
		imagettftext($image, $font_size, $angle, $x, $y, $text_col, $font, $_SESSION['RANDOM_IMAGE_VALUE']);
		header("Content-type: image/png");
		imagepng($image);
		imagedestroy ($image);
	}


	public function search_getPostal($search){		
		$search = $search['search'];
		$status = $_REQUEST;
		if($status['postal']== "true"){
			$sql = "SELECT * FROM `postcodes_geo` WHERE `postcode` LIKE '%$search%' GROUP BY `postcode` ORDER BY `postcode` ASC  LIMIT 20;";
		}		
		if($status['state']== "true"){
			$sql = "SELECT * FROM `postcodes_geo` WHERE `state` LIKE '%$search%' GROUP BY `state` ORDER BY `state` ASC LIMIT 20;";
		}
		if($status['suburb']== "true"){
			$sql = "SELECT * FROM `postcodes_geo` WHERE `suburb` LIKE '%$search%' GROUP BY `suburb` ORDER BY `suburb` ASC LIMIT 20;";
		}
		$row = parent::query($sql);
		$data = array();
		if(parent::getRowCount() > 0){
			foreach($row as $r){
				if($status['postal'] == 'true'){
					$data[] = $r['state'] . ' - ' . $r['postcode'];
				}
				if($status['state'] == 'true'){
					$data[] = $r['state'];
				}
				if($status['suburb'] == 'true'){
					$data[] = $r['suburb'];
				}
			}
		} else {
			unset($data);
			$data = array();
		}
		return $data;
	}
}
?>