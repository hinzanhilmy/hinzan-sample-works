<?php
include_once (SYSTEM_PATH.'3rdparty/phpmailer/class.phpmailer.php');

class mail extends PHPMailer{

	public function __construct(){
		parent::__construct();
	}
	
	public function register($data){
		$email = $data['email'];
		$fullname = $data['fullname'];
		$company = $data['compay'];
		$this->Subject    = 'GMT Registration';
		
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->SetFrom(MAIL_ADMIN, MAIL_ADMIN_NAME);
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->AddAddress($email, $fullname);
		$this->AddBCC('hinzan@gmail.com', MAIL_SYSTEM_NAME);
		
		$message = "Your account is going to review mode. <br />
					We will send the login details after account verification process is over.<br /><br />
					Thank you.";
		
		$body             = file_get_contents(SYSTEM_PATH . 'templates/mail_tamplates/genaral.php');
		$templates_vars	  = array('{name}','{message}','{html}');
		$template_replace_vars = array($fullname,$message,NULL);
		$body			  = str_replace($templates_vars,$template_replace_vars,$body);
		
		$this->AltBody    = "To view the message, please use an HTML compatible email viewer!";
		
		$this->MsgHTML($body);
		$this->Send($body);
		$this->ClearAddresses();		
		return TRUE;
	}
	
	public function feedback($data){
		$member = $data['member'];
		$suggestion_type = $data['suggestion_type'];
		$full_name = $data['full_name'];
		$email = $data['email'];
		$phone = $data['phone'];
		$description = $data['description'];
		
		$this->Subject    = 'GMT Feedback - ' . strtoupper($suggestion_type);
	
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->SetFrom(MAIL_ADMIN, MAIL_ADMIN_NAME);
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->AddAddress(MAIL_ADMIN, MAIL_ADMIN_NAME);
		$this->AddBCC('hinzan@gmail.com', MAIL_SYSTEM_NAME);
		$this->AddBCC('thiwankalk@gmail.com', MAIL_SYSTEM_NAME);
		
	
		$message = "
		<h3>Feedback</h3>
		<br /><br />
		
		<p>
			<b>Member Type</b>
			<p>". $member ."</p>
		</p>
		
		<p>
			<b>Suggestion Type</b>
			<p>". $suggestion_type ."</p>
		</p>
		
		<p>
			<b>Full Name</b>
			<p>". $full_name ."</p>
		</p>
		
		<p>
			<b>Email</b>
			<p>". $email ."</p>
		</p>
		
		<p>
			<b>Phone</b>
			<p>". $phone ."</p>
		</p>
		
		<p>
			<b>Message</b>
			<p>". nl2br($description) ."</p>
		</p>";
	
		$body             = file_get_contents(SYSTEM_PATH . 'templates/mail_tamplates/genaral.php');
		$templates_vars	  = array('{name}','{message}','{html}');
		$template_replace_vars = array($full_name,$message,NULL);
		$body			  = str_replace($templates_vars,$template_replace_vars,$body);
		
		$this->AltBody    = "To view the message, please use an HTML compatible email viewer!";
	
		$this->MsgHTML($body);
		$this->Send($body);
		$this->ClearAddresses();
		return TRUE;
	}
	
	public function verify($data){
		$accountrype = $data['accountrype'];
		if($accountrype=='USER' || $accountrype=='ADMIN'){
			$email = $data['email'];
			$fullname = $data['fullname'];
			$company = $data['compay'];
			$password = $data['password'];
		
			$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
			$this->SetFrom(MAIL_ADMIN, MAIL_ADMIN_NAME);
			$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
			$this->AddAddress($email, $fullname);
			$this->AddBCC('hinzan@gmail.com', MAIL_ADMIN_NAME);
			$this->AddBCC('thiwankalk@gmail.com', MAIL_ADMIN_NAME);
				
			
			$this->Subject    = 'GMT Account Verification';
			
			$message = "Your account is now activated. <br />
					 Please use this login credentials for login to the site<br /><br />
					 
					 ". HTTP_PATH ."login.html <br />
					 User Name : $email <br />
					 Password : $password <br />
					 
					 <br />";
			$body             = file_get_contents(SYSTEM_PATH . 'templates/mail_tamplates/genaral.php');
			$templates_vars	  = array('{name}','{message}','{html}');
			$template_replace_vars = array($fullname,$message,NULL);
			$body			  = str_replace($templates_vars,$template_replace_vars,$body);
			
			$this->AltBody    = "To view the message, please use an HTML compatible email viewer!";
						
			$this->MsgHTML($body);
			$this->Send($body);
			$this->ClearAddresses();
		}
		return TRUE;
	}
	
	public function send_order($data){
		if($data['email'] != ""){
			$email = $data['email'];
			$fullname = $data['fullname'];			
			$table = $data['table'];		
			$eway_transaction = $data['eway_transaction'];
			$message = "Please check your order.";
			$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
			$this->SetFrom(MAIL_ADMIN, MAIL_ADMIN_NAME);
			$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
			$this->AddAddress($email, $fullname);
			$this->AddBCC('hinzan@gmail.com', MAIL_ADMIN_NAME);
			$this->AddBCC('thiwankalk@gmail.com', MAIL_ADMIN_NAME);
			
			$this->Subject    = 'GMT Order';
			
			$body             = file_get_contents(SYSTEM_PATH . 'templates/mail_tamplates/send_customer_order.php');
			$templates_vars	  = array('{name}','{message}','{html}' , '{eway_transaction}');
			$template_replace_vars = array($fullname,$message,$table, $eway_transaction);
			$body			  = str_replace($templates_vars,$template_replace_vars,$body);
		
			$this->MsgHTML($body);
			$this->Send($body);
			$this->ClearAddresses();
		return TRUE;
	 } else {
	 	return FALSE;
	 }
	}
	
	public function re_order($data){
		if($data['email'] != ""){
			$email = $data['email'];
			$fullname = $data['fullname'];
			$message = $data['message'];
			$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
			$this->SetFrom(MAIL_ADMIN, MAIL_ADMIN_NAME);
			$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
			$this->AddAddress($email, $fullname);
			$this->AddBCC('hinzan@gmail.com', MAIL_ADMIN_NAME);
			$this->AddBCC('thiwankalk@gmail.com', MAIL_ADMIN_NAME);
				
			$this->Subject    = 'GMT Order';
				
			$body = "
			Dear $fullname,<br /><br />
			
			Please supply this item <br /><br />
				
			". $message ." <br /> <hr />
				
			<br />
			Url : http://gmt.yaalu.com.au/<br /> <br />
				
				
			Thank you.<br />
			GMT<br /><br /><br />
		
			www.gmt.com.au<br />
			Tel: 000000<br />
			Address <br />
			Find us: {http}";
		
			$this->MsgHTML($body);
			$this->Send($body);
			$this->ClearAddresses();
			return TRUE;
		} else {
		return FALSE;
		}
	}
	
	public function send_pdf_tovendor($data){
		global $fw;
		$pdf_name = $data['pdf_name'] . '.pdf';
		$subject = $data['subject'];
		$email = $data['email'];
		$name = $data['fullname'];
		$html_table = $data['message'];
		$this->Subject    = $subject;
	
		$body             = file_get_contents(SYSTEM_PATH . 'templates/mail_tamplates/vendor_order.php');
	
		$this->Subject    = $subject;
		
		$message = $html_table;
	
		$templates_vars	  = array('{name}','{message}','{html}');
		$template_replace_vars = array($name,$message,$html_table);
		$body			  = str_replace($templates_vars,$template_replace_vars,$body);
	
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->SetFrom(MAIL_ADMIN, MAIL_ADMIN_NAME);
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->AddAddress($email, $name);
		$this->AddBCC('hinzan@gmail.com', MAIL_ADMIN_NAME);
		$this->AddBCC('thiwankalk@gmail.com', MAIL_ADMIN_NAME);
			
	
		$this->AddAttachment(SYSTEM_COUPON_PATH.$pdf_name);
		$this->AltBody    = "To view the message, please use an HTML compatible email viewer!";
		$this->MsgHTML($body);
		$this->Send($body);
		$this->ClearAddresses();
	}
	
	public function send_pdf_customer($data){
		global $fw;
		$pdf_name = $data['pdf_name'] . '.pdf';
		$subject = $data['subject'];
		$email = $data['email'];
		$name = $data['fullname'];
		$html_table = $data['message'];
		$this->Subject    = $subject;
	
		$body             = file_get_contents(SYSTEM_PATH . 'templates/mail_tamplates/customer_invouice.php');
	
		$this->Subject    = $subject;
		
		$message = 'Please view your order.';
	
		$templates_vars	  = array('{name}','{message}','{html}');
		$template_replace_vars = array($name,$message,$html_table);
		$body			  = str_replace($templates_vars,$template_replace_vars,$body);
	
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->SetFrom(MAIL_ADMIN, MAIL_ADMIN_NAME);
		$this->AddReplyTo(MAIL_ADMIN,MAIL_ADMIN_NAME);
		$this->AddAddress($email, $name);
		$this->AddBCC('hinzan@gmail.com', MAIL_ADMIN_NAME);
		$this->AddBCC('thiwankalk@gmail.com', MAIL_ADMIN_NAME);
			
	
		$this->AddAttachment(SYSTEM_COUPON_PATH.$pdf_name);
		$this->AltBody    = "To view the message, please use an HTML compatible email viewer!";
		$this->MsgHTML($body);
		$this->Send($body);
		$this->ClearAddresses();
	}
}
?>