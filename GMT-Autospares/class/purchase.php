<?php
class purchase extends db {	
	public function __construct(){
		parent::__construct();
	}
	
	public function put($data){
		$id = $data['id'];
		$productid = $data['pid'];
		$qun = $data['qun'];
		$date = $data['date'];
		$price = $data['price'];
		$status = $data['status'];
		
		$sql = "UPDATE `purchase_orders`					 
					SET `recived_quantity` = '$qun',
						`recived_date` = '$date',
						`price` = '$price',
						`status` = '$status'	
				WHERE `id` = '$id';";
		parent::query($sql);
		
		if($status == "RECEIVED"){
			$sql = "UPDATE `product` SET `qoh`= `qoh` + $qun WHERE `id` = '$productid';";
			parent::query($sql);
		}		
		return parent::status();
	}
	
	public function history($data){
		$vendorid = $data['id'];
		$productid = $data['product'];
		$strWherevendor = "";
		$strWhereproduct = "";
		
		if($vendorid != 0){
			$strWherevendor = " AND `users_id` = '$vendorid'";
		}
		if($productid != 0){
			$strWhereproduct = " AND `product_id` = '$productid'";
		}
		$sql = "SELECT *				 
					FROM `purchase_orders` WHERE 1 $strWherevendor $strWhereproduct  ORDER BY `id` DESC;";
		return parent::query($sql);
	}
	
	public function stock_status(){
		$sql = "SELECT SUM(qun) AS `order_qun`,product.id, product.title, product.qoh, product.re_orderlevel  FROM product JOIN orders ON orders.product_id =  product.id
					WHERE orders.order_id in (SELECT id FROM order_id WHERE `status` = 'PENDING')
					GROUP BY product.id;";
		return parent::query($sql);
	}
	
	public function order_history($productid){
		$sql = "SELECT *, order_id.date as `order_date`, order_id.id as order_id FROM users JOIN order_id ON order_id.user_id = users.id 
					JOIN orders ON orders.order_id = order_id.id
					JOIN product ON product.id = orders.product_id
					WHERE order_id.status = 'PENDING' 
					AND product.id = '$productid'
					GROUP BY order_id.id
					ORDER BY order_id.id ASC;";
		return parent::query($sql);
	}
}
?>