<?php
class vehicles extends DB{
	private $id;
	public function __construct($data){
		$this->id = strip_tags($data[0]);
		parent::__construct();
	}
	
	public function get(){
		$sql = "SELECT *,
						(SELECT `file` FROM `product_images` WHERE `products_id`=`products`.`id` ORDER BY `default` DESC LIMIT 1) AS `image`,
						(SELECT `label` FROM `catagories` WHERE `id`=`products`.`catagories_id`) AS `cat_name`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_manufacture_year`) AS `manufacture_year`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_make`) AS `make`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_transmission`) AS `meta_type_of_car`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_condition`) AS `condition`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_location`) AS `location`
				FROM `products` WHERE `id` = '$this->id';";
		return parent::query2($sql);
	}
	
	public function getByRelatedCatagory($id,$next=0){
		$id = strip_tags($id);
		$next = ($next !=0)? strip_tags($next): 0;
		$sql = "SELECT *,
						(SELECT `file` FROM `product_images` WHERE `products_id`=`products`.`id` ORDER BY `default` DESC LIMIT 1) AS `image`,
						(SELECT `label` FROM `catagories` WHERE `id`=`products`.`catagories_id`) AS `cat_name`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_manufacture_year`) AS `manufacture_year`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_make`) AS `make`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_transmission`) AS `meta_type_of_car`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_condition`) AS `condition`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_location`) AS `location`
				FROM `products` WHERE `catagories_relations_id` LIKE '%$id%'
						 ORDER BY `id`  DESC LIMIT $next, ". MAX_ITEMS_PER_PAGE_SITE .";";
		$result = parent::query($sql);
		if(parent::getRowCount() > 0){
			return $result;
		} else {
			return FALSE;
		}
		
	}
	
	public function getModelsByMakes($id){
		$result = array();
		$id = strip_tags($id);
		if($id !=""){
			$sql = "SELECT `id` , `model` AS `name`
						FROM `products` WHERE `meta_make` = '$id' GROUP BY `model` ORDER BY `model` ASC;";
			$row = parent::query($sql);
			foreach($row as $r){
				$result[] = array('name'=>$r['name'],'id'=>$r['name']);
			}
			return json_encode($result);
		} else {
			return FALSE;
		}
	}
	
	public function most_recent_vehicles($next){
		$sql = "SELECT *,
						(SELECT `file` FROM `product_images` WHERE `products_id`=`products`.`id` ORDER BY `default` DESC LIMIT 1) AS `image`,
						(SELECT `label` FROM `catagories` WHERE `id`=`products`.`catagories_id`) AS `cat_name`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_manufacture_year`) AS `manufacture_year`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_make`) AS `make`,
                                                (SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_transmission`) AS `meta_type_of_car`,
                                                (SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_condition`) AS `condition`,
                                                (SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_location`) AS `location`
			FROM `products` ORDER BY `id` DESC  LIMIT $next, ". MAX_ITEMS_PER_PAGE_SITE .";";
		$result = parent::query($sql);
		if(parent::getRowCount() > 0){
			return $result;
		} else {
			return FALSE;
		}
	}
	
	public function get_images(){
		global $fw;
		return $fw->products()->getProductImagesByProductId($this->id);
	}
	
	public function getCrumb($id=""){
		global $fw;
		if($_REQUEST['p']=='catagory'){
			$catagory_id = $id;
		} else {
			$sql = "SELECT `catagories_id` FROM `products` WHERE `id` = '$this->id';";
			$result = parent::query2($sql);
			$catagory_id = $result['catagories_id'];
		}
		return $fw->meta()->crumb($catagory_id);
	}
	
	public function search($data,$next=0){
		$searchQuery = array();
		$search_text = mysql_real_escape_string($data['search_text']);
		$catagory = strip_tags($data['catagory']);
		
		$year = mysql_real_escape_string($data['year']);
		$make = mysql_real_escape_string($data['make']);
		$model = mysql_real_escape_string($data['model']);

		if($catagory !=0){
			$searchQuery[] = " AND `catagories_id` = '$catagory' "; 
		}
		if($year != 0){
			$searchQuery[] = " AND `meta_manufacture_year` = '$year' ";
		}
		if($make != 0){
			$searchQuery[] = " AND `meta_make` = '$make' ";
		}
		if($model != 0){
			$searchQuery[] = " AND `model` = '$model' ";
		}
		
		foreach($searchQuery as $s){
			$sqlStr .= $s;
		}
		
		$sql = "SELECT *,
					(SELECT `file` FROM `product_images` WHERE `products_id`=`products`.`id` ORDER BY `default` DESC LIMIT 1) AS `image`,
					(SELECT `label` FROM `catagories` WHERE `id`=`products`.`catagories_id`) AS `cat_name`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_manufacture_year`) AS `manufacture_year`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_make`) AS `make`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_transmission`) AS `meta_type_of_car`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_condition`) AS `condition`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_location`) AS `location`
				FROM `products` 
				WHERE 1 AND CONCAT(`keywords`, `description`, `title`) LIKE '%$search_text%'
						 $sqlStr ORDER BY `id` DESC LIMIT $next,". MAX_ITEMS_PER_PAGE_SITE .";";
		$results = parent::query($sql);
		if(parent::getRowCount() > 0){
			return parent::query($sql);
		} else {
			return FALSE;
		}
	}
	
	public function next($data){
		$searchQuery = array();
		$search_text = mysql_real_escape_string($data['search_text']);
		$catagory = strip_tags($data['catagory']);
		
		$id = mysql_real_escape_string($data['id']);
		
		$year = mysql_real_escape_string($data['year']);
		$make = mysql_real_escape_string($data['make']);
		$model = mysql_real_escape_string($data['model']);
		
		if($search_text !=""){
			$searchQuery[] = " AND CONCAT(`keywords`, `description`, `title`) LIKE '%$search_text%' ";
		}		
		if($catagory !=0){
			$searchQuery[] = " AND `catagories_relations_id` LIKE '%$catagory%' ";
		}
		if($year != 0){
		$searchQuery[] = " AND `meta_manufacture_year` = '$year' ";
		}
		if($make != 0){
		$searchQuery[] = " AND `meta_make` = '$make' ";
		}
		if($model != 0){
		$searchQuery[] = " AND `model` = '$model' ";
		}
		
		foreach($searchQuery as $s){
		$sqlStr .= $s;
		}
		
		$sql = "SELECT *,
					(SELECT `file` FROM `product_images` WHERE `products_id`=`products`.`id` ORDER BY `default` DESC LIMIT 1) AS `image`,
					(SELECT `label` FROM `catagories` WHERE `id`=`products`.`catagories_id`) AS `cat_name`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_manufacture_year`) AS `manufacture_year`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_make`) AS `make`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_transmission`) AS `meta_type_of_car`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_condition`) AS `condition`,
					(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_location`) AS `location`
				FROM `products`
				WHERE 1 AND CONCAT(`keywords`, `description`) LIKE '%$search_text%'
							$sqlStr AND `id` > $id LIMIT 1;";
		$result = parent::query2($sql);
		if(parent::getRowCount() > 0){
			$query = http_build_query(array('catagory'=>$catagory,
											'search_text'=>$search_text,
											'year'=>$year,
											'make'=>$make,
											'model'=>$model));
			echo HTTP_PATH . $result['id'] . '/'. strtolower(str_replace(" ", "-", $result['referance_id'])) . '.html?' . $query;
		}
	}
	
	public function previous($data){
		$searchQuery = array();
		$search_text = mysql_real_escape_string($data['search_text']);
		$catagory = strip_tags($data['catagory']);
	
		$id = mysql_real_escape_string($data['id']);
	
		$year = mysql_real_escape_string($data['year']);
		$make = mysql_real_escape_string($data['make']);
		$model = mysql_real_escape_string($data['model']);
	
		if($search_text !=""){
			$searchQuery[] = " AND CONCAT(`keywords`, `description`, `title`) LIKE '%$search_text%' ";
		}
		
		if($catagory !=0){
			$searchQuery[] = " AND `catagories_id` = '$catagory' ";
		}
		if($year != 0){
			$searchQuery[] = " AND `meta_manufacture_year` = '$year' ";
		}
		if($make != 0){
			$searchQuery[] = " AND `meta_make` = '$make' ";
		}
		if($model != 0){
			$searchQuery[] = " AND `model` = '$model' ";
		}
	
		foreach($searchQuery as $s){
			$sqlStr .= $s;
		}
		
		$sql = "SELECT *,
						(SELECT `file` FROM `product_images` WHERE `products_id`=`products`.`id` ORDER BY `default` DESC LIMIT 1) AS `image`,
						(SELECT `label` FROM `catagories` WHERE `id`=`products`.`catagories_id`) AS `cat_name`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_manufacture_year`) AS `manufacture_year`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_make`) AS `make`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_transmission`) AS `meta_type_of_car`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_condition`) AS `condition`,
						(SELECT `name` FROM `meta` WHERE `id`= `products`.`meta_location`) AS `location`
					FROM `products`
					WHERE 1 $sqlStr AND (`id` = '$id' - 1 OR  `id` < '$id') ORDER BY `id` DESC LIMIT 1;";
		
		$result = parent::query2($sql);
		if(parent::getRowCount() > 0){
			$query = http_build_query(array('catagory'=>$catagory,
											'search_text'=>$search_text,
											'year'=>$year,
											'make'=>$make,
											'model'=>$model));
			echo HTTP_PATH . $result['id'] . '/'. strtolower(str_replace(" ", "-", $result['referance_id'])) . '.html?' . $query;
		}
	}
}
?>
