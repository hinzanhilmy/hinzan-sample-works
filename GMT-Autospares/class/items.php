<?php
class items extends DB{
	private $id;
	public function __construct($data){
		parent::__construct();
	}
	
	public function detail($id){
		$sql = "SELECT *,
					(SELECT `label` FROM `catagories` WHERE `catagories`.`id` = `items`.`catagories_id`) as `cat_name`,
					(SELECT `id` FROM `catagories` WHERE `catagories`.`id` = `items`.`catagories_id`) as `cat_id`,
					(SELECT `organization_name` FROM `profiles` WHERE `profiles`.`id` = `items`.`profiles_id`) as `org_name`,
					(SELECT `id` FROM `profiles` WHERE `profiles`.`id` = `items`.`profiles_id`)  as `org_id`
				FROM `items` WHERE `id` = '$id';";
		$row = parent::query($sql);
		return (object) $row[0];
	}
	
	public function getImages($id){
		$sql = "SELECT * FROM `items_images` WHERE `items_id` = '$id';";
		return parent::query($sql);
	}
	
	public function getName($id){
		$sql = "SELECT * FROM `items` WHERE `id` = '$id';";
		$row = parent::query($sql);
		return $row[0]['title'];
	}
}
?>