<?php
class users extends DB{
	public function __construct(){
		parent::__construct();
	}
	
	public function add($data){
		global $fw;
		$fullname = $fw->xss()->safe($data['fullname']);
		$email = $fw->xss()->safe($data['email']);
		$company = $fw->xss()->safe($data['company']);
		$abn = $fw->xss()->safe($data['abn']);
		
		$streetno = $fw->xss()->safe($data['streetno']);
		$streetname = $fw->xss()->safe($data['streetname']);
		$postal_code = $fw->xss()->safe($data['postal_code']);
		$suburb = $fw->xss()->safe($data['suburb']);
		$state = $fw->xss()->safe($data['state']);
		
		$username = ($data['username'] != "")? $fw->xss()->safe($data['username']) : $email;
		$password = md5($data['password']);
		$accountrype = ($data['accountrype']=='NOLOG')? 'NOLOG' : $data['accountrype'];

		$sql = "INSERT INTO `users` (`fullname`,`username`,`password`,`account_type`, `company`, `abn`, `email`,
									 `streetno`, `streetname`, `postal_code`, `suburb`, `state`) 
				VALUES ('$fullname', '$username', '$password', '$accountrype', '$company', '$abn', '$email',
						'$streetno', '$streetname', '$postal_code', '$suburb', '$state');";
		parent::query($sql);
		
		//exit;
		$status = parent::status();
		if($status){
			$fw->mail()->register($data);
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get($data){
		$id = $_REQUEST['id'];
		if($id==""){		
			$sql = "SELECT * FROM `users`;";
			return parent::query($sql);
		} else if($id != "") {
			$sql = "SELECT * FROM `users` WHERE `id` = '$id';";
			$rows = parent::query($sql);
			return $rows[0];
			
		}
	}
	
	public function getbyType($type="USER"){
		$sql = "SELECT * FROM `users` WHERE `account_type` = '$type';";
		return parent::query($sql);
	}
	
	public function getbyid($data){
		$id = $data['id'];
		if($id==""){
			$sql = "SELECT * FROM `users`;";
			return parent::query($sql);
		} else if($id != "") {
			$sql = "SELECT * FROM `users` WHERE `id` = '$id';";
			$rows = parent::query($sql);
			return (object)$rows[0];
				
		}
	}
	
	public function pagination($next){
		$sql = "SELECT * FROM `users` ORDER BY `id` DESC LIMIT $next, ". MAX_ITEMS_PER_PAGE .";";
		return parent::query($sql);
	}
	
	public function total(){
		$sql = "SELECT * FROM `users`;";
		parent::query($sql);
		return parent::getAffectedRows();
	}
	
	public function pagination_cus($next){
		$sql = "SELECT * FROM `users`  WHERE `account_type` = 'NOLOG' OR `account_type` = 'USER' ORDER BY `id` DESC LIMIT $next, ". MAX_ITEMS_PER_PAGE .";";
		return parent::query($sql);
	}
	
	public function total_cus(){
		$sql = "SELECT * FROM `users`  WHERE `account_type` = 'NOLOG' OR `account_type` = 'USER';";
		parent::query($sql);
		return parent::getAffectedRows();
	}
	public function edit($data){
		global $fw;
		$fullname 		= $fw->xss()->safe($data['fullname']);
		$username 		= $fw->xss()->safe($data['username']);
		
		$email = $fw->xss()->safe($data['email']);
		$company = $fw->xss()->safe($data['company']);
		$abn = $fw->xss()->safe($data['abn']);
		
		$password 		= $data['password'];
		$accounttype 	= $fw->xss()->safe($data['accountrype']);
		$id 	  		= $data['id'];
		
		
		$streetno = $fw->xss()->safe($data['streetno']);
		$streetname = $fw->xss()->safe($data['streetname']);
		$postal_code = $fw->xss()->safe($data['postal_code']);
		$suburb = $fw->xss()->safe($data['suburb']);
		$state = $fw->xss()->safe($data['state']);
		
		if($password!=""){
			$password = md5($password);
			$sql = "UPDATE `users` SET 	`fullname` = '$fullname',
										`username` = '$username',
										`password` = '$password',
										`company` = '$company',
										`email` = '$email',
										`abn`	  = '$abn', 
										`account_type` = '$accounttype',
										`streetno` = '$streetno',
										`streetname` = '$streetname',
										`postal_code` = '$postal_code',
										`suburb` = '$suburb',
										`state` = '$state'										
					WHERE `id` = '$id';";
		} else {
			$sql = "UPDATE `users` SET 	`fullname` = '$fullname',
										`username` = '$username',
										`company` = '$company',
										`email` = '$email',
										`abn`	  = '$abn',
										`account_type` = '$accounttype',
										`streetno` = '$streetno',
										`streetname` = '$streetname',
										`postal_code` = '$postal_code',
										`suburb` = '$suburb',
										`state` = '$state'	
					WHERE `id` = '$id';";
		}
		parent::query($sql);
		$fw->mail()->verify($data);
		return TRUE;
	}
	
	public function isLogin(){
		if($_SESSION['SLOGINUSER'] == ""){
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	public function login($data){
		global $fw;
		$username = $fw->xss()->safe($data['username']);
		$password = $fw->xss()->safe($data['password']);
		
		$sql = "SELECT * FROM `users` WHERE `username` = '$username' AND `password` = MD5('$password')
					AND `account_type` !='NOLOG';";
		$row = parent::query($sql);
		if(parent::getRowCount() > 0){
			$_SESSION['SLOGINUSER'] =  $username;
			$_SESSION['SUSERTYPE'] =   $row[0]['account_type'];
			$_SESSION['SUSEREMAIL'] = $row[0]['email'];
			$_SESSION['SUSERFULLNAME'] = $row[0]['fullname'];
			$_SESSION['SUSERID'] = $row[0]['id'];
			$this->log($row[0]['id']);
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function logOut(){
		unset($_SESSION['SLOGINUSER']);
		unset($_SESSION['SUSERTYPE']);
		return TRUE;
	}

	private function getRealIpAddr(){		
	    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	      $ip=$_SERVER['HTTP_CLIENT_IP'];
	    }
	    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  {
	      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else {
	      $ip=$_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}
	
	public function log($id){
		$ip = $this->getRealIpAddr();
		$sql = "INSERT INTO `users_log` (`users_id`, `ip`) VALUES ('$id', '$ip');";
		parent::query($sql);
		return TRUE;
	}


	public function users_log_products($data){
		$productid = $data['id'];
		$id = $_SESSION['SUSERID'];
		$date = date('Y-m-d');
		$sql = "INSERT INTO `users_log_products` (`users_id`, `product_id`, `date`) VALUES ('$id', '$productid', '$date')
  					ON DUPLICATE KEY UPDATE `count`= `count`+1;";
		parent::query($sql);
		return TRUE;
	}


	public function checkmail($email){
		$sql = "SELECT * FROM `users` WHERE `email` = '$email';";
		parent::query($sql);
		return parent::getRowCount(); 
	}
}
?>
