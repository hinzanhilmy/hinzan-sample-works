<?php
class search extends DB{
	public function __construct(){
		parent::__construct();
	}
	
	public function get($data){
		global $fw;		
		$search = mysql_real_escape_string($data['search']);
		$catagory = mysql_real_escape_string($data['cat']);
		$in = implode(',', array_merge(array($catagory), $fw->meta()->getParentByArray($catagory)));
		
		$showNotPublic = ($fw->users()->isLogin()==TRUE) ? "`items`.`public` IN ('0','1') AND" : "`items`.`public` = '1' AND";
		
		$sql = "SELECT *, 
					(SELECT `image` FROM `items_images` WHERE `items_images`.`items_id` = `items`.`id` LIMIT 1) AS `image`,
					(SELECT `label` FROM `catagories` WHERE `catagories`.`id` = `items`.`catagories_id` LIMIT 1) AS `cat_name`
				FROM `items` 
				WHERE 1  AND `status` = '1'  AND $showNotPublic
					  CONCAT(`title`, `description`) LIKE '%$search%' OR
					  `keywords` LIKE '%$search%' 
					`items`.`catagories_id` IN ($in);";
		return parent::query($sql);
	}
}
?>