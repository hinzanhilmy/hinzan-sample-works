<?php
require_once(SYSTEM_PATH . '3rdparty/tcpdf/tcpdf_import.php');
class pdf extends TCPDF {
	public function __construct(){
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	}
	
	public function set_vendor_perches_order($data, $save=TRUE){
		$id = $data['id'];
		$email = $data['email'];
		$pdf_name = $data['pdf_name'];
		$items_html = $data['table'];
		$seller_company = $data['company'];
		
		$address_streetno = $data['address_streetno'];
		$address_streetname = $data['address_streetname'];
		$postal_code = $data['postal_code'];
		$suburb = $data['suburb'];
		$state = $data['state'];
		$abn = $data['abn'];
		
		$seller_phone = $data['phone'];
		$abn = $data['abn'];
		$html_table = $data['message'];
		$this->SetPrintHeader(false);
	
		// set document information
		$this->SetCreator(PDF_CREATOR);
		// set header and footer fonts
		$this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
		// set default monospaced font
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		// set margins
		$this->SetMargins(5, 5, 5, 5);
		$this->SetHeaderMargin(0);
		$this->SetFooterMargin(0);
	
		// set auto page breaks
		$this->SetAutoPageBreak(FALSE, 0);
	
		// set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		$this->SetFont('dejavusans', '', 10);
		$this->AddPage();
	
		$html = '
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: helvetica;-webkit-font-smoothing: antialiased;">
		<tr>
		<td>
		<table border="0" cellpadding="10" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
	
		</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td width="50%"><img width="200px" src="'.HTTP_PATH.'templates/home/img/gmt_main_logo.jpg"></td>
		<td width="50%" style="text-align: right;">Reference No. <span style="font-size: 21px;color: #00C59B;">'.$id.'</span></td>
		</tr>
		 
		</table>
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
	
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table><table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td width="49%" style="border: 1px solid #EAEAEA;">
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td style="color: #00C59B;font-size: 19px;height: 40px;vertical-align: top;">Seller</td>
		</tr>
		<tr>
		<td>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td align="left" style="vertical-align: top;text-align: left;"></td>
		<td>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td style="font-weight: bold;vertical-align: middle;font-size: 12px;line-height: 17px;">'.$seller_company.'</td>
		</tr><tr>
		<td style="vertical-align: middle;font-size: 12px;line-height: 17px;">'.$address_streetno. ', <br />' .
			$address_streetname. ', <br />' .
			$postal_code. ', <br />' .
			$suburb. ', <br />' .	
			$state . ', <br />' .
			$abn . ', <br />' .					
		'</td>
		</tr>
		<tr>
		<td style="vertical-align: middle;font-size: 12px;line-height: 17px;">'.$email.'</td>
		</tr>
		</table></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table>
		
		'. $html_table .'
		
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table><table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		</table>
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
		</table></td>
		</tr>
		</table>';
	
		$this->writeHTML($html, true, false, true, false, '');
		// reset pointer to the last page
		$this->lastPage();
		//Close and output PDF document
		if($save){
			$this->Output( SYSTEM_COUPON_PATH . $pdf_name . '.pdf', 'F');
			return $pdf_name . '.pdf';
		}/*  else {
			$this->Output($invoice_print_id.'.pdf', 'D');
			exit;
		} */
	}
	
	
	public function set_customer_invoice($data, $save=TRUE){
		$id = $data['id'];
		$email = $data['email'];
		$pdf_name = $data['pdf_name'];
		$items_html = $data['table'];
		$seller_company = $data['company'];
		
		$address_streetno = $data['address_streetno'];
		$address_streetname = $data['address_streetname'];
		$postal_code = $data['postal_code'];
		$suburb = $data['suburb'];
		$state = $data['state'];
		$abn = $data['abn'];
		
		$seller_phone = $data['phone'];
		$abn = $data['abn'];
		$html_table = $data['message'];
		$this->SetPrintHeader(false);
	
		// set document information
		$this->SetCreator(PDF_CREATOR);
		// set header and footer fonts
		$this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
		// set default monospaced font
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		// set margins
		$this->SetMargins(5, 5, 5, 5);
		$this->SetHeaderMargin(0);
		$this->SetFooterMargin(0);
	
		// set auto page breaks
		$this->SetAutoPageBreak(FALSE, 0);
	
		// set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		$this->SetFont('dejavusans', '', 10);
		$this->AddPage();
	
		$html = '
		<table border="0" cellpadding="8" cellspacing="0" width="100%" style="font-family: helvetica;-webkit-font-smoothing: antialiased;">
		<tr>
		<td>
		<table border="0" cellpadding="10" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
	
		</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td width="50%"><img width="200px" src="'.HTTP_PATH.'templates/home/img/gmt_main_logo.jpg"></td>
		<td width="50%" style="text-align: right;">Reference No. <span style="font-size: 21px;color: #00C59B;">'.$id.'</span></td>
		</tr>
			
		</table>
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
	
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table><table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td width="49%" style="border: 1px solid #EAEAEA;">
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td style="color: #00C59B;font-size: 19px;height: 40px;vertical-align: top;">INVOICE</td>
		</tr>
		<tr>
		<td>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td align="left" style="vertical-align: top;text-align: left;"></td>
		<td>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td style="font-weight: bold;vertical-align: middle;font-size: 12px;line-height: 17px;">'.$seller_company.'</td>
		</tr><tr>
		<td style="vertical-align: middle;font-size: 12px;line-height: 17px;">'.$address_streetno. ', <br />' .
			$address_streetname. ', <br />' .
			$postal_code. ', <br />' .
			$suburb. ', <br />' .	
			$state . ', <br />' .
			$abn . ', <br />' .					
		'</td>
		</tr><tr>
		<td style="vertical-align: middle;font-size: 12px;line-height: 17px;">'.$email.'</td>
		</tr>
		</table></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table>
	
		'. $html_table .'
	
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table><table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		</table>
		<table border="0" cellpadding="15" cellspacing="0" width="100%" style="width: 100%;">
		<tr>
		<td>&nbsp;</td>
		</tr>
		</table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
		</table></td>
		</tr>
		</table>';
	
		$this->writeHTML($html, true, false, true, false, '');
		// reset pointer to the last page
		$this->lastPage();
		//Close and output PDF document
		if($save){
			$this->Output( SYSTEM_COUPON_PATH . $pdf_name . '.pdf', 'F');
			return $pdf_name . '.pdf';
		}/*  else {
		$this->Output($invoice_print_id.'.pdf', 'D');
		exit;
		} */
	}
}