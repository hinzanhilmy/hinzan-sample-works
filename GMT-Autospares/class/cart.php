<?php
include_once ( './system/eway_payment.php' );

class cart extends DB{
	public function __construc(){
		global $client;		
		parent::__construct();
	}		
	public function add($data){
		global $fw;
		$action = $fw->xss()->safe($data['action']);
		$id = $fw->xss()->safe($data['id']);
		$qun = $fw->xss()->safe($data['qun']);				
		switch($action){
			case 'add': $this->add_cart($id,$qun);	break;
			case 'remove': $this->remove_cart($id);	break;
		}
		return TRUE;
	}
	
	private function add_cart($id,$qun){
		global $fw;
		$qun = ($qun==0)? 1 : $qun;		
		
		/* if check out of stock
		 * $product= $fw->products()->loadProduct($id);
		$product = $product[0]; */
		
		$_SESSION['CART'][$id] = array('id'=> $id, 'qun'=> $qun);
		
		/* 
		 * if check out of stock
		 * if($qun <= $product['qoh']){
			$_SESSION['CART'][$id] = array('id'=> $id, 'qun'=> $qun);
		} else {
			$fw->set_session_message(array('text'=> 'The Product <u>'. $product['title'] .'</u> quantity is out of stock', 'type'=>FALSE));
		} */
		return TRUE; 
	}
	
	private function remove_cart($id){
		unset($_SESSION['CART'][$id]);
		return TRUE;
	}
	
	public function list_cart(){
		global $fw;
		$result = array();
		foreach($_SESSION['CART'] as $k=>$v){
			$product = $fw->products()->loadProduct($k);
			$rs = array_merge($product[0], array('qun'=>$v['qun']));
			$result[$k] = $rs;
		}
		return $result;
	}
	
	public function is_qoh_exceed(){
		global $fw;
		$_SESSION['CART_REMOVE_ITEMS'] = '';
		$_SESSION['CART_EXCEED'] = FALSE;
		foreach($_SESSION['CART'] as $k=>$v){
			$product = $fw->products()->loadProduct($k);
			if($product[0]['qoh'] < $v['qun']){
				$_SESSION['CART_REMOVE_ITEMS'][$k] = $k;
				$_SESSION['CART_EXCEED'] = TRUE;
			}
		}
	} 
	
	
	public function send($data){
		global $fw;		
		$cart_agree = ($data['cart_agree'] == 1) ? TRUE : FALSE;
		$products = $this->list_cart();
		
		$table = "";
		$total = 0;
		$table = "<table class='html_table'>
					<thead>
					<tr>
						<th>Product</th>
						<th>Name</th>
						<th>Unit Price</th>
						<th>Qun</th>
						<th>Total</th>
					</tr>
					</thead>
					<tbody>";
	
		foreach($products as $id=>$p){			
				$total += $p['price'] * $p['qun'];	
				$price = number_format(round($p['price'], 1) , 2);
				$item_total = number_format( round(($p['price'] * $p['qun']), 1), 2);
				
				if($cart_agree){
					$agree_comment = ($_SESSION['CART_REMOVE_ITEMS'][$id] == $id)? ' - Agreed - &#x2713;' : '';
				} else {
					$agree_comment = ($_SESSION['CART_REMOVE_ITEMS'][$id] == $id)? ' - Agreed - &#x2716;' : '';
				}
				
				$table .= "<tr>
								<td><img class='media-object img-rounded img-responsive' style='height:100px;'  src='" . UPLOAD_THUMB_PATH . IMAGE_SIZE_LARGE . $p['image']. "' alt='" .  $p['title'] ."' /></td>
								<td>". $p['title'] ."</td>
								<td>". $price ."</td>
								<td>". $p['qun'] ." $agree_comment</td>
								<td>". $item_total ."</td>
							</tr>";
		}
		
		$pureTotal = $total;
		$total = number_format(round($total, 1) , 2);
		$gsp   = number_format( round( (($pureTotal / 100) * GSP) , 1) , 2);
		$grandtotal = number_format( round( ($pureTotal + $gsp) , 1) , 2);
		$pureGrandTotal = round( ($pureTotal + $gsp) , 1) * 100;		
				
		$table .= "<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Subtotal (AUD)</td>
						<td>". $total ."</td>
				   </tr>
				  <tr>
						<td></td>
						<td></td>
						<td></td>
						<td>GST(AUD)</td>
						<td>". $gsp ."</td>
				   </tr>
				   <tr>
						<td></td>
						<td></td>
						<td></td>
						<td><b>Total (AUD)</b></td>
						<td><b>". $grandtotal ."</b></td>
				   </tr>
				</tbody>";
		$table .= "</table>";

		
 		$name = $data['name'];
		$number = $data['ccn'];
		$exmonth = $data['exmonth'];
		$exyear = $data['exyear'];
		$cvn = $data['cvn'];
		
		$amount = $data['total'];
		$invoiceno = $data['invoiceno']; 		
				
		$orderid = $this->set_order(array('agree'=> $cart_agree));
		$eway_data =  array('name'=> $name,
				'ccn'=> $data['ccn'],
				'exmonth'=> $exmonth,
				'exyear'=> $exyear,
				'cvn'=> $cvn,
				'total'=> $pureGrandTotal,
				'invoiceno'=> $orderid);
		
		$eway_status = eway_payment($eway_data);
		$this->update_eway(array('id'=> $orderid, 'eway_id'=> $eway_status->code, 'message'=> $eway_status->message));
		$send = array('email'=> $_SESSION['SUSEREMAIL'], 'fullname'=> $_SESSION['SUSERFULLNAME'], 'table'=> $table , 'eway_transaction'=> $eway_status->message);
		
		if($fw->mail()->send_order($send) && $eway_status->status != FALSE){
			unset($_SESSION['CART']);
			unset($_SESSION['CART_REMOVE_ITEMS']);
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function set_order($data){
		global $fw;
		$products = $this->list_cart();
		$userid = $_SESSION['SUSERID'];
		$agree = $data['agree'];
		
		$sql = "INSERT INTO `order_id` (`user_id`, `status`) VALUES ('$userid' , 'PENDING');";
		parent::query($sql);
		$orderid = parent::getLastId();
				
		if($_SESSION['CART_EXCEED']){
			$overstock_status = 1;
		} else {
			$overstock_status = 0;
		}
		
		foreach($products as $id=>$p){
			if($_SESSION['CART_EXCEED']){
				$product = $fw->products()->loadProduct($id);
				if($product[0]['qoh'] < $p['qun']){
					if($agree){
						$agree_status = 1;
					} else {
						$agree_status = 0;
					}
				} else {
					$agree_status = 2;
				}
			} else {
				//ignore
				$agree_status = 2;
			}
			
			$sql = "INSERT INTO `orders` (`order_id`, `product_id`, `price`, `qun`, `agreed` , `overstock_status`) 
						VALUES ('$orderid', '$id', '" . $p['price'] ."','" . $p['qun'] ."','$agree_status', '$overstock_status');";
			parent::query($sql);
		}
		return $orderid;
	}
	
	
	public function orders_pagination($next){
		$sql = "SELECT *,
					 (SELECT (SUM(`price`) * SUM(`qun`)) FROM `orders` WHERE `order_id` = `order_id`.`id`) AS `total_price`,
					 (SELECT COUNT(*) FROM `orders` WHERE `order_id` = `order_id`.`id`) AS `total_count`
					FROM `order_id` ORDER BY `date` DESC LIMIT $next, ". MAX_ITEMS_PER_PAGE .";";
		return parent::query($sql);
	}
	
	public function order_list($id){
		$sql = "SELECT * FROM `orders` WHERE `order_id` = '$id';";
		return parent::query($sql);
	}
	
	public function total_orders(){
		$sql = "SELECT * FROM `order_id`;";
		parent::query($sql);
		return parent::getRowCount();
	}
	
	public function admin_update($data){
		global $fw;
		$id = $data['id'];
		$productid = $data['product_id'];
		$qun = $data['qun'];
		
		$product = $fw->products()->loadProduct($productid);
		
		$sql = "SELECT * FROM `orders` WHERE `order_id` = '$id' AND `product_id` = '$productid';";
		$past_order = parent::query($sql);		
		$previus_qun = $past_order[0]['qun'];
		
		if($product[0]['qoh'] >= $qun){
			$sql = "UPDATE `orders` 
						SET `qun` = '$qun'
					WHERE `order_id` = '$id' AND `product_id` = '$productid';";
			parent::query($sql);			
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function admin_delete($data){
		$id = $data['id'];
		$productid = $data['product_id'];
		
		$sql = "SELECT * FROM `orders` WHERE `order_id` = '$id' AND `product_id` = '$productid';";
		$qun = parent::query($sql);
				
		//adding quantity from removed
		$qun = $qun[0]['qun'];
		$sql = "UPDATE `product` SET `qoh` = `qoh` + ". $qun ." WHERE `id`= '$productid';";
		parent::query($sql);

		
		$sql = "DELETE FROM `orders` WHERE `order_id` = '$id' AND `product_id` = '$productid';";
		parent::query($sql);
		
		$sql = "SELECT * FROM `orders` WHERE `order_id` = '$id';";
		parent::query($sql);
		
		if(parent::getRowCount() == 0){
			$sql = "DELETE FROM `order_id` WHERE `order_id` = '$id';";
			parent::query($sql);
		}
		return TRUE;
	}
	
	public function reject($data){
		$id = $data['id'];
		$sql = "UPDATE `order_id` SET `status` = 'REJECTED' WHERE `id`= '$id';";
		parent::query($sql);
		return TRUE;
	} 

	public function payment_declined($data){
		$id = $data['id'];
		$sql = "UPDATE `order_id` SET `status` = 'PAYMENT_DECLINED' WHERE `id`= '$id';";
		parent::query($sql);
		return TRUE;
	}
	
	public function proceed($data){
		global $fw;
		$id = $data['id'];
		
		$sql = "SELECT * , (SELECT `user_id` FROM `order_id` WHERE `order_id`.`id` = `orders`.`order_id`) AS `user_id` FROM `orders` WHERE `order_id` = '$id';";
		$order = parent::query($sql);
		foreach($order as $o){ 
			$productid = $o['product_id'];
			//Send PDF TO Customer
			$table = "<table class='html_table'>
						<tr>
						<th>Product</th>
						<th>Name</th>
						<th>Unit Price</th>
						<th>Qun</th>
						<th>Total</th>
						</tr>";
						foreach($order as $o){
							$product = $fw->products()->loadProduct($o['product_id']);
							$total += $o['price'] * $o['qun'];
							$table .= "<tr>
							<td><img class='media-object img-rounded img-responsive' style='height:100px;'  src='" . UPLOAD_THUMB_PATH . IMAGE_SIZE_LARGE . $product[0]['image']. "' alt='" .  $product[0]['title'] ."' /></td>
							<td>". $product[0]['title'] ."</td>
							<td>". $o['price'] ."</td>
							<td>". $o['qun'] ."</td>
							<td>". $o['price'] * $o['qun'] ."</td>
							</tr>";
						}

							$pureTotal = $total;
							$total = number_format(round($total, 1) , 2);
							$gsp   = number_format( round( ($pureTotal / 100) * GSP , 1) , 2);
							$grandtotal = number_format( round( ($pureTotal + $gsp) , 1) , 2);						
										
								$table .= "<tr>
												<td></td>
												<td></td>
												<td></td>
												<td>Subtotal (AUD)</td>
												<td>". $total ."</td>
										   </tr>
										  <tr>
												<td></td>
												<td></td>
												<td></td>
												<td>GST(AUD)</td>
												<td>". $gsp ."</td>
										   </tr>
										   <tr>
												<td></td>
												<td></td>
												<td></td>
												<td><b>Total (AUD)</b></td>
												<td><b>". $grandtotal ."</b></td>
										   </tr>";
			$table .= "</table>";
							
						$user = $fw->users()->getbyid(array('id'=> $o['user_id']));						
						$mail_data = array(	'fullname'=> $user->fullname,
								'email'=> $user->email,
								'company'=> $user->company,
								'address_streetno'=> $user->streetno,
								'address_streetname'=> $user->streetname,
								'postal_code'=> $user->postal_code,
								'suburb'=> $user->suburb,
								'state'=> $user->state,
								'abn'=> $user->abn,
								'phone'=> $user->phone,
								'message'=>$table,
								'subject'=> 'GMT Order Confirmation',
								'pdf_name'=> 'GMT_IV_' . $id,
								'id'=> 'GMT/IV/' . $id);
						
						$fw->pdf()->set_customer_invoice($mail_data);
						$fw->mail()->send_pdf_customer($mail_data);
						
			
			$sql = "UPDATE `product` SET `qoh` = `qoh` - ". $o['qun'] ." WHERE `id`= '$productid';";
			parent::query($sql);
			
			$sql = "UPDATE `order_id` SET `status` = 'INVOICED' WHERE `id`= '$id';";
			parent::query($sql);
		}
		return TRUE;
	}
	
	public function admin_add_product_invoice($data){
		global $fw;
		$add_products = $data['add_products'];
		$price_type = ($data['price_type']==1)? 'price' : 'dealer_price';
		$id = $data['id'];
		foreach($add_products as $p){
			$product = $fw->products()->loadProduct($p);
			$price = $product[0][$price_type];
			$sql = "INSERT INTO `orders` (`product_id`, `order_id`, `qun`, `price`)
						VALUES('$p', '$id', '1', '$price');";
			parent::query($sql);
		}
	}
	
	public function new_order($data){
		global $fw;
		$userid = $data['userid'];
		$products = $data['add_products'];
		$price_type = ($data['price_type']==1)? 'price' : 'dealer_price';
		$ip = $_SERVER['REMOTE_ADDR'];

		if($_SESSION['SE_NEW_ORDERID'] == ""){
			$sql = "INSERT INTO `order_id` (`user_id`, `status`, `ip`) VALUES ('$userid' , 'PENDING', '$ip');";
			parent::query($sql);
			$orderid = $_SESSION['SE_NEW_ORDERID'] = parent::getLastId();
			$_SESSION['SE_NEW_USERID'] = $userid;
		} else {
			$orderid = $_SESSION['SE_NEW_ORDERID'];
			$userid = $_SESSION['SE_NEW_USERID'];
		}
		
		foreach($products as $p){
			$product = $fw->products()->loadProduct($p);
			$price = $product[0][$price_type];
			$sql = "INSERT INTO `orders` (`order_id`, `product_id`, `price`, `qun`)
						VALUES ('$orderid', '$p', '" . $price ."','1');";
			parent::query($sql);
		}
		return TRUE;
	}

	public function com_product_add($data){
		$com_product = $data['com_product'];
		$_SESSION['COM_PRODUCT'] = $com_product;
		return TRUE;
	}
	
	public function update_eway($data){
		$status = $data['eway_id'];
		$id = $data['id'];
		$message = $data['message'];
		$sql = "UPDATE `order_id` SET `eway_id`='$status' , `eway_message` = '$message' WHERE `id` = '$id';";
		parent::query($sql);
		return TRUE;
	}
}
?>