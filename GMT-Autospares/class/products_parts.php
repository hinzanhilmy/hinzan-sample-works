<?php
class products_parts extends db_parts{
	public function __construct(){
		parent::__construct();
	}
	
	public function all(){
		$sql = "SELECT * FROM `product` ORDER BY `title` ASC;";
		return parent::query($sql);
	} 
	 
	public function add($data){
		global $fw;
		$status = false;

		$cat = $fw->xss()->safe(($data['sub']=="0")? $data['id'] : $data['sub']);
		$title = addslashes($fw->xss()->safe($data['title']));
		$desc = addslashes($data['description']);
		$highlight = addslashes($fw->xss()->safe($data['highlight']));
		$new_arrivals = ($data['new_arrivals']==1) ? 1 : 0;
		$qoh = $fw->xss()->safe($data['qoh']);
		$keywords = addslashes($fw->xss()->safe($data['keywords']));
		$performances = addslashes($data['performances']);
		$re_orderlevel = $fw->xss()->safe($data['reorder_level']);
		
		$price = $fw->xss()->safe($data['price']);
		$dprice = $fw->xss()->safe($data['d_price']);

		$fe = $data['fe'];		
		
		$re = $fw->meta()->catagories_relations_ids($cat);
		if($this->is_exsist(array('title'=> $title)) == 0){
			$sql = "INSERT INTO `product` (`catagories_id`, `catagories_relation`, `title`, `desc`, `highlight`, `new_arrivals`, `performances`, `price`, `dealer_price`, `qoh`, `keywords`, `re_orderlevel`) 
					VALUES ('$cat', '$re', '$title', '$desc', '$highlight', '$new_arrivals', '$performances', '$price', '$dprice' ,'$qoh', '$keywords', '$re_orderlevel')";
			parent::query($sql);
			$status = parent::status();

			$productId = parent::getLastId();
			$data = array_merge($data,array('id'=>$productId));		
			$this->product_image_add($data);
			$this->set_DefaultImage($data);
			$this->add_features($data);


		} else {
			$status = false;
		}

		return $status;
	}
	
	public function  product_image_add($data){
		$id = $data['id'];
		foreach ($data['images'] as $key=>$m){
			$image = $m;
			copy(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_THUMB.$image, UPLOAD_ITEM_FULL_PATH. IMAGE_SIZE_THUMB.$image);
			copy(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_LARGE.$image, UPLOAD_ITEM_FULL_PATH. IMAGE_SIZE_LARGE.$image);
				
			unlink(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_THUMB.$image);
			unlink(UPLOAD_ITEM_FULL_PATH_TEMP. IMAGE_SIZE_LARGE.$image);
	
			$title = addslashes($data['title'][$key]);
			$description = addslashes($data['description'][$key]);
			$sql = "INSERT INTO `product_images` (`products_id`,`file`) VALUES  ('$id','$image');";
			parent::query($sql);
		}
		return TRUE;
	}
	
	public function set_DefaultImage($data){
		$default_image = $data['default_image'];
		$sql = "UPDATE `product_images` SET `default`  = '1' WHERE `file` = '$default_image';";
		parent::query($sql);
		return TRUE;
	}
	
	public function add_features($data){
		foreach($data['fe'] as $f){
			$val = $data['fe_val'][$f];
			$order = $data['fe_order'][$f];
			$sql = "INSERT INTO `features` (`product_id`,`meta_id`, `value` , `sort`) VALUES  ('". $data['id'] . "','$f', '$val', '$order');";
			parent::query($sql);
		}
		return TRUE;
	}
	
	public function reset_features($data){
		$product_id = $data['id'];
		$sql = "DELETE FROM `features` WHERE `product_id` = '$product_id'";
		$this->add_features($data);
		parent::query($sql);
		return TRUE;
	}
	
	public function getFutureValue($id, $product_id){
		$sql = "SELECT * FROM `features` WHERE `product_id` = '$product_id' AND `meta_id` = '$id';";
		$row = parent::query2($sql);
		return $row['value'];
	}
		
	public function getFutureOrder($id, $product_id){
		$sql = "SELECT * FROM `features` WHERE `product_id` = '$product_id' AND `meta_id` = '$id';";
		$row = parent::query2($sql);
		return $row['sort'];
	}	
	
	public function edit($data){
		global $fw;
		$id = $data['id'];
		$cat = $fw->xss()->safe($data['catid']);
		$title = addslashes($fw->xss()->safe($data['title']));
		$desc = addslashes($data['description']);
		$highlight = addslashes($data['highlight']);
		$fe = $data['fe'];		
		$new_arrivals = ($data['new_arrivals']==1) ? 1 : 0;
		$re = $fw->meta()->catagories_relations_ids($cat);	
		$qoh = $fw->xss()->safe($data['qoh']); 
		$performances = addslashes($data['performances']);
		$keywords = addslashes($fw->xss()->safe($data['keywords']));
		$price = $fw->xss()->safe($data['price']);
		$dprice = $fw->xss()->safe($data['d_price']);
		
		$this->deleteSelectedImages($data['update_delete'],$id);
		$this->product_image_add($data);		
		$this->resetDefaultImage($id);	
		$this->setDefaultImage($id, $data['default_image']);
		
		$this->reset_features($data);
		$this->add_features($data);		
		$re_orderlevel = $fw->xss()->safe($data['reorder_level']);
		$sql = "UPDATE `product`
						SET 
							`catagories_id` = '$cat',
							`catagories_relation` = '$re',
							`title` = '$title',
							`desc` = '$desc',
							`highlight` = '$highlight',
							`new_arrivals` = '$new_arrivals',
							`performances` = '$performances',
							`price` = '$price',
							`dealer_price` = '$dprice',
							`qoh` = '$qoh',
							`keywords` = '$keywords',
							`re_orderlevel` = '$re_orderlevel'
					WHERE `id` = '$id';";
					
		
		parent::query($sql);
		return parent::status();
	}
	
	public function is_features($id, $fid){
		$sql = "SELECT * FROM `features` WHERE `product_id` = '$id' AND `meta_id` = '$fid';";
		parent::query($sql);
		if(parent::getAffectedRows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function is_exsist($data){
		$title = addslashes($data['title']);
		$sql = "SELECT * FROM `product` WHERE `title`='$title';";
		parent::query($sql);
		return parent::getRowCount();
	}
		
	public function deleteSelectedImages($data,$id){
		foreach($data as $imageid=>$d){
			$sql = "DELETE FROM `product_images` WHERE `id`='$imageid' AND `products_id` = '$id';";
			parent::query($sql);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_THUMB. $d);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_LARGE. $d);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_EXTRALARGE. $d);		
		}
		return TRUE;

	}

	public function setDefaultImage($productId, $file){
		$sql = "UPDATE `product_images` SET `default`='1' WHERE `file`='$file' AND `products_id` = '$productId';";
		@parent::query($sql);
		return TRUE;
	}

        public function resetDefaultImage($productId){
                $sql = "UPDATE `product_images` SET `default`='0' WHERE `products_id` = '$productId';";
                @parent::query($sql);
                return TRUE;
        }
	
	public function addImages($data,$hidden_images,$id){
		if(count($hidden_images) > 0){
			foreach($hidden_images as $d){
				$list = explode("-",$d);
				if(in_array($d,  $data)){
					$sql = "INSERT INTO `product_images` (`products_id`,`file`)  VALUES ('$id','".$list[2]."');";
					parent::query($sql);
				} else {
					unlink(UPLOAD_ITEM_FULL_PATH . $d);
					unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_EXTRALARGE. $list[2]);
					unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_LARGE. $list[2]);
				}
			}
		}
		return TRUE;
	}
	
	public function genMetaFields($data){
		$result = array();
		foreach($data as $key=>$value){
			$k = explode("_",$key);
			if($k[0]=='meta'){
				$result['fields'][] = $key;
				$result['values'][] = $value;
			}
		}
		return $result;
	}
	
	public function genMetaEditFields($data){
		$result = array();
		foreach($data as $key=>$value){
			$k = explode("_",$key);
			if($k[0]=='meta'){
				$result[] = "`$key` = '" . $value . "'";
			}
		}
		return $result;
	}
	
	public function get($data){
		$sqlStr = "";
		$id = $data['catagory'];
		$search_text = $data['search_text'];
		if($id != "0"){
			$strCat = " `catagories_id` LIKE '%$id%' OR `catagories_relation` LIKE '%$id%' ";
		}
		if($search_text!=""){
			$sqlStr = " `title` LIKE '%$search_text%' OR `catagories_relation` LIKE '%$search_text%' OR `desc` LIKE '%$search_text%' ";
		}
		if($search_text!="" || $id !=0)
			$sql = "SELECT *, (SELECT `label` FROM `catagories` WHERE `id` = `product`.`catagories_id`) AS `catagory_name` 
						FROM `product`
					WHERE 1 AND $strCat
					$sqlStr	
					ORDER BY `timestamp` DESC LIMIT 30;";
		return parent::query($sql);
	}
	
	public function get_edit($id){
		$sql = "SELECT * FROM `product`	WHERE `id` = '$id'";
		return parent::query2($sql);
	}
	
	public function get_productsby_cat($id){
		if($id!=""){
			$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` FROM `product`  
					WHERE `catagories_id` = '$id' AND `status` = '1' ORDER BY `sort` ASC;";
		} else {
			$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` 
					FROM `product`  WHERE `status` = '1' ORDER BY `sort` ASC;";
		}
		return parent::query($sql);
	}
	
	public function search($next){ 
		global $fw;
		$search = $_SESSION['search-text'];
		$single_search = $this->search_bytitle($search);
		
		if($_SESSION['universal'] == FALSE){
			$universal = '';
		} else {
			$universal = '  OR `keywords` LIKE '%universal%'';
		}
		
		if(sizeof($single_search) > 0){
			return $single_search;
		} else {
			$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` FROM `product`
						WHERE (`title` LIKE '%$search%' OR 	`desc` LIKE '%$search%' OR `keywords` LIKE '%$search%' $universal) AND `status` = '1' ORDER BY `sort` ASC  LIMIT $next," . MAX_ITEMS_PER_PAGE . ";";
			return parent::query($sql);
		}
	}
	
	public function search_total($next){
		global $fw;
		$search = $_SESSION['search-text'];
		
		if($_SESSION['universal'] == FALSE){
			$universal = '';
		} else {
			$universal = '  OR `keywords` LIKE '%universal%'';
		}
		
		$single_search = $this->search_bytitle($search);
		if(sizeof($single_search) > 0){
			return 1;
		} else {
			$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` FROM `product`
					WHERE (`title` LIKE '%$search%' OR 	`desc` LIKE '%$search%' OR `keywords` LIKE '%$search%'  OR `keywords` $universal) AND `status` = '1';";
			parent::query($sql);
			return parent::getRowCount();
		}
	}

	public function search_bytitle($title){
		$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` FROM `product`
		WHERE `title`='$title' AND `status` = '1';";
		return parent::query($sql);
	}
	
	public function new_arivals(){
		$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` 
					FROM `product` WHERE `product`.`new_arrivals` = '1' AND `status` = '1'
					ORDER BY `id` DESC LIMIT 16;";
		return parent::query($sql);
	}
	
	public function loadProduct($id){
		$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = '$id' AND `default` = '1' LIMIT 1) as `image`  FROM `product` WHERE `id` = '$id';";
		return parent::query($sql);	
	}
	
	public function getProductImagesByProductId($id){
		$sql = "SELECT * FROM `product_images` WHERE `products_id` = '$id' ORDER BY `default` DESC;";
		return parent::query($sql);
	}
	
	public function delete($id){
		foreach($this->getProductImagesByProductId($id) as $i){
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_LARGE. $i['file']);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_THUMB . $i['file']);
			@unlink(UPLOAD_ITEM_FULL_PATH . IMAGE_SIZE_EXTRALARGE. $i['file']);
		}
		$sql = "DELETE FROM `product_images` WHERE `products_id` = '$id';";
		parent::query($sql);
		$sql = "DELETE FROM `product` WHERE `id` = '$id';";
		parent::query($sql);
		return  TRUE;
	}
	
	public function getLastId(){
		$sql = "SELECT `id` FROM `products` ORDER BY `id` DESC LIMIT 1;";
		$result = parent::query2($sql);
		return "DPA-" . str_pad($result['id']+1, 5 ,"0" ,STR_PAD_LEFT);
	}
	
	public function is_default_image($productId,$file){
		$sql = "SELECT `default` FROM `product_images` WHERE `products_id`='$productId' AND `file` = '$file';";
		$row = parent::query2($sql);
		if($row['default'] == 1){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function sold($data){
		$id = strip_tags($data['id']);
		$result = $this->loadProduct($id);
		$current_status = $result[0]['sold_status'];
		if($current_status == "SOLD"){
			$chanage_status = 'AVAILABLE';
		} else {
			$chanage_status = 'SOLD';
		}
		$sql = "UPDATE `products` SET `sold_status` = '$chanage_status' WHERE `id` = '$id';";
		parent::query($sql);
		return TRUE;	
	}
	
	public function pagination($next){
		$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file`  
				FROM `product` 
				WHERE `status` = '1'
				ORDER BY `id` DESC LIMIT $next," . MAX_ITEMS_PER_PAGE . ";";
		return parent::query($sql);
	}

	public function admin_pagination($next){
		$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file`  
				FROM `product` 
				ORDER BY `id` DESC LIMIT $next," . MAX_ITEMS_PER_PAGE . ";";
		return parent::query($sql);
	}
	
	public function total($data){
		$status = $data['status'];
		if($status == 1){
			$sql = "SELECT * FROM `product` WHERE `status` = '1';";	
		} else {
			$sql = "SELECT * FROM `product`;";
		}
		parent::query($sql);
		return parent::getRowCount();
	}
	
	public function sort($data){
		$sort = $data['sort'];
		foreach($sort as $k=>$s){
			$sql = "UPDATE `product` SET `sort` = '$s' WHERE `id` = '$k';";
			parent::query($sql);
		}
		return TRUE;
	}
	
	public function api($data){
		$search = $data['search'];
		$sql = "SELECT * FROM `product` WHERE `title` LIKE '%$search%' AND `status` = '1';";
		return parent::query($sql);
	}
	
	public function outofstock(){
		$sql = "SELECT * , (SELECT `file` FROM `product_images` WHERE `products_id` = `product`.`id` AND `default` = '1' LIMIT 1) as `file` 
					FROM `product` WHERE `qoh` <= `re_orderlevel` ORDER BY `sort` ASC;";
		return parent::query($sql);
	}
	
	public function admin_get_orderproduct($id){
		$sql = "SELECT * FROM `product` WHERE id NOT IN (SELECT product_id FROM orders where orders.order_id = '$id');";
		return parent::query($sql);
	}
	
	public function status($data){
		$status = $data['status'];
		$id = $data['id'];
		$sql = "UPDATE `product` SET `status` = '$status' WHERE `id` = '$id';";
		parent::query($sql);
		return TRUE;
	}
}
?>