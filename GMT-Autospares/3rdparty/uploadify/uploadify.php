<?php
include ('../../system/main.php');
/*
Uploadify v2.1.4
Release Date: November 8, 2010

Copyright (c) 2010 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/* if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$fileinfo = explode(".",$_FILES['Filedata']['name']);
	$filename = time().".".$fileinfo[1];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
	$targetFile =  str_replace('//','/',$targetPath) . $filename;

		move_uploaded_file($tempFile,$targetFile);
		str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
		echo $_SESSION['UPLOADED_FILE_NAME'] = $filename;
} */

if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$fileinfo = explode(".",$_FILES['Filedata']['name']);
	$fileinfo[1] = strtolower($fileinfo[1]);
	$filename = trim($fileinfo[0]).'-'.time().".".$fileinfo[1];
	$targetPath = UPLOAD_FILE_FULL_PATH;
	$targetFile =  str_replace('//','/',$targetPath) . $filename;

	$source_image_path_only = UPLOAD_FILE_FULL_PATH;
	$source_image_path = UPLOAD_FILE_FULL_PATH . $filename;

	$thumbnail_image_path = UPLOAD_FILE_THUMB_FULL_PATH;


	if ($fileinfo[1] == "pdf" || $fileinfo[1] == "doc" || $fileinfo[1] == "docx" || $fileinfo[1] == "xlx"){
		move_uploaded_file($tempFile,$targetFile);
		echo $filename;
	} else {
		echo "Etner correct format. (doc, docx, pdf, xlx)";
	}
}
?>