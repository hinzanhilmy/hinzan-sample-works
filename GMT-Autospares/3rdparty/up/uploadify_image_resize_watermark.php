<?php
include ('../../system/main.php');
include ('../../class/watermark.php');
?>
<?php
if (!empty($_FILES)) {
	$kbytes = $_FILES['up_file']['size'] / 1024;
	$mbytes = $kbytes / 1024; // 1KB *  1024 - MB 3MB
	if(round($kbytes) < 4096 && round($kbytes) > 0){		
		$profileid = null;
		$tempFile = $_FILES['up_file']['tmp_name'];
		$ext = strtolower(pathinfo($_FILES['up_file']['name'], PATHINFO_EXTENSION));
		$filename =  str_replace(" ", "", microtime()) . '.' . $ext;
		
		$source_image_path_only = UPLOAD_ITEM_FULL_PATH_TEMP;
		$source_image_path = UPLOAD_ITEM_FULL_PATH_TEMP . $filename;
		$thumbnail_image_path = UPLOAD_ITEM_FULL_PATH_TEMP;
		
		move_uploaded_file($tempFile,UPLOAD_ITEM_FULL_PATH_TEMP.$filename);
		
		
		if ($ext == "jpg" || $ext == "jpeg" || $ext == "png" || $ext == "gif"){
			generate_image_thumbnail($source_image_path,$thumbnail_image_path, IMAGE_SIZE_THUMB . $filename ,230,160,FALSE);
	        	generate_image_thumbnail($source_image_path,$source_image_path_only, IMAGE_SIZE_LARGE . $filename ,500,500, TRUE);
	        	/* generate_image_thumbnail($source_image_path,$source_image_path_only,IMAGE_SIZE_LARGE . '-promotion-' . $filename ,500,500, TRUE); */
	         
	        	$watermark = new Watermark($source_image_path_only.IMAGE_SIZE_LARGE. $filename);
	        	$watermark->setWatermarkImage(UPLOAD_LOGO_PATH . "watermark.png");
	        	$watermark->setType(Watermark::CENTER);
	        	$a = $watermark->saveAs($source_image_path_only.IMAGE_SIZE_LARGE. $filename);


	        	$watermark = new Watermark($source_image_path_only.IMAGE_SIZE_THUMB. $filename);
	        	$watermark->setWatermarkImage(UPLOAD_LOGO_PATH . "watermark.png");
	        	$watermark->setType(Watermark::CENTER);
	        	$a = $watermark->saveAs($source_image_path_only.IMAGE_SIZE_THUMB. $filename);

	        
			/*generate_image_thumbnail($source_image_path,$source_image_path_only,IMAGE_SIZE_EXTRALARGE . $filename ,800,600, TRUE);
	        	$watermark = new Watermark($source_image_path_only.IMAGE_SIZE_EXTRALARGE. $filename);
	       	 	$watermark->setWatermarkImage(UPLOAD_LOGO_PATH . "watermark.png");
	        	$watermark->setType(Watermark::CENTER);
	        	$a = $watermark->saveAs($source_image_path_only.IMAGE_SIZE_EXTRALARGE. $filename); */
	    }
	     unlink(UPLOAD_ITEM_FULL_PATH_TEMP.$filename);
	     echo $filename;
	} else {
		echo "FALSE";
	}
} else {
    echo "FALSE";
}

function generate_image_thumbnail($source_image_path, $thumbnail_image_path,$thumbname,$width,$height, $watermark=FALSE)
{
	global $water;
    list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
    switch ($source_image_type) {
        case IMAGETYPE_GIF:
            $source_gd_image = imagecreatefromgif($source_image_path);
            break;
        case IMAGETYPE_JPEG:
            $source_gd_image = imagecreatefromjpeg($source_image_path);
            break;
        case IMAGETYPE_PNG:
            $source_gd_image = imagecreatefrompng($source_image_path);
            break;
        case IMAGETYPE_JPG:
        	$source_gd_image = imagecreatefromjpeg($source_image_path);
        	break;
    }
    if ($source_gd_image === false) {
        return false;
    }
    $source_aspect_ratio = $source_image_width / $source_image_height;
    $thumbnail_aspect_ratio = $width / $height;
    if ($source_image_width <= $width && $source_image_height <= $height) {
        $thumbnail_image_width = $source_image_width;
        $thumbnail_image_height = $source_image_height;
    } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
        $thumbnail_image_width = (int) ($height * $source_aspect_ratio);
        $thumbnail_image_height = $height;
    } else {
        $thumbnail_image_width = $width;
        $thumbnail_image_height = (int) ($width / $source_aspect_ratio);
    }
    $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
    imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
    imagejpeg($thumbnail_gd_image, $thumbnail_image_path . $thumbname, 90);
    imagedestroy($source_gd_image);
    imagedestroy($thumbnail_gd_image);
    return true;
}
?>