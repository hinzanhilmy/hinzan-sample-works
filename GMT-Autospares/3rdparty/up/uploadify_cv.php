<?php
include ('../../system/main.php');
include ('../../class/watermark.php');
?>
<?php


if (!empty($_FILES)) {	
	$kbytes = $_FILES['wbt_file']['size'] / 1024;
	$mbytes = $kbytes / 1024; // 1KB *  1024 - MB 3MB
	if(round($kbytes) < 4096 && round($kbytes) > 0){

		//$profileid = $_SESSION['manual_profile_id'];
		$tempFile = $_FILES['wbt_file']['tmp_name'];
		$ext = strtolower(pathinfo($_FILES['wbt_file']['name'], PATHINFO_EXTENSION));
		$filename =  str_replace(" ", "", microtime()) . '.' . $ext;
		
		
		if($ext =="docx" || $ext =="doc" || $ext =="pdf"){
			
			move_uploaded_file($tempFile,UPLOAD_ITEM_FULL_PATH_TEMP.$filename);
			//unlink(UPLOAD_ITEM_FULL_PATH_TEMP.$filename);
			echo $filename;
		}
		else
			echo("please upload using either doc/docx/pdf");
	} else {
		echo "FALSE";
	}
} else {
    echo "FALSE";
}

function generate_image_thumbnail($source_image_path, $thumbnail_image_path,$thumbname,$width,$height, $watermark=FALSE)
{
	global $water;
    list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
    switch ($source_image_type) {
        case IMAGETYPE_GIF:
            $source_gd_image = imagecreatefromgif($source_image_path);
            break;
        case IMAGETYPE_JPEG:
            $source_gd_image = imagecreatefromjpeg($source_image_path);
            break;
        case IMAGETYPE_PNG:
            $source_gd_image = imagecreatefrompng($source_image_path);
            break;
        case IMAGETYPE_JPG:
        	$source_gd_image = imagecreatefromjpeg($source_image_path);
        	break;
    }
    if ($source_gd_image === false) {
        return false;
    }
    $source_aspect_ratio = $source_image_width / $source_image_height;
    $thumbnail_aspect_ratio = $width / $height;
    if ($source_image_width <= $width && $source_image_height <= $height) {
        $thumbnail_image_width = $source_image_width;
        $thumbnail_image_height = $source_image_height;
    } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
        $thumbnail_image_width = (int) ($height * $source_aspect_ratio);
        $thumbnail_image_height = $height;
    } else {
        $thumbnail_image_width = $width;
        $thumbnail_image_height = (int) ($width / $source_aspect_ratio);
    }
    $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
    imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
    imagejpeg($thumbnail_gd_image, $thumbnail_image_path . $thumbname, 90);
    imagedestroy($source_gd_image);
    imagedestroy($thumbnail_gd_image);
    return true;
}
?>