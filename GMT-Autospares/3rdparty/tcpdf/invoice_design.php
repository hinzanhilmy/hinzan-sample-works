<?php
require_once('../../system/main.php');
require_once('tcpdf_import.php');

?>
<?php 
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$order_log = $fw->invoice()->order_log($id);
$profile_redeem_points = $fw->invoice()->profile_redeem_points($id);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

$pdf->SetPrintHeader(false);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' - ' . $id, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetAutoPageBreak(FALSE, 0);
$pdf->SetMargins(5, 5, 5, 5);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// set auto page breaks
$pdf->SetAutoPageBreak(FALSE, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->SetFont('dejavusans', '', 10);
$pdf->AddPage();

    
$html = '<table cellpadding="10" cellspacing="0" border="0" style="font-family: sans-serif;-webkit-font-smoothing: antialiased; width: 100%;background-image: url("http://dev.webuytogether.com.au/cdn/themes/ice/img/invoice/watermark.gif");background-repeat: no-repeat; background-position: 420px bottom;background-size: 700px;">

<tr>
  <td style="text-align: center;padding-top: 30px;">
    <img src="http://dev.webuytogether.com.au/cdn/themes/ice/img/invoice/WBTLogo.png" alt="WBT" style="width: 200px;">
  </td>
</tr>
<tr>
  <td style=" vertical-align: top; padding-top: 20px; padding-bottom: 20px;">
    <table cellpadding="8" cellspacing="2" border="0" style="font-family: sans-serif;-webkit-font-smoothing: antialiased;overflow: auto;width: 100%;">
      <tr>
        <td style="text-align:left"><h3>Deal Information</h3></td>
      </tr>
      <tbody>
      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;padding-top: 20px;">Reference No</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;padding-top: 20px;">'.$invoice_print_id.'</td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Deposit Paid</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$total_payment.'</td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Balance</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$total_balance.'</td>
      </tr>

      <tr>
        <td style="background-color:white;color: black;width: 30%;"><br><h3>Buyer Information</h3></td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Buyer Name</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$buyer_name.'</td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Buyer Phone</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$buyer_phone.'</td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Buyer Email</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$buyer_email.'</td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Buyer Address</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$buyer_address.'</td>
      </tr>
      <!-- Seller -->

      <tr>
        <td  style="background-color:white;color: black;width: 30%;"><br><h3>Seller Information</h3></td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Seller Company Name</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$seller_company.'</td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;">Seller Phone</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;">'.$seller_phone.'</td>
      </tr>

      <tr>
        <td style="background-color: #27b899;color: white;padding: 10px 20px;width: 30%;padding-bottom: 20px;">Seller Email</td>
        <td style="background-color: #F3F3F3;color: rgb(111, 111, 111);padding: 10px 20px;width: 70%;padding-bottom: 20px;">'.$seller_email.'</td>
      </tr>
      <!-- Seller -->
      </tbody>
    </table>
  </td><!-- Outside TABLE TD -->
</tr>
<!--tr>
<td>
  <img src="http://dev.webuytogether.com.au/cdn/themes/ice/img/invoice/address.png" alt="WBT" style="width: 350px;padding-bottom: 10px;">
</td>
</tr-->
  <tr>
	<td colspan="10">
		<table width="100%" border="0"  cellpadding="6" cellspacing="0" >
			<tr >
				<td colspan="10">
					We Buy Together Pty Ltd
				</td>
			</tr>
			<tr>
				<td colspan="10">
					Address:1 Heland Place Braeside VIC Australia 3195
				</td>
			</tr>
			<tr>
				<td colspan="10">
					Email: info@webuytogether.com.au 
				</td>
			</tr>
			<tr>
				<td colspan="10">
					ACN: 162 272 513 
				</td>
			</tr>
		</table>
	</td>
  </tr>
<!--
tr><td style="text-align: center;"><img src="http://dev.webuytogether.com.au/cdn/themes/ice/img/invoice/powerof.png" alt="WBT" style="width: 250px;"></td>
</tr -->
</table>';

$pdf->writeHTML($html, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();
//Close and output PDF document
$pdf->Output('invoice_design.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
?>