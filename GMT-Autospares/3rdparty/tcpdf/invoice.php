<?php
require_once('../../system/main.php');
require_once('tcpdf_import.php');

?>
<?php if(!$fw->auth()->isLogin()){  echo $fw->message(FALSE, 'Access Denied!.'); exit;}?>
<?php 
// create new PDF document

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$order_log = $fw->invoice()->order_log($id);
$profile_redeem_points = $fw->invoice()->profile_redeem_points($id);
$point_value = ($profile_redeem_points['points_redeem'] * 0.01);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Webuytogether Team');
$pdf->SetTitle('Invoice PDF');
$pdf->SetSubject('System Generated PDF');

$pdf->SetKeywords('webuytogether');
$pdf->SetPrintHeader(false);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' - ' . $id, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->SetFont('dejavusans', '', 10);
$pdf->AddPage();


foreach($fw->invoice()->get_current($id) as $i){
	$perchas_item .= '
      <tr>
        <td>' . substr($fw->product()->getSellProduct(array('id'=>$i['product_id']))->name,0,10) . '..</td>
        <td>' . $i['unit_price'] .'</td>
        <td>' . $i['quantity'] .'</td>
        <td>' . $i['total_amount'] .'</td>
        <td>' . $i['payment'] .'</td>
        <td>' . $i['balance'] .'</td>
      </tr>';
      $total_payment +=$i['payment'];
}
      

foreach($fw->invoice()->get($id) as $i){
      $unitprice = $i['unit_price'];
      $remain_days = $fw->product()->getSellProduct(array('id'=>$i['product_id']))->remain_days;
      $totalamount = $unitprice * $i['quantity'];
      $balance = $totalamount - $i['payment'];
      $running_status = ($remain_days > 0) ? '<span class="btn red">Running</span>' : '<span class="btn green">Closed</span>';
      
      $current_item .='
      <tr>
        <td>'.substr($fw->product()->getSellProduct(array('id'=>$i['product_id']))->name,0,30).'..</td>
        <td>'.number_format($unitprice,2).'</td>
        <td>'.$i['quantity'].'</td>
        <td>'.number_format($totalamount,2).'</td>
        <td>'.number_format($i['payment'] - $point_value,2).'</td>
        <td>'.number_format($balance,2).'</td>
      </tr>';
 	$total_balance +=$balance;        			
}
$depositpaid = number_format($total_payment - $point_value,2);

$payment_status = ($order_log['remote_status']==1)? 'Success': 'Pending';
      
$html = '
<table width="100%" border="0" cellpadding="10" cellspacing="0">
	<tr>
	  <td style="text-align: center;">
	    <img src="http://dev.webuytogether.com.au/cdn/themes/ice/img/invoice/WBTLogo.png" alt="WBT" style="width: 200px;">
	  </td>
	</tr>
   <tr style="background-color:#27b899 ; color:white; !important">
    <td colspan="2">'.$id .'</td>
  </tr>
  <tr>
    <td width="26%">Date</td>
    <td width="74%">' . $order_log['date'] . '</td>
  </tr>
  <tr>
    <td width="26%">Status</td>
    <td width="74%"><span>'.$payment_status.'</span></td>
  </tr>
  <tr>
    <td width="26%">Payment Method</td>
    <td width="74%"><span>'.$order_log['gateway'].'</span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   
  <tr>
	<td colspan="10"><h4>Item Purchased & Current Deal Price</h4></td>
  </tr>
  
 <tr>
	<td colspan="2" padding="0" margin="0">
		<table width="100%" border="0" style="border:1px solid graytext;" cellpadding="6" cellspacing="0">
     
	<thead>
		<tr style="background-color:#27b899 ; color:white; !important">
        <th>Product</th>
        <th>Unit Price</th>
        <th>Quantity</th>
        <th>Total Amount</th>
        <th>Payment</th>
        <th>Balance</th>
		</tr>
      </thead>
      <tbody>
      			'. $current_item .'
      </tbody>
    </table>
  	</td>
  </tr>
  
  <tr>
    <td colspan="2"><h4>Summery</h4></td>
  </tr>
  
  <tr>
    <td colspan="3"><table width="100%" border="0"  cellpadding="6" cellspacing="0" margin="0" padding="0" >
      <tr>
        <td width="50%">Total Points Redeem For This Invoice</td>
        <td width="30%">'. (float)$profile_redeem_points['points_redeem'] .'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
      <tr>
        <td width="50%">Redeem Points Value (AUD)</td>
        <td width="30%">'.number_format(($profile_redeem_points['points_redeem']*0.01), 2).'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
      <tr>
        <td width="50%">Amount Payed (AUD)</td>
        <td width="30%">'.number_format($total_payment - $point_value,2).'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
      <tr>
        <td width="50%">Total Due Balance (AUD)</td>
        <td width="30%">'. number_format(($total_balance - $depositpaid),2) .'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  
  
  <tr>
	<td> &nbsp;</td>
  </tr>
  <tr>
				<td colspan="10"></td>
			</tr>
  
  <tr>
	<td colspan="10" align="center">
		<table width="100%" border="0"  cellpadding="6" cellspacing="0" >
			
			<tr>
				<td colspan="10"></td>
			</tr>
			<tr>
				<td colspan="10"></td>
			</tr>
			
			<tr>
				<td colspan="10">
					We Buy Together Pty Ltd
				</td>
			</tr>
			<tr>
				<td colspan="10">
					Address:1 Heland Place Braeside VIC Australia 3195
				</td>
			</tr>
			<tr>
				<td colspan="10">
					Email: info@webuytogether.com.au | ACN: 162 272 513 
				</td>
			</tr>
	
			
		</table>
	</td>
  </tr>
</table>';

$pdf->writeHTML($html, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();
//Close and output PDF document
$pdf->Output('invoice.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
?>