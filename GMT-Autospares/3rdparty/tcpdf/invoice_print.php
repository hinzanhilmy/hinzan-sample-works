<?php
require_once('../../system/main.php');
?>
<style style="font-family">

	table tr td{
		font-family:helvetica;
		font-size:15px;
	}

</style>


<?php //if(($_SESSION['SUSERTYPE'] !="BUYER") || ($_SESSION['SUSERTYPE'] !="SELLER")) { echo $fw->message(FALSE, 'Access Denied!.'); exit;}?>
<?php 
$productid = strip_tags($_REQUEST['product_id']);
$order_log = $fw->invoice()->order_log($id);
$profile_redeem_points = $fw->invoice()->profile_redeem_points($id);
$point_value = ($profile_redeem_points['points_redeem'] * 0.01);

$i = $fw->invoice()->get_order_history($id, $productid);
$seller = $fw->profile()->get($i->profile_id);
                              	
$seller_logo = ($seller->logo!="default.jpg")?  'http://'. HTTP_PATH .'cdn/logo/'.$seller->logo : 'http://placehold.it/230x160&text=Logo'; 
	  
	  $seller_details = '
	  					  <tr style="background-color:#27b899 ; color:white; !important">
						    <td colspan="2"><img src="'.$seller_logo.'" alt="LOGO" style="width: 200px;"></td>
						  </tr>
						  <tr>
						    <td width="26%">Company</td>
						    <td width="74%">' . $seller->company_name . '</td>
						  </tr>
						  <tr>
						    <td width="26%">Phone</td>
						    <td width="74%">' . $seller->company_phone_no . '</td>
						  </tr>
						  <tr>
						    <td>&nbsp;</td>
						    <td>&nbsp;</td>
						  </tr>';

      $unitprice = $i->unit_price;
      $total_payment = $totalamount = $unitprice * $i->quantity;
      $balance = $totalamount - $i->payment;
      $remain_days = 1;
      $running_status = ($remain_days > 0) ? '<span class="btn red">Running</span>' : '<span class="btn green">Closed</span>';
      
      $current_item .='
      <tr>
        <td>'.substr($fw->product()->getSellProduct(array('id'=>$i->product_id))->name,0,20).'..</td>
        <td>'.number_format($unitprice,2).'</td>
        <td>'.$i->quantity.'</td>
        <td>'.number_format($totalamount,2).'</td>
        <td>'.number_format($i->payment,2).'</td>
        <td>'.number_format($balance,2).'</td>
        <td>
        	'. $running_status .'
        </td>
      </tr>';
 	$total_balance +=$balance;
 	
$depositpaid = number_format($total_payment - $point_value,2);
$payment_status = ($order_log['remote_status']==1)? 'Success': 'Pending';
      
$html = '
<table width="100%" border="0" cellpadding="10" cellspacing="0">
	<tr>
	  <td style="text-align: center;" colspan="4">
	    <img src="'.HTTP_PATH.'cdn/themes/ice/img/invoice/WBTLogo.png" alt="WBT" style="width: 200px;">
	  </td>
	</tr>
  '.$seller_details.'
  	
  <tr style="background-color:#27b899 ; color:white; !important">
    <td colspan="2">'.$id .'</td>
  </tr>
  <tr>
    <td width="26%">Date</td>
    <td width="74%">' . $order_log['date'] . '</td>
  </tr>
  <tr>
    <td width="26%">Status</td>
    <td width="74%"><span>'.$payment_status.'</span></td>
  </tr>
  <tr>
    <td width="26%">Payment Method</td>
    <td width="74%"><span>'.$order_log['gateway'].'</span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
	<td colspan="10"><h4>Item Purchased & Current Deal Price</h4></td>
  </tr>
  
 <tr>
	<td colspan="2" padding="0" margin="0">
		<table width="100%" border="0" style="border:1px solid graytext;" cellpadding="6" cellspacing="0">
     
	<thead>
		<tr style="background-color:#27b899 ; color:white; !important">
        <th>Product</th>
        <th>Unit Price</th>
        <th>Quantity</th>
        <th>Total Amount</th>
        <th>Payment</th>
        <th>Balance</th>
        <th>Deal</th>
		</tr>
      </thead>
      <tbody>
      			'. $current_item .'
      </tbody>
    </table>
  	</td>
  </tr>
  
  <tr>
    <td colspan="2"><h4>Summery</h4></td>
  </tr>
  
  <tr>
    <td colspan="3"><table width="100%" border="0"  cellpadding="6" cellspacing="0">
      <tr>
        <td width="50%">Total Points Redeem For This Invoice</td>
        <td width="30%">'. (float)$profile_redeem_points['points_redeem'] .'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
      <tr>
        <td width="50%">Redeem Points Value (AUD)</td>
        <td width="30%">'.number_format(($profile_redeem_points['points_redeem']*0.01), 2).'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
      <tr>
        <td width="50%">Amount Payed (AUD)</td>
        <td width="30%">'.number_format($total_payment - $point_value,2).'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
      <tr>
        <td width="50%">Total Due Balance (AUD)</td>
        <td width="30%">'. number_format(($total_balance - $depositpaid),2) .'</td>
		<td width="20%"> &nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  
<tr>
	<td> &nbsp;</td>
  </tr>
  <tr>
				<td colspan="10"><strong>Company Information</strong></td>
			</tr>
  
  <tr>
	<td colspan="10">
		<table width="100%" border="0"  cellpadding="6" cellspacing="0" >
			
			<tr >
				<td colspan="10">
					We Buy Together Pty Ltd
				</td>
			</tr>
			<tr>
				<td colspan="10">
					Address:1 Heland Place Braeside VIC Australia 3195
				</td>
			</tr>
			<tr>
				<td colspan="10">
					Email: info@webuytogether.com.au 
				</td>
			</tr>
		<tr>
			<td colspan="10">
				ACN: 162 272 513 
			</td>
		</tr>
			
		</table>
	</td>
  </tr>
  
  
  
  
  
</table>';
echo $html;
?>
<script>
//window.print();
</script>