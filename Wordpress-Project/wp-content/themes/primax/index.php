
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <title>Numo | Flat Portfolio</title>
    <!-- CSS -->
    <!--===============================================================-->


    <link href="<?php echo get_template_directory_uri()?>/css/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri()?>/assets/css/theme.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri()?>/assets/css/magnific-popup.css" rel="stylesheet">]
    <link href="<?php echo get_template_directory_uri()?>/css/primax.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/lightbox.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>



  <!-- NAVBAR-->
  <!--===============================================================-->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" data-scroll href="#home"><img src="<?php echo get_template_directory_uri()?>/assets/images/logo.png" alt="logo"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a data-scroll href="#portfolio">Home</a></li>
            <li><a data-scroll href="#skills">About Us</a></li>
            <li><a data-scroll href="#services">Our Works & Promotions</a></li>
            <li><a data-scroll href="blog.html">Contact Us</a></li>
            <li><a data-scroll href="#quote"><span class="contact">Get a Quote</span></a></li>
          </ul>
          </ul>
        </div>
      </div>
    </div>
    


    <!-- INTRO-->
    <!--===============================================================-->
    <div class="bg-intro" id="home">
      <div class="layer-intro">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
               <h1 class="text-center">Premium Products<br> Better Price</h1>

               <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                      <img class="d-block img-fluid" src="..." alt="First slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block img-fluid" src="..." alt="Second slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block img-fluid" src="..." alt="Third slide">
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="arrow-down"></div>

    <!-- PORTFOLIO-->
    <!--===============================================================-->
    <div class="bg-portfolio" id="portfolio">
      <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <h2 class="title">LATEST WORK</h2>
            <hr>
          </div>
        </div>

        <section>
        <div class="row">
          <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
            <div class="gallery">
               <div>
                <a class="example-image-link" href="images/IMG_001.JPG" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
                <img class="example-image img-responsive img-thumbnail" src="images/IMG_001.JPG" alt=""/></a>
                
                <a class="example-image-link" href="images/IMG_002.JPG" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image img-responsive img-thumbnail" src="images/IMG_002.JPG" alt="" /></a>
                
                <a class="example-image-link" href="images/IMG_004.JPG" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image img-responsive img-thumbnail" src="images/IMG_004.JPG" alt=""/></a>
                
                
                
                <a class="example-image-link" href="images/IMG_005.JPG" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image img-responsive img-thumbnail" src="images/IMG_005.JPG" alt=""/></a>
                
                <a class="example-image-link" href="images/IMG_006.JPG" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image img-responsive img-thumbnail" src="images/IMG_006.JPG" alt="" /></a>
                
                <a class="example-image-link" href="images/IMG_007.JPG" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image img-responsive img-thumbnail" src="images/IMG_007.JPG" alt="" /></a>
                
                <a class="example-image-link" href="images/IMG_008.JPG" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image img-responsive img-thumbnail" src="images/IMG_008.JPG" alt="" /></a>
                
          
                
              </div>
            </div>
          </div>
        </div>
      </section>

      </div>
    </div>
</div>
</div>
    <!-- SKILLS-->
    <!--===============================================================-->
    <div class="bg-skills" id="skills">
      <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <h2 class="title">About Us</h2>
              <p class="primax-content-after-heading">
                We are a 100% Australian-owned company specialising in solar PV systems and founded by a highly accredited electrician. We deliver quality products and services with 100% customer satisfaction.
                All premium products under one roof.
              </p>
          </div>
      </div>
    </div>

    <!-- SERVICES-->
    <!--===============================================================-->
    <div class="bg-services" id="services">
      <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <h2 class="title">SERVICES - Commercial & Residentials</h2>
          </div>
        </div>

        <div class="row margin-bottom primax-content-after-heading">
          <div class="col-sm-12 margin-bottom-xs">
            <div class="row">
                <p>
                  <ul class="primax-default">
                    <li>
                        Solar Panel Installation - Trina, Jinko, Canadian, Fronius, SMA, Solax, Sungrow and Other top tier Products
                    </li>
                    <li>
                        Hybrid Battery System - LG Chem, Tesla, Samsung
                    </li>
                    <li>
                        Switch Board Upgrade
                    </li>
                    <li>
                        Ground Mounting System
                    </li>
                    <li>
                        All other electrical services
                    </li>



                  <ul>
                </p>
            </div>
          </div>

       
        </div>
        </div>
      </div>
    </div>

    <!-- CONTACT-->
    <!--===============================================================-->
    
    <div class="container" id="quote">
      <div class="row">
        <div class="cell-sm-12">
            <h2 class="title">Get a quote for a residential or business solar panel installation</h2>
            <!-- RD Mailform-->
            <form data-form-output="form-output-global" data-form-type="quote" method="post" action="bat/rd-mailform.php" class="rd-mailform offset-top-30">
            <div class="form-group">
              <label for="contact-name" class="form-label">Your Name</label>
              <input id="contact-name" type="text" name="name" data-constraints="@Required" class="form-control">
            </div>
            <div class="form-group">
             <label for="contact-email" class="form-label">Email</label>
              <input id="contact-email" type="email" name="email" data-constraints="@Email @Required" class="form-control">
            </div>
             <div class="form-group">
            <label for="contact-phone" class="form-label">Phone</label>
            <input id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required" class="form-control">
          </div>
          <div class="form-group">
            <!--Select 2-->
            <select data-placeholder="Do you need a residental or commercial system?" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter" name="system-location">
              <option value="Did not specify">Do you need a residental or commercial system?</option>
              <option value="Residental system">Residental system</option>
              <option value="Commercial system">Commercial system</option>
            </select>
          </div>
          <div class="form-group">
            <!--Select 2-->
            <select name="sysmte_size" data-placeholder="What is your average electricity bill?" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
              <option>What is your average electricity bill?</option>
              <option value="100">100</option>
              <option value="200">200</option>
              <option value="300">300</option>
              <option value="Others">Others</option>
            </select>
          </div>
          <div class="form-group">
            <label for="contact-phone" class="form-label">Message (Special Note)</label>
            <textarea class="form-control" rows="5" data-constraints="@Required" id="Message-text-area" name="message"></textarea>
          </div>





          <button type="submit" class="btn btn-primary offset-top-30">GET A QUOTE</button>
          </form>
         </div> 
      </div> 
    </div>  


    <section class="container">
            <h3>Contact Us</h3>
            <div class="row">
                  <div class="col-sm-6">
                      <Address>
                      13, Wonder Street <br/ ><br/ >
                      
                      Officer VIC 3809 <br/ ><br/ >
                      
                      P.O Box 230 Officer <br/ ><br/ >
                      
                      Tel : 1300-511-383,  045 8167 273 <br/ ><br/ >
                       
                      Email : info@primexsolar.com.au


                      </Address>

                  </div>
                  <div class="col-sm-6">
                  <form data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php" class="rd-mailform offset-top-30">
                    <div class="form-group">
                      <label for="contact-name" class="form-label">Your Name</label>
                      <input id="contact-name" type="text" name="name" data-constraints="@Required" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="contact-email" class="form-label">Email</label>
                      <input id="contact-email" type="email" name="email" data-constraints="@Email @Required" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="contact-phone" class="form-label">Phone</label>
                      <input id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required" class="form-control">
                    </div>
                
					<div class="form-group">
                      <label for="contact-phone" class="form-label">Message (Special Note)</label>
					  <textarea class="form-control" rows="5" data-constraints="@Required" id="Message-text-area" name="message">
					  
					  </textarea>
                    </div>
					
					
                    <button type="submit" class="btn btn-primary offset-top-30">Submit</button>
                  </form>
                  
                  </div>

            </div>

            </div>
</div>
    </section>  










    <!-- FOOTER-->
    <!--===============================================================-->
    <div class="bg-footer">
      <div class="container-fluid">
        <div class="row text-center">
          <div class="col-sm-12">
            <a href="#"><i class="fa fa-facebook"></i></a>
          </div>
        </div>
      </div>
    </div>

    <!-- FOOTER-BOTTOM-->
    <!--===============================================================-->
    <div class="bg-footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p class="copyright">2017 &copy; Primax All Rights Reserved</p>
          </div>
          <div class="col-sm-6">
            <ul class="list-inline pull-right">
              <li class="border-right"><a href="#home" data-scroll>Top</a></li>

                     <li  class="border-right"><a data-scroll href="#portfolio">Home</a></li>
            <li class="border-right"><a data-scroll href="#skills">About Us</a></li>
            <li class="border-right"><a data-scroll href="#services">Service</a></li>
            <li class=""><a data-scroll href="blog.html">Contact Us</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- JAVASCRIPT-->
    <!--===============================================================-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/assets/js/smooth-scroll.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/assets/js/jRespond.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/assets/js/script.js"></script>
    <script>
      $('.carousel').carousel();
    </script>

  </body>
</html>