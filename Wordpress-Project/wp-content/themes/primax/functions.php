<?php

/**
 * Proper way to enqueue scripts and styles
 */
function wpdocs_theme_name_scripts() {
    wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script( 'script-name', get_template_directory_uri() . '/wp-content/themes/primax/css/bootstrap.min.css', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );


register_nav_menus( array(
    "primary"=>__('Primary Menue'),
    "footer"=>__('footer Menue')

) );

?>